var scripts =  document.getElementById('posisi_room').getAttribute("src");

// myScript now contains our script object
var queryString = scripts.replace(/^[^\?]+\??/,'');

// console.log(queryString);
var res = queryString.split("=");
var res_param = queryString.split("&");

var dataurl = res[1].split("=")[0].split("&")[0];
var cpyid = res_param[1].split("=")[1];
var brand_id = res_param[2].split("=")[1];
var access_token = res_param[3].split("=")[1];
var url = res_param[4].split("=")[1];
var chat_session = res_param[5].split("=")[1];

const headers_ = {
    "Authorization": "Bearer "+access_token    
};

class Posisi extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            harga: '',
            // chat_session: '',
            posisi_list: [],
            total_stan: 0,
            total_amount: 0
        }

    }

    componentDidMount(){

        // const fd = new FormData()
        // fd.append("register_id", "AAAA2FwbwdM:APA91bFPqMP4e2IdJ3EB_bx_8hKRnSWa3Uc_FXu0o_cuo69H14N4DvYd70yNoCiIcXnw86J1OBxcs4ng_BJK7YREukI8hoPnp9t9O49_5LetUkVLPEdpECHlygNrWf4Xm3JaRvTf2Xre")
        // fd.append("authenticator", "browser")
        // fd.append("cpyid", cpyid)

        // axios.post(dataurl+"api/customer/add/nego/chat/init", fd, {headers:headers_})
        // .then(data => {
        //     // console.log(data)
        //     this.setState({
        //         chat_session: data.data.token.chat_session_token
        //     })
        // })
        // .catch(e => {
        //     console.log(e)
        // })
        // console.log(this.state.chat_session)
        this.getDataStan()

        // Automatic reload 5 seconds
        // setInterval(() => {
        //     this.getDataStan()
        // }, 5000)
        messaging.onMessage(payload => {
            console.log('onMessage:', payload)
            new Notification('New Chat Message', {
                icon: 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                body: payload.data.body,
            });
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', '../audio/sound.mp3');
            audioElement.play();

            axios.get(dataurl+'api/customer/view/order/booking/pending/all',{headers:headers_})
                .then(data => {
                    this.setState({
                        posisi_list: data.data.payment_grid_with_code,
                        total_stan: data.data.payment_grid_with_code.length
                    })
                })
                .catch(e => {
                    // console.log(e)
                })
        })

    }

    getDataStan() {

        const headers_chat = {
            "Authorization": "Bearer "+access_token,
            "chat-session": "Bearer "+chat_session       
        };

        axios.get(dataurl+'api/customer/view/order/booking/pending/spec?cpyid='+cpyid,{headers:headers_chat})
            .then(data => {
                // console.log(data)
                this.setState({
                    posisi_list: data.data.payment_grid_with_code,
                    total_stan: data.data.payment_grid_with_code.length
                })

                // var t = data.data.payment_grid_with_code.filter( obj => obj.name === 'dealvalue')[0];
                // console.log(t)
                var total = [];
                data.data.payment_grid_with_code.map((value, i) => {
                    // console.log(value.dealvalue)
                    total.push(parseInt(value.dealvalue))
                })

                // console.log(total)
                var sum = total.reduce((a, b) => a + b, 0);
                // console.log(sum);
                this.setState({
                    total_amount: this.convertToRupiah(sum)
                })
            })
            .catch(e => {
                console.log(e)
            })
    }

    convertToRupiah(angka) {
        var rupiah = '';		
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
    }

    render(){
        const posisi = this.state.posisi_list.map((value,i) =>
            <tr key={i}>
                <td className="bg-light-gray">{value.code}</td>
                <td className="bg-light-gray">{this.convertToRupiah(parseInt(value.dealvalue))}</td>
            </tr>
        )
        return(
            <React.Fragment>
                <div className="table-responsive m-t-15">
                    <table className="table t-a-c m-r-b">
                        <thead>
                            <tr className="bg-orange c-white">
                                <td>NO. POSISI</td>
                                <td>HARGA</td>
                            </tr>
                        </thead>
                        <tbody>
                            {posisi}
                        </tbody>
                    </table>
                </div>

                <div className="table-responsive">   
                    <table className="table m-r-b">
                        <tbody>
                            <tr className="bg-gray c-white">
                                <td>TOTAL STAN</td>
                                <td className="t-a-r">{this.state.total_stan}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div className="table-responsive">   
                    <table className="table m-r-b">
                        <tbody>
                            <tr className="bg-light-black c-white">
                                <td className="b-r-top">
                                    <i>
                                        LAIN LAIN<br/>
                                        DEPOSIT
                                    </i>
                                </td>
                                <td className="t-a-r b-r-top">
                                    0<br/>
                                    0
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <hr className="b-orange m-r"/>
                <div className="table-responsive">   
                    <table className="table m-r-b">
                        <tbody>
                            <tr className="bg-light-black c-white">
                                <td className="b-r-top">
                                    <h6 className="m-r-b">TOTAL TAGIHAN</h6>
                                </td>
                                <td className="t-a-r b-r-top">
                                    {this.state.total_amount}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </React.Fragment>
        )
    }


}

ReactDOM.render(
    <Posisi/>,
    document.getElementById('posisi')
)
