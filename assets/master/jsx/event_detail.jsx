var scripts =  document.getElementById('event_detail').getAttribute("src");
var queryString = scripts.replace(/^[^\?]+\??/,'');
var res = queryString.split("=");
var res_param = queryString.split("&");
var dataurl = res[1].split("=")[0].split("&")[0];
var eventid = res_param[1].split("=")[1];
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth() + 1; // January is 0!
var yyyy = today.getFullYear();

if (dd < 10) {
  dd = '0' + dd;
}

if (mm < 10) {
  mm = '0' + mm;
}

today = yyyy + '-' + mm + '-' + dd;

class EventDetail extends React.Component {
    _isMounted = false;
    isSelected = [];
    isDataSelected = [];
    constructor(props){
        super(props);
        this.state = {
            data_event: [],
            key: 0,
            checkedItems: new Map(),
            selectedZona: null,
            selectedStartDate: this.props.startdate,
            selectedEndDate: this.props.lastdate,
            zonaList: []
        };

        this.deleteData = this.deleteData.bind(this);
        this.loadZonaList = this.loadZonaList.bind(this);
    }

    componentDidMount() {
        global.changeStartDate = (date) => {
            if (date !== "") {
                this.isSelected = [];
                this.setState({
                    selectedStartDate: date,
                    checkedItems: new Map()
                }, this.loadZonaList);
            }
        };

        global.changeLastDate = (date) => {
            if (date !== "") {
                this.isSelected = [];
                this.setState({
                    selectedEndDate: date,
                    checkedItems: new Map()
                }, this.loadZonaList);
            }
        };

        this._isMounted = true;
        axios.get(dataurl + "api/event/view/detail?sid=" + eventid)
            .then(data => {
                if (this._isMounted) {
                    this.setState({
                        data_event: data.data.event_data[0].space_provide_layout
                    });

                    // set defaut data to first zona, terus load zona list
                    if (data.data.event_data[0].space_provide_layout.length > 0) {
                        if (data.data.event_data[0].space_provide_layout[0].space_provide_layout_grid.length > 0) {
                            this.setState({
                                selectedZona: data.data.event_data[0].space_provide_layout[0].space_provide_layout_grid[0].code.substr(0, 1)
                            }, this.loadZonaList);
                        }
                    }
                   
                }
            })
            .catch(e => {
                console.log(e);
            });

        
    }

    componentWillUnmount() {
      this._isMounted = false;
    }
    
    handleZonaChange(event) {
        this.setState({
            selectedZona: event.target.value
        }, this.loadZonaList);
    }

    handleChange(e) {
        const item = e.target.value.split("|");
        const isChecked = e.target.checked;
        this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(item[0], isChecked) }));
        if(!isChecked) {
            this.deleteData(this.isDataSelected, item[1]);
        } else {
            this.appendData(e.target.value);
        }
    }

    appendData(value) {
        this.isSelected.push(<li>{value.split("|")[1]}</li>);
        this.isDataSelected.push(value.split("|")[1]);
    }
    
    deleteData(arr, value) {
        var what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        this.isSelected = [];
        for(var i = 0; i < arr.length; i++){
            this.isSelected.push(<li>{arr[i]}</li>);
        }
    }

    loadZonaList() {
        axios.get(dataurl + "api/event/view/detail/grid/filter?sid=" + eventid + "&start_date=" + this.state.selectedStartDate + "&finish_date=" + this.state.selectedEndDate + "&zone=" + this.state.selectedZona)
            .then(data => {
                this.setState({
                    zonaList: data.data.event_data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    render(){

        console.log(this.state);

        let zonaList = [];
        this.state.data_event.map((value, i) => 
            value.space_provide_layout_grid.map((v, j) => {
                zonaList.push(v.code.substr(0,1));
            })
        );
        zonaList = [... new Set(zonaList)];

        const zonaOptions = zonaList.map((v, i) => {
            return (
                <option value={v} key={i}>
                    Zona {v}
                </option>
            );
        });

        const content = this.state.zonaList.map((value, i) =>
            value.brandname !== null ?
                <div key={i} className="d-flex bd-highlight" style={{ background: 'darkgray', color: 'white', borderBottom: '1px solid #fff' }}>
                    <div className="p-2 bd-highlight check">
                        <input disabled type="checkbox" name="gridselected[]" value={value.code} />
                    </div>
                    <div className="p-2 bd-highlight">{value.code}</div>
                </div>
                :
                <div key={i} className="d-flex" style={{ color: 'black', borderBottom: '1px solid #fff' }}>
                    <div className="p-2 bd-highlight check">
                        <Checkbox value={value.layoutgridid + "|" + value.code} checked={this.state.checkedItems.get(value.layoutgridid)} onChange={this.handleChange.bind(this)} />
                    </div>
                    <div className="p-2 bd-highlight">{value.code}</div>
                </div>
        );

        return(
            <React.Fragment>
				<div className="row">
					<div className="offset-md-3 col-md-6">
						<p className="font-weight-bold">PILIHAN STAN</p>
					</div>
				</div>
				<div className="row">
					<div className="offset-md-3 col-md-6 bg-second py-3 px-5">
						<div className="row">
							<div className="col-md-12">
								<p className="text-left font-weight-bold">ZONA</p>
							</div>
						</div>
						<div className="row">
							<div className="col-md-8">
								<div className="row">
									<div className="col-md-6">
                                        <select className="form-control" value={this.state.selectedZona} onChange={this.handleZonaChange.bind(this)}>
											{zonaOptions}
										</select>
									</div>
								</div>
							</div>
							<div className="col-md-4">
								<p className="text-left font-weight-bold">STAND DIPILIH</p>
							</div>
						</div>
						<div className="row">
							<div className="col-md-8">
								<div className="ex1" id="data-event-append">	
									<div className="bg-white h-100 p-2">
										<div className="bg-light-gray h-100 d-block" style={{overflow: 'auto'}}>
											{content}
										</div>
									</div>
								</div>
							</div>
							<div className="col-md-4">
								<div className="display-data" id="display-data">
									<div className="bg-white h-100 p-2">
										<div className="h-100 d-block" style={{overflow: 'auto'}}>
											<ul>
												{this.isSelected}
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br/>
            </React.Fragment>
        )
    }
    
}

const Checkbox = ({ type = 'checkbox', value, checked = false, onChange }) => (
    <input type={type} name="gridselected[]" value={value} checked={checked} onChange={onChange} />
  );

const elementId = "data-event";
const element = document.getElementById(elementId);
if (element !== null && element.dataset.startdate !== undefined && element.dataset.lastdate !== undefined) {
    ReactDOM.render(
        <EventDetail startdate={element.dataset.startdate} lastdate={element.dataset.lastdate} />, document.getElementById('data-event')
    );
}