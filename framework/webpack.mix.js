const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/common.scss', 'public/css')
    .css('node_modules/react-datepicker/dist/react-datepicker.min.css', 'public/css')
    .react('resources/jsx/contact/help-login.jsx', 'public/js/contact')
    .react('resources/jsx/contact/help-nologin.jsx', 'public/js/contact')
    .react('resources/jsx/contact/spaceprovider.jsx', 'public/js/contact')
    .react('resources/jsx/home/message.jsx', 'public/js/home')
    .react('resources/jsx/suratloading/create.jsx', 'public/js/suratloading')
    .react('resources/jsx/refund/refundlist.jsx', 'public/js/refund')
    .react('resources/jsx/refund/refundselect.jsx', 'public/js/refund')
    .react('resources/jsx/refund/refundcreate.jsx', 'public/js/refund')
    .react('resources/jsx/payment/history.jsx', 'public/js/payment')
    .react('resources/jsx/event/eventcard.jsx', 'public/js/event')
    .react('resources/jsx/contact/list-aduan-admin.jsx', 'public/js/contact')
    .react('resources/jsx/contact/list-aduan-sp.jsx', 'public/js/contact')
    .react('resources/jsx/contact/view-aduan-admin.jsx', 'public/js/contact')
    .react('resources/jsx/contact/view-aduan-sp.jsx', 'public/js/contact')
    .react('resources/jsx/pending.jsx', 'public/js')
    .react('resources/jsx/chat.jsx', 'public/js');
