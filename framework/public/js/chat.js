/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 283);
/******/ })
/************************************************************************/
/******/ ({

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(284);


/***/ }),

/***/ 284:
/***/ (function(module, exports) {

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var scripts = document.getElementById('chat_room').getAttribute("src");
var endpoint_url = "https://tukbase3.tukmu.com/";
var queryString = scripts.replace(/^[^\?]+\??/, '');
var res = queryString.split("=");
var res_param = queryString.split("&");
var dataurl = res[1].split("=")[0].split("&")[0];
var cpyid = res_param[1].split("=")[1];
var brand_id = res_param[2].split("=")[1];
var access_token = res_param[3].split("=")[1];
var url = res_param[4].split("=")[1];
var chat_session = res_param[5].split("=")[1];

var headers_ = {
    "Authorization": "Bearer " + access_token
};

var config = {
    apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
    authDomain: "bukapameran-45e78.firebaseapp.com",
    databaseURL: "https://bukapameran-45e78.firebaseio.com",
    projectId: "bukapameran-45e78",
    storageBucket: "bukapameran-45e78.appspot.com",
    messagingSenderId: "929258258899"
};

firebase.initializeApp(config);
var messaging = firebase.messaging();

messaging.requestPermission().then(function () {
    console.log("have Permission");
    return messaging.getToken();
}).catch(function (e) {
    console.log("error ", e);
});

messaging.getToken().then(function (token) {
    var fd = new FormData();
    fd.append("register_id", token);
    fd.append("authenticator", "browser");
    fd.append("cpyid", cpyid);

    axios.post("https://tukbase3.tukmu.com/api/customer/add/nego/chat/init", fd, { headers: headers_ }).then(function (data) {
        localStorage.setItem("_chat_token", data.data.token.chat_session_token);
    }).catch(function (e) {
        console.log(e);
        if (e == "Error: Request failed with status code 401") {
            window.location.replace(url + "/logout");
        }
    });
});

var headers_chat = {
    "Authorization": "Bearer " + access_token,
    "chat-session": "Bearer " + localStorage.getItem("_chat_token")
};

var Chat = function (_React$Component) {
    _inherits(Chat, _React$Component);

    function Chat(props) {
        _classCallCheck(this, Chat);

        var _this = _possibleConstructorReturn(this, (Chat.__proto__ || Object.getPrototypeOf(Chat)).call(this, props));

        _this.state = {
            // chat_session: '',
            chat_text: '',
            chat_list: [],
            loading: false,
            loading_data: false,
            loading_chat: false,
            stan_list: [],
            posisi_list: [],
            total_stan: 0,
            total_amount: 0,
            branddetail: {},
            addon: []
        };

        _this.handleChangeChat = _this.handleChangeChat.bind(_this);
        return _this;
    }

    _createClass(Chat, [{
        key: "componentDidMount",
        value: function componentDidMount() {
            var _this2 = this;

            this.getDataChat();
            this.getDataStan();
            this.getDataStan1();
            this.getDataBrandDetail();
            this.getTambahan();

            navigator.serviceWorker.addEventListener('message', function (event) {
                axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                    _this2.setState({
                        chat_list: data.data.nego_chat
                    });
                }).catch(function (e) {});
            });

            messaging.onMessage(function (payload) {
                new Notification('New Chat Message', {
                    icon: 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                    body: payload.data.body
                });
                var audioElement = document.createElement('audio');
                audioElement.setAttribute('src', url + '/assets/master/audio/sound.mp3');
                audioElement.play();

                axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                    _this2.setState({
                        chat_list: data.data.nego_chat
                    });
                }).catch(function (e) {});

                axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat }).then(function (data) {
                    _this2.setState({
                        stan_list: data.data.payment_grid_with_code
                    });
                }).catch(function (e) {
                    console.log(e);
                });

                axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat }).then(function (data) {
                    _this2.setState({
                        posisi_list: data.data.payment_grid_with_code,
                        total_stan: data.data.payment_grid_with_code.length
                    });
                    var total = [];
                    data.data.payment_grid_with_code.map(function (value, i) {
                        total.push(parseInt(value.dealvalue));
                    });
                    var sum = total.reduce(function (a, b) {
                        return a + b;
                    }, 0);
                    _this2.setState({
                        total_amount: _this2.convertToRupiah(sum)
                    });
                }).catch(function (e) {
                    console.log(e);
                });
            });
        }
    }, {
        key: "componentDidUpdate",
        value: function componentDidUpdate() {
            // get the messagelist container and set the scrollTop to the height of the container
            if (!this.state.loading_data) {
                var objDiv = document.getElementById('chat');
                objDiv.scrollTop = objDiv.scrollHeight;
            }
        }
    }, {
        key: "getDataBrandDetail",
        value: function getDataBrandDetail() {
            var _this3 = this;

            this.setState({
                loading_data: true
            });
            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                try {
                    var otherfee = 0;
                    for (var i = 0; i < data.data.customer_payment_other_fee.length; i++) {
                        otherfee = otherfee + parseInt(data.data.customer_payment_other_fee[i].otherpayvalue);
                    }
                    _this3.setState({
                        branddetail: {
                            startdate: data.data.startdate,
                            finishdate: data.data.finishdate,
                            spacename: data.data.spacename,
                            minprice: data.data.minprice,
                            maxprice: data.data.maxprice,
                            brandname: data.data.customer_brand.brandname,
                            payment_grid_with_code: data.data.payment_grid_with_code,
                            code: data.data.payment_grid_with_code[0].code,
                            customer_brand: data.data.customer_brand,
                            desc: data.data.desc,
                            namaspaceprov: data.data.namaspaceprov,
                            termpath: data.data.term_condition.length > 0 ? data.data.term_condition[0].spacetermpath : '',
                            eventname: data.data.eventname,
                            eventpic: data.data.space_detail.length > 0 ? data.data.space_detail[0].first_media != null ? data.data.space_detail[0].first_media.spacemediapath : '' : '',
                            depositvalue: data.data.customer_payment_deposit.depositvalue,
                            otherfeevalue: otherfee,
                            customer_term_payment: data.data.customer_term_payment,
                            customer_flexible_payment: data.data.customer_flexible_payment
                        },
                        loading_data: false
                    });
                } catch (e) {
                    console.log(e);
                }
            }).catch(function (e) {
                // console.log(e)
                _this3.setState({
                    loading_data: false
                });
            });
        }
    }, {
        key: "getDataChat",
        value: function getDataChat() {
            var _this4 = this;

            this.setState({
                loading_chat: true
            });
            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                _this4.setState({
                    chat_list: data.data.nego_chat,
                    loading_chat: false
                });
            }).catch(function (e) {
                // console.log(e)
                _this4.setState({
                    loading_chat: false
                });
            });
        }
    }, {
        key: "getTambahan",
        value: function getTambahan() {
            var _this5 = this;

            this.setState({
                loading_data: true
            });
            axios.get(dataurl + 'api/event/view/detail/addon?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                _this5.setState({
                    addon: data.data.addon_data,
                    loading_data: false
                });
            }).catch(function (e) {
                // console.log(e)
                _this5.setState({
                    loading_data: false
                });
            });
        }
    }, {
        key: "handleChangeChat",
        value: function handleChangeChat(event) {
            this.setState({ chat_text: event.target.value });
        }
    }, {
        key: "sendChat",
        value: function sendChat() {
            var _this6 = this;

            // this.setState({
            //     chat_list: this.state.chat_list.concat(this.state.chat_text)
            // })

            var fd = new FormData();
            fd.append("cust_chat", this.state.chat_text);
            fd.append("cpyid", cpyid);
            fd.append("chat_type", 0);

            this.setState({
                loading: true
            });

            axios.post(dataurl + "api/customer/add/nego/chat/send", fd, { headers: headers_chat }).then(function (data) {
                // console.log(data)
                if (data.data.status === "1") {
                    axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ }).then(function (data) {
                        // console.log(data)
                        _this6.setState({
                            chat_list: data.data.nego_chat,
                            chat_text: '',
                            loading: false
                        });
                    }).catch(function (e) {
                        console.log(e);
                        _this6.setState({
                            loading: false
                        });
                    });
                }
            }).catch(function (e) {
                // console.log(e)
            });
            // console.log(fd)
        }
    }, {
        key: "rejectSPOffer",
        value: function rejectSPOffer() {
            var fd = new FormData();
            fd.append("cpyid", cpyid);

            this.setState({
                loading: true
            });

            axios.post(dataurl + "api/customer/add/nego/deal/button/2", fd, { headers: headers_chat }).then(function (data) {
                console.log(data);
            });
        }
    }, {
        key: "getDataStan",
        value: function getDataStan() {
            var _this7 = this;

            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat }).then(function (data) {
                // console.log(data)
                _this7.setState({
                    stan_list: data.data.payment_grid_with_code
                });
            }).catch(function (e) {
                console.log(e);
            });
        }
    }, {
        key: "getDataStan1",
        value: function getDataStan1() {
            var _this8 = this;

            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat }).then(function (data) {
                // console.log(data)
                _this8.setState({
                    posisi_list: data.data.payment_grid_with_code,
                    total_stan: data.data.payment_grid_with_code.length
                });

                // var t = data.data.payment_grid_with_code.filter( obj => obj.name === 'dealvalue')[0];
                // console.log(t)
                var total = [];
                data.data.payment_grid_with_code.map(function (value, i) {
                    // console.log(value.dealvalue)
                    total.push(parseInt(value.dealvalue));
                });

                // console.log(total)
                var sum = total.reduce(function (a, b) {
                    return a + b;
                }, 0);
                // console.log(sum);
                _this8.setState({
                    total_amount: _this8.convertToRupiah(sum)
                });
            }).catch(function (e) {
                console.log(e);
            });
        }
    }, {
        key: "convertToRupiah",
        value: function convertToRupiah(angka) {
            var rupiah = '';
            var angkarev = angka.toString().split('').reverse().join('');
            for (var i = 0; i < angkarev.length; i++) {
                if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
            }return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
        }
    }, {
        key: "formatDate",
        value: function formatDate(date) {

            var monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Augustus", "September", "Oktober", "November", "Desember"];

            var hari = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];

            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = this.addZero(hours) + ':' + this.addZero(minutes) + ' ' + ampm;

            var hariIndex = date.getDay();
            var tanggal = this.addZero(date.getDate());
            var monthIndex = date.getMonth();
            var year = date.getFullYear();

            return tanggal + ' ' + monthNames[monthIndex] + ' ' + year;
        }
    }, {
        key: "addZero",
        value: function addZero(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }
    }, {
        key: "render",
        value: function render() {
            var _this9 = this;

            var _state = this.state,
                branddetail = _state.branddetail,
                loading_data = _state.loading_data,
                loading_chat = _state.loading_chat;


            var ListChat = this.state.chat_list.map(function (value, i) {
                if (value.sp_name === null && value.cust_name === null) {
                    return React.createElement(
                        "div",
                        { key: i, className: "row ml-5 mr-1 my-3" },
                        React.createElement(
                            "div",
                            { className: "col-11 p-3", style: { borderRadius: '1rem', backgroundColor: '#b7a7f8' } },
                            React.createElement(
                                "div",
                                null,
                                "SYSTEM"
                            ),
                            React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                            React.createElement(
                                "div",
                                null,
                                value.chatvalue
                            )
                        ),
                        React.createElement(
                            "div",
                            { className: "col-1 d-flex p-1" },
                            React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                        )
                    );
                } else if (value.sp_name !== null) {
                    if (value.chattype === 0) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    value.chatvalue
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    } else if (value.chattype === 3) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    value.chatvalue
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    "Rp. " + value.chatparams.total
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    value.chatparams.status === "0" ? React.createElement(
                                        "div",
                                        null,
                                        React.createElement(
                                            "a",
                                            { href: url + "/tambahan/1/" + chat_session + "/" + cpyid },
                                            "YES"
                                        ),
                                        "\xA0\xA0",
                                        React.createElement(
                                            "a",
                                            { href: "#", onClick: function onClick() {
                                                    return _this9.rejectSPOffer();
                                                } },
                                            "NO"
                                        )
                                    ) : null,
                                    value.chatparams.status === "1" ? React.createElement(
                                        "div",
                                        null,
                                        "Approved"
                                    ) : null,
                                    value.chatparams.status === "2" ? React.createElement(
                                        "div",
                                        null,
                                        "Rejected"
                                    ) : null
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    } else if (value.chattype === 4) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    "Perubahan Harga"
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    value.chatparams.grid_code + " - " + _this9.convertToRupiah(parseInt(value.chatparams.price))
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    } else if (value.chattype === 5) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    "Perubahan Tanggal"
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    "Dari: ",
                                    value.chatparams.old_start_booking_date + " - " + value.chatparams.old_finish_booking_date
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    "Menjadi: ",
                                    value.chatparams.start_booking_date + " - " + value.chatparams.finish_booking_date
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    } else if (value.chattype === 6) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    "Biaya Lain-lain"
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    decodeURI(value.chatparams.other_fee_name) + " - " + _this9.convertToRupiah(parseInt(value.chatparams.other_fee_value))
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    } else if (value.chattype === 7) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-5 mr-1 my-3" },
                            React.createElement(
                                "div",
                                { className: "bg-orange col-11 p-3", style: { borderRadius: '1rem' } },
                                React.createElement(
                                    "div",
                                    null,
                                    value.sp_name
                                ),
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                React.createElement(
                                    "div",
                                    null,
                                    "Deposit"
                                ),
                                React.createElement(
                                    "div",
                                    null,
                                    _this9.convertToRupiah(parseInt(value.chatparams.deposit))
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-orange.png" })
                            )
                        );
                    }
                } else if (value.cust_name !== null) {
                    if (value.chattype === 0) {
                        return React.createElement(
                            "div",
                            { key: i, className: "row ml-1 mr-5 my-3" },
                            React.createElement(
                                "div",
                                { className: "col-1 d-flex p-1" },
                                React.createElement("img", { className: "img-responsive w-100 align-self-center", src: "/assets/master/img/user/user-gray.png" })
                            ),
                            React.createElement(
                                "div",
                                { className: "bg-light-gray col-11 p-3", style: { borderRadius: '1rem' } },
                                value.cust_name,
                                React.createElement("hr", { className: "my-1", style: { borderColor: 'white' } }),
                                value.chatvalue
                            )
                        );
                    }
                }
            });

            var ListFile = this.state.chat_list.map(function (value, i) {
                if (value.sp_name !== null) {
                    if (value.chattype === 1) {
                        return React.createElement(
                            "div",
                            { className: "p-2 border-bottom" },
                            React.createElement(
                                "span",
                                null,
                                React.createElement("i", { className: "far fa-file f-s-20 align-middle" })
                            ),
                            React.createElement(
                                "a",
                                { className: "p-2 align-middle", href: endpoint_url + "/fileGet/" + value.nego_attachment[0].attachmentpath, target: "_blank" },
                                value.chatvalue
                            )
                        );
                    }
                } else if (value.cust_name !== null) {
                    if (value.chattype == 1) {
                        return React.createElement(
                            "div",
                            { className: "p-2 border-bottom" },
                            React.createElement(
                                "span",
                                null,
                                React.createElement("i", { className: "far fa-file f-s-20 align-middle" })
                            ),
                            React.createElement(
                                "a",
                                { className: "p-2 align-middle", href: endpoint_url + "/fileGet/" + value.nego_attachment[0].attachmentpath, target: "_blank" },
                                value.chatvalue
                            )
                        );
                    }
                }
            });

            var ListLink = this.state.chat_list.map(function (value, i) {
                if (value.sp_name !== null) {
                    if (value.chattype == 2) {
                        return React.createElement(
                            "div",
                            { className: "p-2 border-bottom" },
                            React.createElement(
                                "span",
                                null,
                                React.createElement("i", { className: "fa fa-link f-s-20 align-middle" })
                            ),
                            React.createElement(
                                "a",
                                { className: "p-2 align-middle", href: value.nego_link[0].linkpath, target: "_blank" },
                                value.nego_link[0].linkname
                            ),
                            React.createElement("br", null),
                            React.createElement(
                                "small",
                                null,
                                value.nego_link[0].linkpath
                            )
                        );
                    }
                } else if (value.cust_name !== null) {
                    if (value.chattype == 2) {
                        return React.createElement(
                            "div",
                            { className: "p-2 border-bottom" },
                            React.createElement(
                                "span",
                                null,
                                React.createElement("i", { className: "fa fa-link f-s-20 align-middle" })
                            ),
                            React.createElement(
                                "a",
                                { className: "p-2 align-middle", href: value.nego_link[0].linkpath, target: "_blank" },
                                value.nego_link[0].linkname
                            ),
                            React.createElement("br", null),
                            React.createElement(
                                "small",
                                null,
                                value.nego_link[0].linkpath
                            )
                        );
                    }
                }
            });

            var ListAddon = this.state.addon.map(function (value, i) {
                return React.createElement(
                    "tr",
                    { className: "t-a-c" },
                    React.createElement(
                        "td",
                        { className: "bg-light-orange" },
                        value.spaceaddsrvdesc
                    ),
                    React.createElement(
                        "td",
                        { className: "bg-light-yellow" },
                        _this9.convertToRupiah(parseInt(value.spaceaddsrvprice))
                    ),
                    React.createElement(
                        "td",
                        { className: "bg-light-orange" },
                        value.remaining
                    )
                );
            });

            var stan = this.state.stan_list.map(function (value, i) {
                return React.createElement(
                    "div",
                    { key: i, style: { marginRight: '2px' } },
                    value.code,
                    ","
                );
            });

            var posisi = this.state.posisi_list.map(function (value, i) {
                return React.createElement(
                    "tr",
                    { key: i },
                    React.createElement(
                        "td",
                        { className: "bg-light-gray" },
                        value.code
                    ),
                    React.createElement(
                        "td",
                        { className: "bg-light-gray" },
                        _this9.convertToRupiah(parseInt(value.dealvalue))
                    )
                );
            });

            var DetailCicilan = function DetailCicilan() {
                if (_this9.state.branddetail.payment_grid_with_code != undefined) {
                    var header = _this9.state.branddetail.payment_grid_with_code.map(function (value, i) {
                        if (_this9.state.branddetail.payment_grid_with_code.length == i + 1) {
                            return value.code;
                        } else {
                            return value.code + ", ";
                        }
                    });
                    var total = [];
                    _this9.state.branddetail.payment_grid_with_code.map(function (value, i) {
                        total.push(parseInt(value.dealvalue));
                    });
                    var sum = total.reduce(function (a, b) {
                        return a + b;
                    }, 0);
                    var termdates = null;
                    if (_this9.state.branddetail.customer_term_payment != null) {
                        termdates = _this9.state.branddetail.customer_term_payment.customer_term_payment_data.map(function (value, i) {
                            if (i != 0) {
                                return React.createElement(
                                    "tr",
                                    null,
                                    React.createElement(
                                        "td",
                                        null,
                                        ("0" + new Date(value.paymentdate).getDate()).slice(-2)
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        ("0" + (new Date(value.paymentdate).getMonth() + 1)).slice(-2)
                                    ),
                                    React.createElement(
                                        "td",
                                        null,
                                        new Date(value.paymentdate).getFullYear()
                                    )
                                );
                            }
                        });
                    }
                    if (_this9.state.branddetail.customer_term_payment == null && _this9.state.branddetail.customer_flexible_payment == null) {
                        return React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "div",
                                { className: "text-center c-white bg-orange p-2" },
                                React.createElement(
                                    "strong",
                                    null,
                                    header
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "bg-light-gray" },
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Pembayaran di Muka"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.convertToRupiah(parseInt(sum))
                                    )
                                )
                            )
                        );
                    } else if (_this9.state.branddetail.customer_term_payment != null) {
                        return React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "div",
                                { className: "text-center c-white bg-orange p-2" },
                                React.createElement(
                                    "strong",
                                    null,
                                    header
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "bg-light-gray" },
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Harga Total"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.convertToRupiah(parseInt(sum))
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Down Payment"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.state.branddetail.customer_term_payment.customer_term_payment_data != undefined ? _this9.convertToRupiah(parseInt(_this9.state.branddetail.customer_term_payment.customer_term_payment_data[0].termpaymentval)) : _this9.convertToRupiah(parseInt(sum))
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Batas Akhir Tenor"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.formatDate(new Date(_this9.state.branddetail.customer_term_payment.finishtermdate))
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Tenor"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.state.branddetail.customer_term_payment.customer_term_payment_data != undefined ? _this9.state.branddetail.customer_term_payment.customer_term_payment_data.length - 1 : 0
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Tanggal Ditagih"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2", style: { maxHeight: '200px', overflow: 'auto' } },
                                        React.createElement(
                                            "table",
                                            { className: "table table-striped table-borderless text-center bg-white" },
                                            React.createElement(
                                                "thead",
                                                { className: "bg-orange" },
                                                React.createElement(
                                                    "tr",
                                                    null,
                                                    React.createElement(
                                                        "th",
                                                        null,
                                                        "Tanggal"
                                                    ),
                                                    React.createElement(
                                                        "th",
                                                        null,
                                                        "Bulan"
                                                    ),
                                                    React.createElement(
                                                        "th",
                                                        null,
                                                        "Tahun"
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                "tbody",
                                                null,
                                                termdates
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2 border-top border-bottom border-warning" },
                                        "Tagihan Per Tenor"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2 border-top border-bottom border-warning" },
                                        React.createElement(
                                            "strong",
                                            null,
                                            _this9.convertToRupiah(_this9.state.branddetail.customer_term_payment.customer_term_payment_data !== undefined ? _this9.state.branddetail.customer_term_payment.customer_term_payment_data.length > 1 ? _this9.state.branddetail.customer_term_payment.customer_term_payment_data[1].termpaymentval : 0 : 0)
                                        )
                                    )
                                )
                            )
                        );
                    } else if (_this9.state.branddetail.customer_flexible_payment != null) {
                        return React.createElement(
                            "div",
                            null,
                            React.createElement(
                                "div",
                                { className: "text-center c-white bg-orange p-2" },
                                React.createElement(
                                    "strong",
                                    null,
                                    header
                                )
                            ),
                            React.createElement(
                                "div",
                                { className: "bg-light-gray" },
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Harga Total"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.convertToRupiah(parseInt(sum))
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Batas Awal Tenor"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.formatDate(new Date(_this9.state.branddetail.customer_flexible_payment.startflexdate))
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "row p-3" },
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-4 p-2" },
                                        "Batas Akhir Tenor"
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "col-sm-8 p-2" },
                                        _this9.formatDate(new Date(_this9.state.branddetail.customer_flexible_payment.finishflexdate))
                                    )
                                )
                            )
                        );
                    }
                }
            };

            return React.createElement(
                React.Fragment,
                null,
                loading_data ? React.createElement(
                    "div",
                    { style: { width: "300px", margin: "100px auto" } },
                    React.createElement(
                        "p",
                        { style: { textAlign: 'center' } },
                        React.createElement("i", { className: "fas fa-spinner fa-spin", style: { fontSize: '90px', textAlign: 'center' } })
                    )
                ) : React.createElement(
                    "div",
                    { className: "container content m-t-15 m-b-15" },
                    React.createElement(
                        "div",
                        { className: "row" },
                        React.createElement(
                            "div",
                            { className: "col-md-12" },
                            React.createElement(
                                "h2",
                                { className: "f-w-300" },
                                "PEMESANAN"
                            ),
                            React.createElement(
                                "div",
                                { className: "row content-booking" },
                                React.createElement(
                                    "div",
                                    { className: "col-md-6" },
                                    React.createElement(
                                        "div",
                                        { className: "bg-white p-15 m-b-15" },
                                        React.createElement(
                                            "div",
                                            { className: "row" },
                                            React.createElement(
                                                "div",
                                                { className: "col-md-6" },
                                                React.createElement(
                                                    "h2",
                                                    { className: "f-w-300" },
                                                    branddetail.eventname
                                                ),
                                                React.createElement("br", null),
                                                React.createElement(
                                                    "div",
                                                    { className: "d-flex" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement("i", { className: "fa fa-calendar-alt c-orange" })
                                                    ),
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement(
                                                            "span",
                                                            null,
                                                            this.formatDate(new Date(branddetail.startdate)),
                                                            " - ",
                                                            this.formatDate(new Date(branddetail.finishdate))
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "d-flex" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement("i", { className: "fa fa-map-marker-alt c-orange" })
                                                    ),
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement(
                                                            "span",
                                                            null,
                                                            branddetail.spacename
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "d-flex" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement("i", { className: "fa fa-dollar-sign c-orange" })
                                                    ),
                                                    React.createElement(
                                                        "div",
                                                        { className: "p-2 p-r-l" },
                                                        React.createElement(
                                                            "span",
                                                            null,
                                                            "Rp. ",
                                                            branddetail.minprice,
                                                            " - Rp. ",
                                                            branddetail.maxprice
                                                        )
                                                    )
                                                ),
                                                React.createElement("br", null),
                                                React.createElement(
                                                    "h6",
                                                    { className: "m-r-b f-s-14" },
                                                    "STAN"
                                                ),
                                                React.createElement(
                                                    "span",
                                                    { className: "c-orange t-td-u" },
                                                    React.createElement(
                                                        "div",
                                                        { id: "stan", style: { display: 'flex' } },
                                                        stan
                                                    )
                                                ),
                                                React.createElement("hr", null),
                                                branddetail.customer_brand !== null ? React.createElement(
                                                    React.Fragment,
                                                    null,
                                                    React.createElement(
                                                        "h6",
                                                        { className: "m-r-b f-s-14" },
                                                        "BRAND"
                                                    ),
                                                    React.createElement(
                                                        "span",
                                                        { className: "c-orange" },
                                                        branddetail.brandname
                                                    )
                                                ) : null,
                                                React.createElement("hr", null),
                                                React.createElement(
                                                    "h6",
                                                    { className: "m-r-b f-s-14" },
                                                    "PENGELOLA"
                                                ),
                                                React.createElement(
                                                    "span",
                                                    { className: "c-orange t-td-u" },
                                                    branddetail.namaspaceprov,
                                                    " "
                                                ),
                                                React.createElement("hr", null)
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-6" },
                                                React.createElement("img", { className: "img-fluid img-thumbnail", style: { width: '100%' }, src: endpoint_url + "/picGet/" + branddetail.eventpic }),
                                                React.createElement("br", null),
                                                React.createElement("br", null),
                                                React.createElement(
                                                    "h6",
                                                    { className: "m-r-b" },
                                                    "KONDISI & SYARAT"
                                                ),
                                                React.createElement(
                                                    "a",
                                                    { className: "c-orange t-td-u", target: "_blank", href: endpoint_url + "/fileGet/" + branddetail.termpath },
                                                    React.createElement(
                                                        "span",
                                                        null,
                                                        "LIHAT"
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "col-md-12" },
                                                React.createElement(
                                                    "h6",
                                                    null,
                                                    "TENTANG ACARA"
                                                ),
                                                React.createElement("p", { className: "f-s-14", dangerouslySetInnerHTML: { __html: branddetail.desc } })
                                            )
                                        )
                                    ),
                                    React.createElement(
                                        "div",
                                        { className: "bg-white p-15" },
                                        React.createElement(
                                            "h2",
                                            { className: "m-r-b" },
                                            "TAMBAHAN"
                                        ),
                                        React.createElement(
                                            "span",
                                            { className: "c-orange f-s-12" },
                                            React.createElement(
                                                "i",
                                                null,
                                                "Anda baru bisa memilih tambahan setelah menyelesaikan negosiasi"
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "table-responsive-md m-t-15 m-b-15" },
                                            React.createElement(
                                                "table",
                                                { className: "table" },
                                                React.createElement(
                                                    "thead",
                                                    null,
                                                    React.createElement(
                                                        "tr",
                                                        { className: "bg-orange c-white t-a-c" },
                                                        React.createElement(
                                                            "td",
                                                            null,
                                                            "JENIS"
                                                        ),
                                                        React.createElement(
                                                            "td",
                                                            null,
                                                            "HARGA"
                                                        ),
                                                        React.createElement(
                                                            "td",
                                                            null,
                                                            "SISA"
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    "tbody",
                                                    null,
                                                    ListAddon
                                                )
                                            )
                                        )
                                    )
                                ),
                                React.createElement(
                                    "div",
                                    { className: "col-md-6 p-r-l chat-content" },
                                    React.createElement(
                                        "div",
                                        { className: "bg-light-black p-15" },
                                        React.createElement(
                                            "div",
                                            { className: "b-radius-15 p-b-15 p-r-15 p-l-15 bg-dark-gray" },
                                            React.createElement(
                                                "h3",
                                                { className: "t-a-c f-w-300 c-white" },
                                                "PESAN"
                                            ),
                                            React.createElement(
                                                "div",
                                                { id: "chat_content" },
                                                React.createElement(
                                                    "ul",
                                                    { className: "nav nav-tabs tabs-pesan", id: "myTab", role: "tablist" },
                                                    React.createElement(
                                                        "li",
                                                        { className: "nav-item bg-light-gray b-radius-t-10" },
                                                        React.createElement(
                                                            "a",
                                                            { className: "nav-link active c-black", id: "home-tab", "data-toggle": "tab", href: "#chat", role: "tab", "aria-controls": "chat", "aria-selected": "true" },
                                                            "CHAT"
                                                        )
                                                    ),
                                                    React.createElement(
                                                        "li",
                                                        { className: "nav-item bg-light-gray b-radius-t-10" },
                                                        React.createElement(
                                                            "a",
                                                            { className: "nav-link c-black", id: "profile-tab", "data-toggle": "tab", href: "#dokumen", role: "tab", "aria-controls": "dokumen", "aria-selected": "false" },
                                                            "DOKUMEN"
                                                        )
                                                    ),
                                                    React.createElement(
                                                        "li",
                                                        { className: "nav-item bg-light-gray b-radius-t-10" },
                                                        React.createElement(
                                                            "a",
                                                            { className: "nav-link c-black", id: "contact-tab", "data-toggle": "tab", href: "#link", role: "tab", "aria-controls": "link", "aria-selected": "false" },
                                                            "LINK"
                                                        )
                                                    )
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "tab-content content-pesan", id: "myTabContent" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "tab-pane fade show active p-5", id: "chat", role: "tabpanel", "aria-labelledby": "chat-tab", style: { height: '400px', overflowY: 'scroll' } },
                                                        loading_chat ? React.createElement(
                                                            "p",
                                                            { style: { textAlign: 'center' } },
                                                            React.createElement("i", { className: "fas fa-spinner fa-spin", style: { fontSize: '40px', textAlign: 'center' } })
                                                        ) : ListChat
                                                    ),
                                                    React.createElement(
                                                        "div",
                                                        { className: "tab-pane fade p-5", id: "dokumen", role: "tabpanel", "aria-labelledby": "dokumen-tab", style: { height: '400px', overflowY: 'scroll' } },
                                                        ListFile
                                                    ),
                                                    React.createElement(
                                                        "div",
                                                        { className: "tab-pane fade p-5", id: "link", role: "tabpanel", "aria-labelledby": "link-tab", style: { height: '400px', overflowY: 'scroll' } },
                                                        ListLink
                                                    )
                                                ),
                                                React.createElement(
                                                    "div",
                                                    { className: "p-5 bg-white m-t-5 b-radius-10 papan-ketik-pesan" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "d-flex bd-highlight" },
                                                        React.createElement(
                                                            "div",
                                                            { className: "p-2 flex-grow-1 bd-highlight" },
                                                            React.createElement("textarea", { style: {
                                                                    borderTop: '1px solid #bebebe', borderBottom: '1px solid #bebebe',
                                                                    borderRight: '1px solid #bebebe', borderLeft: '1px solid #bebebe', borderRadius: '5px'
                                                                }, className: "form-control text-area", rows: "2", value: this.state.chat_text, onChange: this.handleChangeChat })
                                                        ),
                                                        React.createElement(
                                                            "div",
                                                            { className: "p-2 bd-highlight p-r-r" },
                                                            React.createElement(
                                                                "button",
                                                                { className: "link" },
                                                                React.createElement("i", { className: "fa fa-link c-orange" })
                                                            )
                                                        ),
                                                        React.createElement(
                                                            "div",
                                                            { className: "p-2 bd-highlight p-r-r" },
                                                            React.createElement(
                                                                "button",
                                                                { className: "link" },
                                                                React.createElement("i", { className: "fa fa-paperclip c-orange" })
                                                            )
                                                        ),
                                                        React.createElement(
                                                            "div",
                                                            { className: "p-2 bd-highlight" },
                                                            this.state.loading ? React.createElement("i", { className: "fas fa-spinner fa-spin", style: { fontSize: '30px' } }) : React.createElement(
                                                                "button",
                                                                { className: "link bg-orange", onClick: function onClick() {
                                                                        return _this9.sendChat();
                                                                    } },
                                                                React.createElement("i", { className: "fa fa-paper-plane c-white" })
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            React.Fragment,
                                            null,
                                            React.createElement(
                                                "div",
                                                { className: "table-responsive m-t-15" },
                                                React.createElement(
                                                    "table",
                                                    { className: "table t-a-c m-r-b" },
                                                    React.createElement(
                                                        "thead",
                                                        null,
                                                        React.createElement(
                                                            "tr",
                                                            { className: "bg-orange c-white" },
                                                            React.createElement(
                                                                "td",
                                                                null,
                                                                "NO. POSISI"
                                                            ),
                                                            React.createElement(
                                                                "td",
                                                                null,
                                                                "HARGA"
                                                            )
                                                        )
                                                    ),
                                                    React.createElement(
                                                        "tbody",
                                                        null,
                                                        posisi
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "table-responsive" },
                                                React.createElement(
                                                    "table",
                                                    { className: "table m-r-b" },
                                                    React.createElement(
                                                        "tbody",
                                                        null,
                                                        React.createElement(
                                                            "tr",
                                                            { className: "bg-gray c-white" },
                                                            React.createElement(
                                                                "td",
                                                                null,
                                                                "TOTAL STAN"
                                                            ),
                                                            React.createElement(
                                                                "td",
                                                                { className: "t-a-r" },
                                                                this.state.total_stan
                                                            )
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement(
                                                "div",
                                                { className: "table-responsive" },
                                                React.createElement(
                                                    "table",
                                                    { className: "table m-r-b" },
                                                    React.createElement(
                                                        "tbody",
                                                        null,
                                                        React.createElement(
                                                            "tr",
                                                            { className: "bg-light-black c-white" },
                                                            React.createElement(
                                                                "td",
                                                                { className: "b-r-top" },
                                                                React.createElement(
                                                                    "i",
                                                                    null,
                                                                    "LAIN LAIN",
                                                                    React.createElement("br", null),
                                                                    "DEPOSIT"
                                                                )
                                                            ),
                                                            React.createElement(
                                                                "td",
                                                                { className: "t-a-r b-r-top" },
                                                                this.convertToRupiah(parseInt(branddetail.otherfeevalue)),
                                                                React.createElement("br", null),
                                                                this.convertToRupiah(parseInt(branddetail.depositvalue))
                                                            )
                                                        )
                                                    )
                                                )
                                            ),
                                            React.createElement("hr", { className: "b-orange m-r" }),
                                            React.createElement(
                                                "div",
                                                { className: "table-responsive" },
                                                React.createElement(
                                                    "table",
                                                    { className: "table m-r-b" },
                                                    React.createElement(
                                                        "tbody",
                                                        null,
                                                        React.createElement(
                                                            "tr",
                                                            { className: "bg-light-black c-white" },
                                                            React.createElement(
                                                                "td",
                                                                { className: "b-r-top" },
                                                                React.createElement(
                                                                    "h6",
                                                                    { className: "m-r-b" },
                                                                    "TOTAL TAGIHAN"
                                                                )
                                                            ),
                                                            React.createElement(
                                                                "td",
                                                                { className: "t-a-r b-r-top" },
                                                                this.state.total_amount
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        React.createElement("hr", { className: "b-orange m-r" }),
                                        React.createElement(
                                            "div",
                                            { className: "table-responsive" },
                                            React.createElement(
                                                "table",
                                                { className: "table m-r-b" },
                                                React.createElement(
                                                    "tbody",
                                                    null,
                                                    React.createElement(
                                                        "tr",
                                                        { className: "bg-light-black c-white" },
                                                        React.createElement(
                                                            "td",
                                                            { className: "b-r-top" },
                                                            React.createElement(
                                                                "span",
                                                                { className: "t-td-u f-s-12" },
                                                                React.createElement(
                                                                    "a",
                                                                    { className: "c-orange", "data-toggle": "modal", href: "#detail-cicilan" },
                                                                    React.createElement(
                                                                        "i",
                                                                        null,
                                                                        "detail cicilan"
                                                                    )
                                                                )
                                                            )
                                                        )
                                                    )
                                                )
                                            )
                                        ),
                                        React.createElement(
                                            "div",
                                            { className: "modal", id: "detail-cicilan" },
                                            React.createElement(
                                                "div",
                                                { className: "modal-dialog" },
                                                React.createElement(
                                                    "div",
                                                    { className: "modal-content" },
                                                    React.createElement(
                                                        "div",
                                                        { className: "modal-body p-0" },
                                                        DetailCicilan()
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            );
        }
    }]);

    return Chat;
}(React.Component);

ReactDOM.render(React.createElement(Chat, null), document.getElementById('chat_content'));

/***/ })

/******/ });