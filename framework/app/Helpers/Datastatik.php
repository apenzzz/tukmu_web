<?php

namespace App\Helpers;


use GuzzleHttp\Client;
use GuzzleHttp\Promise;


class Datastatik{

    public function __construct()
    {
        $this->client = new Client(['base_uri' => $this->pointing()]);
    }

    public function pointing(){

        $point = env('APP_ENDPOINT', 'https://tukbase3.tukmu.com/');
        return $point;
    }

}
