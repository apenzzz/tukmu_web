<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class COPendaftaran extends Controller
{
    public $datastatic;
    public $client;

    public $end_week;
    public $start_week;
    public $last_day;

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }
    

    public function registrasi(){

        $data = [];
        
        return view('booking.chat-booking')->with($data);
    }

    public function login_pengguna(Request $request){

        $email = $request->get('email_peng');
        $password = $request->get('password_peng');

        $promises = [
            'login' => $this->client->postAsync('api/customer/login', [
                'form_params' => [
                    'email' => $email,
                    'password' => $password,
                ]
            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'login' => json_decode($results['login']['value']->getBody())
        );

        $data = array(
            'login' => $hasilapi['login']
        );

        //dd($data);
        if($hasilapi['login']->status == 1){
            $request->session()->put('my_token___', $hasilapi['login']->token);
            if(!empty($hasilapi['login']->cust_data)) {
                $request->session()->put('id_user__', $hasilapi['login']->cust_data[0]->picid);
                $request->session()->put('email__', $hasilapi['login']->cust_data[0]->email);
                $request->session()->put('name__', $hasilapi['login']->cust_data[0]->name);
                $request->session()->put('telp__', $hasilapi['login']->cust_data[0]->telp);
                $request->session()->put('address__', $hasilapi['login']->cust_data[0]->address);
                $request->session()->put('cityname__', $hasilapi['login']->cust_data[0]->cityname);
                $request->session()->put('province__', $hasilapi['login']->cust_data[0]->province);
            }

            return redirect('/');
        } else {
            return redirect('/')->with('error_code', 1);
        }   

    }

    public function registrasiproses(Request $request) {

        $email = $request->input('email');
        $name = $request->input('name');
        $telp = $request->input('notelp__');
        $password = $request->input('password');

        // $email = $request->input('pic_email');
        // $name = $request->input('pic_name');
        // $telp = $request->input('pic_telp');
        // $password = $request->input('pic_password');

        $promises = [
            'registrasi' => $this->client->postAsync('api/customer/register', [
                'form_params' => [
                    'pic_email' => $email,
                    'pic_name' => $name,
                    'telp' => $telp,
                    'pic_password' => $password,
                    'city' => 1,
                    'address' => "null"
                 ]
             ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'registrasi' => json_decode($results['registrasi']['value']->getBody()),
        );

        // return response($hasilapi['registrasi']);
        // dd($hasilapi['registrasi']);
        if($hasilapi['registrasi']->status == "1"){
            return view('home.success_registrasi');
        } else {
            return view('home.failed_registrasi');
        }
        // return response()->json($hasilapi['registrasi']);
    }

    public function logout(Request $request){
        
        $request->session()->forget('my_token___');
        $request->session()->forget('id_user__');
        $request->session()->forget('email__');
        $request->session()->forget('name__');
        $request->session()->forget('notelp__');
        // $request->session()->forget('chat_session__');

        $request->session()->flush();

        $data = [];

        return redirect('/');
        // return view('home.index')->with($data);
    }

    public function getCityList(Request $request){
        $promises = [
            'citylist' => $this->client->getAsync('api/event/list/city/only')
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'citylist' => json_decode($results['citylist']['value']->getBody()),
        );

        $data = array(
            'citylist' => $hasilapi['citylist']
        );

        //dd($hasilapi['userdata']);
        return view('user.user_register_profile')->with($data);
    }

    public function userEditDataProses(Request $request){
        $promises = [
            'useredit' => $this->client->postAsync('api/customer/edit/data', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],
                
                'form_params' => [
                    'pic_password' => $request->input('passwordrp'),
                    'pic_name' => $request->input('namerp'),
                    'pic_telp' => $request->input('telpnumb'),
                    'city' => $request->input('city'),
                    'address' => $request->input('address'),
                ]

                
            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'useredit' => json_decode($results['useredit']['value']->getBody())
        );

        
        if($hasilapi['useredit']->status == "1"){
            return redirect()->back()->with('message', 'Data berhasil di ubah!');
        } else {
            return view('home.failed_registrasi');
        }
    }

    public function editCompanyData(){

        return view('user.user_register_company');
    }

    public function addBrandData(){

        $promises = [
            'categorylist' => $this->client->getAsync('api/customer/view/brand/category')
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'categorylist' => json_decode($results['categorylist']['value']->getBody()),
        );

        $data = array(
            'categorylist' => $hasilapi['categorylist']
        );

        return view('user.user_register_brand')->with($data);
    }

     public function addBrandDataProses(Request $request){
        $promises = [
            'brandadd' => $this->client->postAsync('api/customer/add/brand', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],
                
                'form_params' => [
                    'brand_name' => $request->input('brandname'),
                    'brand_desc' => $request->input('branddesc'),
                    'brand_hp' => $request->input('brandhp'),
                    'brand_category' => $request->input('brandcat'),
                    'website' => $request->input('brandweb'),
                    'fb' => $request->input('fb'),
                    'twitter' => $request->input('tw'),
                    'instagram' => $request->input('ig'),
                    'register_name' => $request->input('regname'),
                    'npwp' => $request->input('npwp'),
                    'bankaccountno' => $request->input('bankaccountno'),
                    'bankaccountname' => $request->input('bankaccountname'),
                    'bankname' => $request->input('bankname'),
                    'bankbranch' => $request->input('bankbranch'),
                    'brand_display[0][display_photo]' => $request->input('branddispic'),
                    'brand_display[0][mediadesc]' => $request->input('branddisdesc'),
                
            ]])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'brandadd' => json_decode($results['brandadd']['value']->getBody())
        );

        //dd($hasilapi['brandadd']);
        if($hasilapi['brandadd']->status == "1"){
            $redirecturl = $request->input('redirecturl');
            if(isset($redirecturl))
            {
                return redirect($redirecturl);
            }
            else
            {
                return redirect()->back()->with('message-success', 'Data BERHASIL di ditambahkan! Terimakasih!');
            }
        } else {
            $errorstring = "";
            foreach($hasilapi['brandadd']->error_codes as $eacherror) {
                if($eacherror == "BRAND_HP_MUST_NUM") {
                    $eacherror = "Nomor HP yang dimasukkan bukan nomor";
                }
                else if($eacherror == "BRAND_HP_MUST_NUM_MIN 10") {
                    $eacherror = "Nomor HP minimal 10 digit";
                }

                $errorstring .= $eacherror . ". ";
            }

            return redirect()->back()->with('message-failed', 'Data GAGAL di ditambahkan! ' . $errorstring);
        }
    }

}
