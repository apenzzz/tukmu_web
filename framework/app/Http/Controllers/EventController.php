<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

use Mapper;

class EventController extends Controller
{

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function detail($id){
        
        $promises = [
            'detail' => $this->client->getAsync('api/event/view/detail?sid='.$id)
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'detail' => json_decode($results['detail']['value']->getBody())
        );

        $data = array(
            'detail' => $hasilapi['detail']
        );
        
        //dd($data['detail']->event_data[0]);
        return view('event.event_profile')->with($data);
    }
}
