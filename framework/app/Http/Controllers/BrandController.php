<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class BrandController extends Controller
{

	public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function viewBrandDetail(Request $request, $id){
        $promises = [
            'viewbrand' => $this->client->getAsync('api/customer/view/brand?brand_id='.$id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'viewbrand' => json_decode($results['viewbrand']['value']->getBody())
        );

        $data = array(
            'viewbrand' => $hasilapi['viewbrand']
        );

        //dd($hasilapi['viewbrand']);

        if ($hasilapi['viewbrand']->status == 1) {
            return view('brand.brand_detail')->with($data);
        } else {
            return redirect('/');
        }
    }

    public function editBrandDetail(Request $request, $id){
        $promises = [
            'viewbrand' => $this->client->getAsync('api/customer/view/brand?brand_id='.$id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ]),
            'categorylist' => $this->client->getAsync('api/customer/view/brand/category')
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'viewbrand' => json_decode($results['viewbrand']['value']->getBody()),
            'categorylist' => json_decode($results['categorylist']['value']->getBody())
        );

        $data = array(
            'viewbrand' => $hasilapi['viewbrand'],
            'categorylist' => $hasilapi['categorylist']
        );

        //dd($hasilapi['userdata']);

        if ($hasilapi['viewbrand']->status == 1) {
            return view('brand.brand_edit')->with($data);
        } else {
            return redirect('/');
        }
    }

    public function editBrandProses(Request $request) {
    	$promises = [
            'editbrand' => $this->client->postAsync('api/customer/edit/brand', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],

                'form_params' => [
                	'brand_id' => $request->input('brandid'),
					'brand_name' => $request->input('brandname'),
					'brand_desc' => $request->input('branddesc'),
					'brand_hp' => $request->input('brandhp'),
					'brand_category' => $request->input('brandcat'),
					'website' => $request->input('brandweb'),
					'fb' => $request->input('fb'),
					'twitter' => $request->input('tw'),
					'instagram' => $request->input('ig'),
					'register_name' => $request->input('regname'),
					'npwp' => $request->input('npwp'),
                 ]

            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'editbrand' => json_decode($results['editbrand']['value']->getBody())
        );

        $data = array(
            'editbrand' => $hasilapi['editbrand']
        );

        //dd($hasilapi['editbrand']);

        if($hasilapi['editbrand']->status == "1"){
            return redirect('brand-detail/'.$request->input('brandid'))->with('message-success', 'Data BERHASIL di ubah!');
        } else {
            return redirect('brand-detail/'.$request->input('brandid'))->with('message-failed', 'Data GAGAL di ubah!');
        }
    }

    public function deleteBrandProses($id, Request $request) {
        $promises = [
            'deletebrand' => $this->client->postAsync('api/customer/delete/brand', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],

                'form_params' => [
                    'brand_id' => $id,
                 ]

            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'deletebrand' => json_decode($results['deletebrand']['value']->getBody())
        );

        $data = array(
            'deletebrand' => $hasilapi['deletebrand']
        );

        //dd($hasilapi['deletebrand']);
        return redirect()->back()->with('message-success', 'Data berhasil di hapus!');
    }
}
