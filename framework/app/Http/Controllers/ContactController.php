<?php
namespace App\Http\Controllers;

class ContactController extends Controller {
    public function SpaceProvider($param = '') {
        $data['param'] = $param;
        return view('contact.spaceprovider', $data);
    }

    public function Admin($param = '') {
        $data['param'] = $param;
        return view('contact.admin', $data);
    }

    public function ListAduanSP() {
        return view('contact.list_aduan_sp');
    }

    public function ListAduanAdmin() {
        return view('contact.list_aduan_admin');
    }

    public function ViewAduanSP($caseid) {
        $data['caseid'] = $caseid;
        return view('contact.view_aduan_sp', $data);
    }

    public function ViewAduanAdmin($caseid) {
        $data['caseid'] = $caseid;
        return view('contact.view_aduan_admin', $data);
    }
}
