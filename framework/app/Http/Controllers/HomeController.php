<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Mapper;

class HomeController extends Controller {
    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function About() {
        return view('home.about');
    }

    public function Message(Request $request) {
        return view('home.message');
    }
}
