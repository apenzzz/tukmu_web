<?php
namespace App\Http\Controllers;

class RefundController extends Controller {
    public function List() {
        return view('refund.list');
    }
    public function Select() {
        return view('refund.select');
    }
    public function Create($cpyid) {
        $data['cpyid'] = $cpyid;
        return view('refund.create', $data);
    }
}
