<?php

namespace App\Http\Controllers;

class PaymentController extends Controller {
    public function History($cpyid) {
        $data['cpyid'] = $cpyid;
        return view('payment.history', $data);
    }

    public function Print($cpyid) {
        $data['cpyid'] = $cpyid;
        return view('payment.print', $data);
    }

    public function UploadReceipt($cpyid) {
        $data['cpyid'] = $cpyid;
        return view('payment.uploadreceipt', $data);
    }
}
