<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class TransactionController extends Controller
{

	public $datastatic;
    public $client;

    public $end_week;
    public $start_week;
    public $last_day;

	 public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function SelectPaymentMethod($id, Request $request){
    	 $promises = [
            'paymentsummary' => $this->client->getAsync('api/customer/view/order/payment/summary?cpyid='.$id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
            ]),
            'paymentmethod' => $this->client->getAsync('api/customer/payment/method', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
            ]),
            'paymentterm' => $this->client->getAsync('api/customer/view/payment/term?cpyid=' . $id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
            ]),
            
        ];

        $results = Promise\unwrap($promises);
        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'paymentsummary' => json_decode($results['paymentsummary']['value']->getBody()),
            'paymentmethod' => json_decode($results['paymentmethod']['value']->getBody()),
            'paymentterm' => json_decode($results['paymentterm']['value']->getBody()),
        );

        $data = array(
            'paymentsummary' => $hasilapi['paymentsummary'],
            'paymentmethod' => $hasilapi['paymentmethod'],
            'paymentterm' => $hasilapi['paymentterm'],
        );
        
        // dd($data);

        if($data['paymentterm']->status == "0") {
            // pembayaran di muka
            return view('payment.payment_method')->with($data);
        }
        else {
            // pembayaran cicilan, maka redirect
            $data['cpyid'] = $id;
            return view('payment.history', $data);
        }
    }

    public function SubmitPaymentMethod(Request $request){
        $promises1 = [
            'paymentproses' => $this->client->postAsync('api/customer/add/order/payment/method', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ],
                'form_params' => [
                    'cpyid' => $request->cpyid,
                    'method' => $request->paymentmethod,
                ]
            ])
        ];
        $results1 = Promise\unwrap($promises1);
        $results1 = Promise\settle($promises1)->wait();

        $promises2 = [
            'paymentsummary' => $this->client->getAsync('api/customer/view/order/payment/summary?cpyid=' . $request->cpyid, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
            ])
        ];
        $results2 = Promise\unwrap($promises2);
        $results2 = Promise\settle($promises2)->wait();

        $hasilapi = array(
            'paymentproses' => json_decode($results1['paymentproses']['value']->getBody()),
            'paymentsummary' => json_decode($results2['paymentsummary']['value']->getBody())
        );

        $data = array(
            'paymentproses' => $hasilapi['paymentproses'],
            'paymentsummary' => $hasilapi['paymentsummary']
        );

        // dd($data);

        //TODO: nanti rubah jangan hardcode
        if(!is_null($request->espayredirect) && $request->espayredirect == "1") {
            return redirect(env('ESPAY_URL') . '?url=' . env('APP_URL') . '/booking-complete/' . $request->cpyid . '&key=e08e895b1666f60eedc2a66a7ed849e5&paymentId=' . $request->cpyid . '&paymentAmount=' . $data['paymentsummary']->grand_total_payment . '&commCode=SGWTUKMU&bankCode=' . $request->bankcode . '&productCode=' . $request->productcode);
        }
        else {
            return view('payment.confirmation')->with($data);   
        }
    }

    public function SubmitUploadReceipt(Request $request) {
        $filebuktibayar = Input::file('filebuktibayar');
        
        $promises = [
            'submituploadreceipt' => $this->client->postAsync('api/customer/add/payment/transfer/attach', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ],
                'multipart' => [
                    [
                        'name' => 'cpyid',
                        'contents' => $request->cpyid
                    ],
                    [
                        'name' => 'bank',
                        'contents' => $request->namabank
                    ],
                    [
                        'name' => 'accname',
                        'contents' => $request->namaakun
                    ],
                    [
                        'name' => 'transfer_attach',
                        'contents' => file_get_contents($filebuktibayar->getRealPath()),
                        'filename' => $filebuktibayar->getClientOriginalName()
                    ]
                ]
            ])
        ];
        $results = Promise\unwrap($promises);
        $results = Promise\settle($promises)->wait();
        $hasilapi = array(
            'submituploadreceipt' => json_decode($results['submituploadreceipt']['value']->getBody())
        );
        $data = array(
            'submituploadreceipt' => $hasilapi['submituploadreceipt']
        );

        //dd($data);

        if($data['submituploadreceipt']->status == 1) {
            return redirect('booking-complete/' . $request->cpyid);
        }
        else {
            $errordata = array(
                'error' => $data['submituploadreceipt']->error
            );
            return view('common.error')->with($errordata);
        }
    }
}
