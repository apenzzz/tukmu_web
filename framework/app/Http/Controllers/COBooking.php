<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use Session;

class COBooking extends Controller
{
    public $datastatic;
    public $client;

    public $end_week;
    public $start_week;
    public $last_day;

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }
    

    public function index($cpid, $id_brand, Request $request){

        $promises = [
            'brand' => $this->client->postAsync('api/customer/order/space/brand', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],

                'form_params' => [
                    'cpyid' => $cpid,
                    'brand_id'=> $id_brand,
                 ]
             ]),

            'branddetail' => $this->client->getAsync('api/customer/view/order/booking/pending/spec?cpyid='.$cpid, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
             ]),

            'chat_init' => $this->client->postAsync('api/customer/add/nego/chat/init', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],
                'form_params' => [
                    'register_id' => 'AAAA2FwbwdM:APA91bFPqMP4e2IdJ3EB_bx_8hKRnSWa3Uc_FXu0o_cuo69H14N4DvYd70yNoCiIcXnw86J1OBxcs4ng_BJK7YREukI8hoPnp9t9O49_5LetUkVLPEdpECHlygNrWf4Xm3JaRvTf2Xre',
                    'authenticator' => 'browser',
                    'cpyid' => $cpid,
                ]
            ])

        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'brand' => json_decode($results['brand']['value']->getBody()),
            'branddetail' => json_decode($results['branddetail']['value']->getBody()),
            'chat_init' => json_decode($results['chat_init']['value']->getBody())
        );

         $data = array(
            'id' => $cpid,
            'brand_id' => $id_brand,
            'brand' => $hasilapi['brand'],
            'branddetail' => $hasilapi['branddetail'],
            'chat_init' => $hasilapi['chat_init']
        );

        if($hasilapi['chat_init']->status == "1"){
            $request->session()->put('chat_session__', $hasilapi['chat_init']->token->chat_session_token);
        }
        
        // dd($data);
        // return view('booking.test_chat_embed')->with($data);
        return view('booking.chat-booking')->with($data);
    }

    public function tambahan($deal, $chatsession, $cpyid, Request $request){

        
        $promises = [
            'nego' => $this->client->postAsync('api/customer/add/nego/deal/button/'.$deal, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                    'chat-session' => "Bearer ".$chatsession
                ],

                'form_params' => [
                    'cpyid' => $cpyid,
                 ]
             ]),
             'addon' => $this->client->getAsync('api/event/view/detail/addon?cpyid='.$cpyid, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
             ]),
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'nego' => json_decode($results['nego']['value']->getBody()),
            'addon' => json_decode($results['addon']['value']->getBody()),
        );

         $data = array(
            'nego' => $hasilapi['nego'],
            'addon' => $hasilapi['addon'],
            'cpyid' => $cpyid
        );
        
        // dd($data);
        // dd($request->session()->get('my_token___'));
        return view('booking.tambahan')->with($data);
    }

    public function tambahanProses($cpyid, Request $request){
        
        $promises = [
            'tambahanproses' => $this->client->postAsync('api/customer/add/order/addon', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ],

                'form_params' => [
                    'cpyid' => $cpyid,
                    'addon[0][addon_id]' => $request->input(),
                    'addon[0][addon_order]' => $request->input(),
                    'addon[0][addon_note]' => $request->input(),
                 ]
             ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'tambahanproses' => json_decode($results['tambahanproses']['value']->getBody())
        );

         $data = array(
            'tambahanproses' => $hasilapi['tambahanproses']
        );
        
        // dd($data);
        // dd($request->session()->get('my_token___'));
        return view('booking.tambahan')->with($data);
    }

    public function complete($id, Request $request){
        $promises = [
             'bookingcomplete' => $this->client->getAsync('api/customer/view/order/payment/summary?cpyid='.$id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                ]
             ]),
        ];

        $results = Promise\unwrap($promises);
        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'bookingcomplete' => json_decode($results['bookingcomplete']['value']->getBody())
        );

         $data = array(
            'bookingcomplete' => $hasilapi['bookingcomplete']
        );
        
        //dd($data);
        return view('booking.complete')->with($data);
    }

    public function orderBookingProses(Request $request){

        // $spid = $request->get('paymentid');
        // $grid_selected = value('SGI20181011000000042%2300000A8');
        // $startbookingdate = $request->get('startbookingdate');
        // $lastbookingdate = $request->get('lastbookingdate');


        $datafix = [];
        for($i = 0; $i < count($request->input('gridselected')); $i++){
            $dataslice = explode("|",$request->input('gridselected')[$i]);
            array_push($datafix, $dataslice[0]);
        }
        $dataevent = implode(",", $datafix);
        $spid = $request->input('spaceid');
        $cekusertoken = $request->input('cektoken');

        $promises = [
            'orderbooking' => $this->client->postAsync('api/customer/order/space/booking', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ],

                'form_params' => [
                    'spid' => $spid,
                    'grid_selected'=> $dataevent,
                    'start_booking_date'=> $request->input('startbookingdate'),
                    'finish_booking_date' => $request->input('lastbookingdate'),
                 ]
             ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'orderbooking' => json_decode($results['orderbooking']['value']->getBody())
        );

         $data = array(
            'orderbooking' => $hasilapi['orderbooking']
        );
        
        //dd($dataevent);
        // dd($data);
          if (($hasilapi['orderbooking']->status) == 0) {
             return redirect()->back()->with('error_code', 5);
          } else {
             return redirect('select-brand/'.$hasilapi['orderbooking']->custpaymentid . '/' . $spid);
          }
    }

    public function selectBrand($id, $space_id, Request $request){
        $promises = [
            'displaybrand' => $this->client->getAsync('api/customer/view/brand/all', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ]),
            'event_detail' => $this->client->getAsync('api/event/view/detail?sid='.$space_id, [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]
            ])
        ];

        $results = Promise\unwrap($promises);
        $results = Promise\settle($promises)->wait();
        $hasilapi = array(
            'displaybrand' => json_decode($results['displaybrand']['value']->getBody()),
            'event_detail' => json_decode($results['event_detail']['value']->getBody())
        );
         $data = array(
            'displaybrand' => $hasilapi['displaybrand'],
            'id' => $id,
            'event_detail' => $hasilapi['event_detail']->event_data[0]
        );

        //dd($data);
        return view('booking.select_brand')->with($data);
    }

    public function dealNegoProses($cpid, Request $request){
        $promises = [
            'dealnego' => $this->client->getAsync('api/customer/add/nego/deal/button/{deal}', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___')
                    // 'chat-session' => "Bearer ".$request->session()->get('chat-session')
                ],
                // 'form_params' => [
                //     'cpyid' => $cpid
                //  ]

            ])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'dealnego' => json_decode($results['dealnego']['value']->getBody())
        );

         $data = array(
            'dealnego' => $hasilapi['dealnego']
        );

        // dd($data);
         return view('booking.tambahan')->with($data);
    }
}
