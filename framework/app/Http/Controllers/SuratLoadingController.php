<?php

namespace App\Http\Controllers;

class SuratLoadingController extends Controller {
    public function List() {
        return view('suratloading.list');
    }

    public function Create($cpyid) {
        $data['cpyid'] = $cpyid;
        return view('suratloading.create', $data);
    }
}
