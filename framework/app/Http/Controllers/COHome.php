<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class COHome extends Controller
{
    public $datastatic;
    public $client;

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function index(Request $request){

        $promises = [
            'category' => $this->client->getAsync('api/event/list/category'),
            'upcomingevent' => $this->client->getAsync('api/event/list/promoted')
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'category' => json_decode($results['category']['value']->getBody()),
            'upcomingevent' => json_decode($results['upcomingevent']['value']->getBody()),
        );

        $data = array(
            'category' => $hasilapi['category'],
            'upcomingevent' => $hasilapi['upcomingevent']
        );
        
        return view('home.index')->with($data);
    }
}
