<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchResult(){
    	return view('user.user_search_result');
    }
}
