<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Helpers\Datastatik;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;

class StatusController extends Controller
{

	public $datastatic;
    public $client;

    public function __construct()
    {
        $this->datastatic = new Datastatik();
        $this->client = new Client(['base_uri' => $this->datastatic->pointing()]);
    }

    public function pendingOrder(Request $request){
        if (session()->get('my_token___') == null) {
            return redirect('/')->with('error_code', 5);
        } else {
            return view('status.pending_order');
        }
    }

     public function cicilan(Request $request){

        return view('status.cicilan');
    }

     public function tagihan(Request $request){

        $promises = [
            'tagihan' => $this->client->getAsync('api/customer/view/order/payment/pending/all', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'tagihan' => json_decode($results['tagihan']['value']->getBody())
        );

        $data = array(
            'tagihan' => $hasilapi['tagihan']
        );

        //dd($data);
        if (session()->get('my_token___') == null) {
            return redirect('/')->with('error_code', 5);
        } else {
            return view('status.tagihan')->with($data);
        }
    }

     public function lunas(Request $request){

        $promises = [
            'lunas' => $this->client->getAsync('api/customer/view/order/payment/payed/all', [
                'headers' => [
                    'Authorization' => "Bearer ".$request->session()->get('my_token___'),
                ]])
        ];

        $results = Promise\unwrap($promises);

        $results = Promise\settle($promises)->wait();

        $hasilapi = array(
            'lunas' => json_decode($results['lunas']['value']->getBody())
        );

        $data = array(
            'lunas' => $hasilapi['lunas']
        );

        //dd($data);
        if (session()->get('my_token___') == null) {
            return redirect('/')->with('error_code', 5);
        } else {
            return view('status.lunas')->with($data);
        }
    }
}
