<?php

Route::get('/', 'COHome@index');

Route::get('hasil-pencaharian', 'SearchController@searchResult');

Route::get('cekemail-registrasi', function () {
    return view('home.success_registrasi');
});

// Home
Route::get('/about', 'HomeController@About');
Route::get('/pesan', 'HomeController@Message');

// Contact
Route::get('/contact/spaceprovider/list', 'ContactController@ListAduanSP');
Route::get('/contact/spaceprovider/view/{caseid}', 'ContactController@ViewAduanSP');
Route::get('/contact/spaceprovider/{param?}', 'ContactController@SpaceProvider')->where('param', '.*');
Route::get('/contact/admin/list', 'ContactController@ListAduanAdmin');
Route::get('/contact/admin/view/{caseid}', 'ContactController@ViewAduanAdmin');
Route::get('/contact/admin/{param?}', 'ContactController@Admin')->where('param', '.*');

// TRANSACTIONS / BOOKING
Route::post('/detail-event/orderBookingProses', 'COBooking@orderBookingProses');
Route::get('detail-event/{id}', 'EventController@detail');
Route::get('select-brand/{custpaymentid}/{spaceid}', 'COBooking@selectBrand');
Route::get('/booking/{id}/{id_brand}', 'COBooking@index');
Route::get('/tambahan/{deal}/{session_chat}/{cpyid}', 'COBooking@tambahan');
Route::post('tambahanProses', 'COBooking@tambahanProses');
Route::get('metode-pembayaran/{id}', 'TransactionController@SelectPaymentMethod');
Route::post('metode-pembayaran/submitpaymentmethod', 'TransactionController@SubmitPaymentMethod');
Route::get('/booking-complete/{id}', 'COBooking@complete');
Route::post('payment/confirmation/submituploadreceipt', 'TransactionController@SubmitUploadReceipt');

// STATUS TRANSACTION
Route::get('pending-order', 'StatusController@pendingOrder');
Route::get('cicilan', 'StatusController@cicilan');
Route::get('tagihan', 'StatusController@tagihan');
Route::get('lunas', 'StatusController@lunas');

// USER NEEDS
Route::post('registrasi', 'COPendaftaran@registrasiproses');
Route::post('dologin_pengguna', 'COPendaftaran@login_pengguna');
Route::post('detail-event/dologin_pengguna', 'COPendaftaran@login_pengguna');
Route::get('logout', 'COPendaftaran@logout');

Route::get('profil-pengguna', 'UserController@index');
Route::get('daftar-profil-pengguna', 'COPendaftaran@getCityList');
Route::post('editDataProses', 'COPendaftaran@userEditDataProses');

Route::get('daftar-perusahaan-pengguna', 'COPendaftaran@editCompanyData');
Route::post('editDataProses', 'COPendaftaran@userEditDataProses');

Route::get('daftar-brand-pengguna', 'COPendaftaran@addBrandData');
Route::post('addBrandDataProses', 'COPendaftaran@addBrandDataProses');

//BRAND
Route::get('brand-detail/{id}', 'BrandController@viewBrandDetail');
Route::get('brand-edit/{id}', 'BrandController@editBrandDetail');
Route::get('deleteBrandProses/{id}', 'BrandController@deleteBrandProses');
Route::post('brand-edit/editBrandProses', 'BrandController@editBrandProses');

// Surat loading
Route::get('transaksi/loading/list', 'SuratLoadingController@List');
Route::get('transaksi/loading/create/{cpyid}', 'SuratLoadingController@Create');

// Refund
Route::get('refund/list', 'RefundController@List');
Route::get('refund/select', 'RefundController@Select');
Route::get('refund/create/{cpyid}', 'RefundController@Create');

// Payment
Route::get('payment/history/{cpyid}', 'PaymentController@History');
Route::get('payment/print/{cpyid}', 'PaymentController@Print');

