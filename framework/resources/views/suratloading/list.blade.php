@extends('layouts.master')
@section('st_css')
	<style type="text/css">
		.jumbotron {
			padding: 3%;
		}
	</style>
@stop
@section('content')
  @include('common.header')
  <div class="row">
    <div class="col-md-1">
    </div>
    <div class="col-md-5">
      <br>
      <br>
      <h2>PEMESANAN</h2>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <h2 style="color: darkorange;">ABAH GANI</h2>
          <br>
          <div class="row">
            <div class="col-md-12">
              <h4>INDEPENDENCE DAY</h4>
            </div>
            <div class="col-md-6">
              <img src="{!! asset('assets/master/img/search-engine/date.png') !!}" width="20" height="20">&nbsp;<span>17 - 20 Agustus 2019</span>
              <hr>
            </div>
            <div class="col-md-6">
              <img src="{!! asset('assets/master/img/search-engine/location.png') !!}" width="20" height="20">&nbsp;<span>PLAZA INDONESIA</span>
              <hr>
            </div>
            <div class="col-md-6">
              <p><strong>PENANGGUNG JAWAB</strong></p>
              <p style="color: darkorange; margin-top: -5%;">ANDREAS GANI</p>
              <p style="color: darkorange; margin-top: -8%;">0899-0090-0000</p>
              <hr>
              <p><strong>ACARA</strong></p>
              <p style="color: darkorange; margin-top: -5%;">INDEPENDENCE DAY</p>
              <hr>
              <p><strong>STAN</strong></p>
              <p style="color: darkorange; margin-top: -5%;">A1, A2</p>
              <hr>
              <p><strong>PENGELOLA</strong></p>
              <p style="color: darkorange; margin-top: -5%;">PT. LININDO JAKARTA</p>
              <hr>
            </div>
            <div class="col-md-6" style="margin-top: 5%;">
              <img src="{!! asset('assets/master/img/sample.png') !!}" width="230" height="230">
            </div>
            <div class="col-md-12">
              <p><strong>TENTANG ACARA</strong></p>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-5">
      <br>
      <br>
      <h2>&nbsp;</h2>
      <div class="jumbotron jumbotron-fluid">
        <div class="container">
          <div class="row" class="form-group">
            <div class="col-md-12">
              <h2>DAFTAR BARANG</h2>
              <p style="color: darkorange;"><i>Pastikan semua barang yang diperlukan telah tercantum</i></p>
            </div>
            <div class="col-md-12">
              <table style="text-align: center;">
                <thead>
                  <tr style="background-color: darkorange; color: white;">
                    <th width="450">JENIS</th>
                    <th width="150">JUMLAH</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td colspan="2" style="background-color: white;" height="2"></td>
                  </tr>
                  <tr>
                    <td style="background-color: #ffe0b2;">test barang</td>
                    <td style="background-color: orange;"  >5</td>
                  </tr>
                  <tr>
                    <td colspan="2" style="background-color: white;" height="2"></td>
                  </tr>
                  <tr>
                    <td style="background-color: #ffe0b2;">test barang 2</td>
                    <td style="background-color: orange;"  >9</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-1">
    </div>
    <div class="col-md-12">
      <center><button class="btn btn-primary" style="background-color: darkorange; border-color: darkorange;">CETAK SURAT</button></center>
    </div>
  </div>
@endsection
