@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div class="container mt-5">
        <h2>DAFTAR BARANG</h2>
        <h3 class="c-gray">Lampirkan daftar barang yang akan Anda bawa untuk dibuat kedalam surat masuk event.</h3>
    </div>
    <div id="divLoadingCreate" data-accesstoken="{{session('my_token___')}}" data-cpyid="{{$cpyid}}" class="mb-5" ></div>
@stop
@section('script')
    <script src="/framework/public/js/suratloading/create.js"></script>
@stop
@section('style')
    <link rel="stylesheet" type="text/css" href="/framework/public/css/react-datepicker.min.css">
@stop
