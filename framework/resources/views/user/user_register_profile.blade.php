@extends('layouts.master')

@section('title', 'Register Profil Pengguna')

@section('rp_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .in, .out {
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-duration: 500ms !important;
    }

    label{
      float: left;
    }

    p{
      text-align: left;
      font-style: italic;
    }

    a{
      float: left;
    }

    .font{
        font-weight:300;
        font-size:40px;
    }
    #social{
        display: none;
        padding:20px;
    }

    #personal{
        display:none;
        padding:20px;
    }

    #account{
        display: block;
        padding:20px;
    }

    .text.container{

        width:50%;

    }


    #logo{
        border:2px solid white;
        border-radius: 10px;
        color:white;
        padding:15px;
    }
    #logo:hover{
        box-shadow: 0px 0px 5px 5px white;
    }
  </style>
@endsection

@section('content')

  @include('common.header')

<body style="background-color: #e9e9e9;">
  <div class="ui centered  grid container">

    <div class="row"></div>
    <div class="row">
      @if(session()->has('message'))
          <div class="alert alert-success">
              {{ session()->get('message') }}
          </div>
      @endif
    </div>
    <div class="row"></div>


    <div class="ui text container">

      <div class="three ui buttons center aligned grid container" style="margin:20px;">
        <a href="{{ url('#') }}" class=" ui  big orange button" id="accountS">
          <div class="content " style="font-size:12px;color: white;">PROFIL PENGGUNA</div>
        </a>
        <a href="{{ url('daftar-brand-pengguna') }}" class="ui big green button" id="details">
          <div class="content" style="font-size:12px;color:white;">PROFIL BRAND</div>
        </a>
      </div>

      <div class="row"></div>
      <div class="row"></div>

      <div id="account">
        
        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #fff; border-radius:5px;">


          <div class="ui centered header">
            <h1 class="font" style="color: orange;">Lengkapi Profil Anda!</h1>
          </div>

          <br>

          <div class="field">
              <img src="assets/master/img/user/user-img.png" width="100px" height="100px">
              <br>
              <button style="margin-top: 10px;" class="ui  inverted orange  medium button" onclick="document.getElementById('getUserImg').click()" onclose="submit()">Unggah Foto</button>
              <input type='file' id="getUserImg" name="getUserImg" style="display:none;" required="required">
          </div>
          <form class="ui form" action="editDataProses" method="post">
            @csrf
            <br>

            <div class="field">
                <label>Nama Lengkap</label>
                <input type="text" placeholder="-" name="namerp" id="namerp" required="required" value="{{Session::get('name__')}}">
            </div>

            <div class="field">
                <label>Kata Sandi</label>
                <input type="password" placeholder="-" name="passwordrp" id="passwordrp">
            </div>

            <div class="field">
                <label>No Telepon</label>
                <input type="number" placeholder="08389866****" name="telpnumb" id="telpnumb" required="required" value="{{Session::get('telp__')}}">
            </div>

            <div class="field">
              <label>Kota</label>
              <select name="city" id="city">
                  <option value="">Pilih Kota</option>
                  @foreach ($citylist->city_list as $key => $value)
                  <option value="{{$value->city_id}}">{{$value->city_name}}</option>
                  @endforeach
              </select>
            </div>

            <div class="field">
                <label>Alamat Lengkap</label>
                <textarea placeholder="-" name="address" id="address" cols="40" rows="4" style="resize: none;">{{ Session::get('address__') }}</textarea>
            </div>

            <div class="two ui buttons">
              <button type="submit" class="ui  inverted orange  medium button next1 ">Submit</button>
            </div>

          </form>


        </div>

      </div>
    </div>
  </div>
</body>
@endsection
