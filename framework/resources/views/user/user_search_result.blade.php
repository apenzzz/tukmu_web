@extends('layouts.master')
@section('content')

	@include('common.header')

	<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
	    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
	  </ol>
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img class="d-block w-100" src="assets/master/img/home/slider/001.jpg" alt="First slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="assets/master/img/home/slider/002.jpg" alt="Second slide">
	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="assets/master/img/home/slider/003.jpg" alt="Third slide">
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>

	<br>
	<br>

	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="jumbotron">
					<div class="row">
					  	<div class="col-md-4">
					  		<h5>CARI</h5>
					  		<input type="text" name="search">
					  	</div>
					  	<div class="col-md-4">
					  		<h5>TANGGAL</h5>
					  		<input type="date" name="date">
					  	</div>
					  	<div class="col-md-4">
					  		<h5>KATEGORI EVENT</h5>
					  		<input type="text" name="date">
					  	</div>
					</div>
					<br><br>
					<div class="row">
					  	<div class="col-md-5"></div><!--  -->
					  	<div class="col-md-2">
					  		<button class="btn btn-defaullt" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white"><b>CARI</b></span></button>
					  	</div>
					  	<div class="col-md-5"></div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>

		<br>

		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="row">
					<button class="btn btn-defaullt" style="margin-right: 20px; margin-left: 15px; color: white; background-color: rgb(143,143,143);">FIX TIME</button>
					<button class="btn btn-defaullt" style="background-color: #ff9649; color: white;">FLEXIBLE TIME</button>
				</div>
				<br>
				<h4>HASIL PENCAHARIAN</h4>
				<br>
				<div class="jumbotron" style="padding: 10px;">
					<div class="row">
						<div class="col-md-3">
							<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">
						</div>
						<div class="col-md-5">
							<p style="color: red;"><i>Highlight</i></p>
							<h5>INDEPENDENCE DAY</h5>
							<p><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; 17 - 20 AGUSTUS 2017</p>
							<p><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; PLAZA INDONESIA</p>
							<p><img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; RP 1.500.000 - RP 2.500.000</p>
							<span>PENGELOLA / EVENT ORGANIZER</span>
							<br>
							<span><a href="#" style="color: #ff9649">PT. LININDO JAKARTA</a></span>
						</div>
						<div class="col-md-4">
							<h5 style="margin-top: 35px;">DESKRIPSI ACARA</h5>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. .</p>
							<a href="{{ url('profil-event') }}" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
@stop
