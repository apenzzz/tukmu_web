@extends('layouts.master')
@section('title', 'Register Profil Pengguna')
@section('rp_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .in, .out {
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-duration: 500ms !important;
    }
    label{
      float: left;
    }
    p{
      text-align: left;
      font-style: italic;
    }
    a{
      float: left;
    }
    .font{
        font-weight:300;
        font-size:40px;
    }
    #social{
        display: none;
        padding:20px;
    }
    #personal{
        display:none;
        padding:20px;
    }
    #account{
        display: block;
        padding:20px;
    }
    .text.container{
        width:50%;
    }
    #logo{
        border:2px solid white;
        border-radius: 10px;
        color:white;
        padding:15px;
    }
    #logo:hover{
        box-shadow: 0px 0px 5px 5px white;
    }
  </style>
@endsection
@section('content')
  @include('common.header')
<body style="background-color: #e9e9e9;">
  <div class="ui centered  grid container">
    <div class="row"></div>
    <div class="row">
      @if(session()->has('message-success'))
          <div class="alert alert-success">
              {{ session()->get('message-success') }}
          </div>
      @elseif (session()->has('message-failed'))
          <div class="alert alert-danger">
              {{ session()->get('message-failed') }}
          </div>
      @endif
    </div>
    <div class="row"></div>
    <div class="ui text container">
      <div class="three ui buttons center aligned grid container" style="margin:20px;">
        <a href="{{ url('daftar-profil-pengguna') }}" class=" ui  big orange button" id="accountS">
          <div class="content " style="font-size:12px;color: white;">PROFIL PENGGUNA</div>
        </a>
        <a href="{{ url('#') }}" class="ui big green button" id="details">
          <div class="content" style="font-size:12px;color:white;">PROFIL BRAND</div>
        </a>
      </div>
      <div class="row"></div>
      <div class="row"></div>
      <div id="">
        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">
          <div class="ui centered header">
            <h1 class="font" style="color:green; font-size: 35px;">Lengkapi Profil Usaha Anda!</h1>
          </div>
          <p>Pastikan Anda terhubung dengan pasar tenan dari pasar lokal dan internasional secara cepat!</p>
          <br>
          <a href="#" style="color: green;"><u><i>Terms & Condition</i></u></a>
          <p style="margin-top: 30px; margin-bottom: 1px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p><a href="#" style="color: green;"><b>Read More</b></a>
          <br>
          <br>
          <form class="ui form" action="addBrandDataProses" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="field">
              <label>Nama Brand &nbsp; &nbsp; &nbsp;</label><p>Anda bisa memasukkan sampai 5 nama brand</p>
                <input type="text" placeholder="-" name="brandname" id="brandname" required>
            </div>
            <div class="field">
              <label>Brand Kategori</label>
              <select name="brandcat" id="brandcat" required>
                  <option value="">Pilih Kategori</option>
                  @foreach ($categorylist->category_list as $key => $value)
                  <option value="{{$value->custbrandcatid}}">{{$value->custbrandcatname}}</option>
                  @endforeach
              </select>
            </div>
            <div class="field">
              <label>No Telepon</label>
                <input pattern=".[0-9]{5,10}" placeholder="08123456789" name="brandhp" id="brandhp" required>
            </div>
            <div class="field">
              <label>Deskripsi Brand</label>
                <textarea placeholder="Contoh: Peralatan masak / Aksesoris handphone / Makanan siap saji" name="branddesc" id="branddesc" cols="40" rows="4" style="resize: none;" required></textarea>
            </div>
            <div class="field">
              <label>NPWP</label>
                <input type="text" placeholder="-" name="npwp" id="npwp">
            </div>
            <div class="field">
              <label>Foto Profil / Logo Brand</label>
              <br>
              <br>
              <div class="row">
                <div class="col-md-2">
                  <img src="assets/master/img/user/add-img.png" align="left" class="ui  inverted green  medium button" onclick="document.getElementById('branddispic').click()" width="100px" height="90px">
                  <input type='file' id="branddispic" name="branddispic" style="display:none;" onchange="readURL(this);">
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-2">
                  <img src="#" id="uploadeImg" alt="Your Img">
                </div>
              </div>
            </div>
            <hr style="background-color: green;">
            <br>
            <div class="field">
              <label>Deskripsi Foto</label>
                <input type="text" placeholder="-" name="branddisdesc" id="branddisdesc">
            </div>
            <div class="field">
              <label>Website</label>
                <input type="text" placeholder="-" name="brandweb" id="brandweb">
            </div>
            <div class="field">
              <label>Twitter</label>
                <input type="text" placeholder="-" name="tw" id="tw">
            </div>
            <div class="field">
              <label>Facebook</label>
                <input type="text" placeholder="-" name="fb" id="fb">
            </div>
            <div class="field">
              <label>Instagram</label>
                <input type="text" placeholder="-" name="ig" id="ig">
            </div>
            <div class="field">
              <p>Informasi ini akan kami gunakan untuk pengembalian dana seperti deposit, atau refund.
              Kerahasiaan informasi dijamin.</p>
            </div>
            <div class="field">
              <label>Nama Tercantum di Rekening</label>
                <input type="text" placeholder="-" name="bankaccountname" id="bankaccountname" required>
            </div>
            <div class="field">
              <label>No Rekening &nbsp; &nbsp; &nbsp;</label><p>Digunakan untuk pencairan dana dan pengembalian dana</p>
                <input type="text" placeholder="-" name="bankaccountno" id="bankaccountno" required>
            </div>
            <div class="field">
              <label>Nama Bank</label>
                <input type="text" placeholder="-" name="bankname" id="bankname" required>
            </div>
            <div class="field">
              <label>Bank Cabang</label>
                <input type="text" placeholder="-" name="bankbranch" id="bankbranch" required>
            </div>
            <input type="hidden" name="redirecturl" id="redirecturl" value="{{ app('request')->input('redirecturl') }}" >
            <div class="two ui buttons">
              <button type="submit" class="ui  inverted green  medium button submit">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</body>

<script type="text/javascript">
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();
          reader.onload = function (e) {
              $('#uploadeImg')
                  .attr('src', e.target.result)
                  .width(90)
                  .height(90);
          };
          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection
@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection
