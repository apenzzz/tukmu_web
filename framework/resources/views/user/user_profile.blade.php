@extends('layouts.master')

@section('title', 'Profil Pengguna')

@section('up_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .jumbotron{
      border-style: solid;
      border-color: rgb(250,167,25);
      border-width: 9px;
    }
  </style>
@stop

@section('content')
  
  @include('common.header')

  <br>
  <br>
  <div class="container">
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div class="jumbotron">
          <div class="row">
            <div class="col-md-4" style="text-align: center; margin-top: 30px;">
              <div class="field">
                <img src="assets/master/img/user/user-img.png" width="150px" height="150px">
                <br>
                <button style="margin-top: 10px;" class="ui  inverted orange  medium button" onclick="document.getElementById('getUserImg').click()">Unggah Foto</button>
                <input type='file' id="getUserImg" style="display:none" required="required">
              </div>
            </div>
            <div class="col-md-8" style="margin-top: 20px;">
              <h1 style="color: rgb(250,167,25);">DATA PERSONAL</h1>
              <table class="table">
                @foreach ($userdata->cust_data as $key => $value)
                </thead>
                <tbody>
                  <tr>
                    <td>Nama</td>
                    <td>:</td>
                    <td>{{ $value->name }}</td>
                  </tr>
                  <tr>
                    <td>Email</td>
                    <td>:</td>
                    <td>{{ $value->email }}</td>
                  </tr>
                  <tr>
                    <td>Telepon</td>
                    <td>:</td>
                    <td>{{ $value->telp }}</td>
                  </tr>
                  <tr>
                    <td>Alamat</td>
                    <td>:</td>
                    <td>{{ $value->address }}</td>
                  </tr>
                </tbody>
                @endforeach
              </table>
              <br>
              <a href="{{ url('daftar-profil-pengguna') }}" class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH</b></span> <img src="assets/master/img/user/btn.png" width="20" height="20"></a>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-2"></div>
    </div>

    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <h1 style="margin-top: 50px; color: rgb(250,167,25);">LIST BRAND</h1>
        <hr style="background-color: rgb(250,167,25);">
          @foreach ($branddata->brand_list as $key => $value)
            <div class="jumbotron jumbotron-fluid" style="border: none; padding: 7px;">
              <div class="container">
                <div class="row">
                  <div class="col-md-3">
                    <h4>{{ $value->brand_name }}</h4>
                  </div>
                  <div class="col-md-3">
                    <h4>Category</h4>
                  </div>
                  <div class="col-md-3">
                    <a href="{{ url('brand-detail/'.$value->brand_id) }}" style="color: black;"><b><u><i>Klik disini untuk detail</i></u></b></a>
                  </div>
                  <div class="col-md-3">
                    <a href="{{ url('deleteBrandProses/'.$value->brand_id) }}" style="color: red;" onclick="return confirm('Are you sure you want to delete this brand?');"><b><u><i>Delete Brand</i></u></b></a>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
          <center>
            @if(count($branddata->brand_list) == 5) 
                <button disabled="disabled" class="btn btn-default" style="background-color: #ff9649; color: white;">ADD NEW BRAND</button>
            @else
                <a href="{{ url('daftar-brand-pengguna') }}" class="btn btn-default" style="background-color: #ff9649; color: white;">ADD NEW BRAND</a>
            @endif
          </center>
        </div>
      <div class="col-md-2"></div>
    </div>
  </div>
@endsection

@section('rp_js')
  <script type="text/javascript">
    $('.divExpander').click(function(){
    $(this).find('.hideDiv').toggle();
    });
  </script>
  <script type="text/javascript">
    
  </script>
@endsection
@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection
