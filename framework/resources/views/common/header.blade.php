<!-- Navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-orange">
    <a class="navbar-brand" href="{{ url('/') }}" style="color: white;">TUKMU.COM</a> &nbsp; &nbsp;
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
    @if(!Session::has('my_token___'))
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <a class="nav-link" href="{{ url('about') }}" style="color: white;">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ url('/contact/admin') }}" style="color: white;">Bantuan</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <button type="button" class="btn btn-link" style="color: white;" data-toggle="modal" data-target="#modalregister"><img src="{!! asset('assets/master/img/navbar/daftar.png') !!}" width="30px" height="30px">Daftar</button>
            &nbsp; &nbsp;  
            <button type="button" class="btn btn-link" style="color: white;" data-toggle="modal" data-target="#modallogin">
            <img src="{!! asset('assets/master/img/navbar/masuk.png') !!}" width="30px" height="30px">Masuk</button>
        </form>
    @else
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link" href="{{ url('about') }}" style="color: white;">About</a>
                </div>
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link" href="#dropdownMenu" style="color: white;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Transaksi</a>
                    <div class="dropdown-menu" id="dropdownMenu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ url('pending-order') }}">Nego</a>
                        <a class="dropdown-item" href="{{ url('tagihan') }}">Tagihan</a>
                        <a class="dropdown-item" href="{{ url('lunas') }}">Lunas</a>
                        <a class="dropdown-item" href="{{ url('transaksi/loading/list') }}">Surat Masuk / Keluar</a>
                        <a class="dropdown-item" href="{{ url('/refund/list') }}">Refund</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <div class="dropdown">
                    <a class="nav-link" href="#dropdownMenu2" style="color: white;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Notifikasi</a>
                    <div class="dropdown-menu" id="dropdownMenu2" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item" href="{{ url('pesan') }}">Pesan</a>
                        <a class="dropdown-item" href="{{ url('contact/spaceprovider') }}">Hubungi Penyelenggara</a>
                        <a class="dropdown-item" href="{{ url('contact/admin') }}">Hubungi Kami</a>
                    </div>
                </div>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <div class="dropdown">
                <a href="#dropdownMenu3" class="btn btn-link" style="color: white;" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Session::get('name__')}}</a>
                <div class="dropdown-menu" id="dropdownMenu3" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{ url('profil-pengguna') }}">Profil</a>
                    <a class="dropdown-item" href="#">Penawaran</a>
                </div>
            </div>
            <a class="nav-link" href="{{ url('/') }}/logout" style="color: white;"><img src="{!! asset('assets/master/img/logout.png') !!}" width="30px" height="30px"> Keluar</a>
        </form>
    @endif
</nav>
<!-- Modal Register -->
<div class="modal fade" id="modalregister" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: orange;">
                <ul class="nav nav-pills2" id="myTab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userregister" role="tab" aria-controls="home" aria-selected="true" style="color: black;">DAFTAR PENGGUNA</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminregister" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">DAFTAR PENYELENGGARA</a>
                    </li>
                </ul>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="userregister" role="tabpanel" aria-labelledby="home-tab">
                    <center>
                        <h4>DAFTAR <strong>PENGGUNA</strong></h4>
                    </center>
                    <br>
                    <form action="{{ env('APP_URL') . 'registrasi' }}" method="post">
                        @csrf
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nama Lengkap" required>
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control" name="notelp__" id="notelp__" placeholder="Nomer Telepon" required>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Kata Sandi" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="repassword" id="repassword" placeholder="Ulangi Kata Sandi" required>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-primary" id="daftar">
                                    DAFTAR &nbsp;<i class="fa fa-spinner fa-spin d-none" id="loading-dafftar"></i>
                                </button>
                            </div>
                        </div><br/>
                        <div class="alert alert-success d-none" id="sukses-daftar">
                            <h5>Anda berhasil registrasi, Silahkan cek email untuk verifikasi</h5> 
                        </div>
                    </form>
                    </div>
                    <div class="tab-pane fade" id="adminregister" role="tabpanel" aria-labelledby="profile-tab">
                        <h4>APAKAH ANDA <strong>PENYELENGGARA ACARA?</strong></h4>
                        <br>
                        <p>Pastikan Anda terhubung dengan para tenan dari pasar lokan dan internasional secara cepat!</p>
                        <br>
                        <a href="#" class="btn btn-primary">DAFTAR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Login -->
<div class="modal fade" id="modallogin" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: orange;">
                <ul class="nav nav-pills2" id="myTab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">LOGIN PENGGUNA</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#adminlogin" role="tab" aria-controls="profile" aria-selected="false" style="color: black;">LOGIN PENYELENGGARA</a>
                    </li>
                </ul>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="userlogin" role="tabpanel" aria-labelledby="home-tab">
                    <center>
                        <h4>LOGIN <strong>PENGGUNA</strong></h4>
                    </center>
                    <br>

                    <form action="{{ env('APP_URL') . 'dologin_pengguna' }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <input type="email" class="form-control" name="email_peng" id="email_peng" placeholder="Email">
                            <center><p id="emailnotverif" style="color: red; font-size: 11px; display: none;">Email belum terverifikasi! mohon verifikasi Email anda terlebih dahulu untuk melakukan login.</p></center>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password_peng" id="password_peng" placeholder="Kata Sandi">
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">LOGIN</button>
                            </div>
                            <div class="col">
                                <p>Lupa Kata Sandi? <a href="#modalpr" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                            </div>
                        </div>
                    </form>
                    </div>
                    <div class="tab-pane fade" id="adminlogin" role="tabpanel" aria-labelledby="profile-tab">
                    <center>
                        <h4>LOGIN <strong>PENYELENGGARA</strong></h4>
                    </center>
                    <br>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email_peny" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" name="password" placeholder="Kata Sandi">
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-success">LOGIN</button>
                            </div>
                            <div class="col">
                                <p>Lupa Kata Sandi? <a href="">Klik disini!</a></p>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Password Recovery -->
<div class="modal fade" id="modalpr" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: orange;">
                <ul class="nav nav-pills2" id="myTab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#userlogin" role="tab" aria-controls="home" aria-selected="true" style="color: black;">PEMULIHAN KATA SANDI PENGGUNA</a>
                    </li>
                </ul>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="pr" role="tabpanel" aria-labelledby="home-tab">
                    <h4>APAKAH ANDA <strong>LUPA KATA SANDI?</strong></h4>
                    <p>Silahkan masukkan email yang sudah terdaftar untuk akun anda.</p>
                    <br>
                    <br>
                    <form>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Contoh : pengguna@gmail.com">
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col">
                                <button type="submit" class="btn btn-warning">Kirim Link Recovery</button>
                            </div>
                            <div class="col">
                                <p>Mengingat Kata Sandi Anda? <a href="#modallogin" data-dismiss="modal" data-toggle="modal">Klik disini!</a></p>
                            </div>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script>
    $('#email_peng').change(function () {
        var email_peng = $('#email_peng').val();
        var cekemail = {
            "async": true,
            "crossDomain": true,
            "url": "https://tukbase3.tukmu.com/api/customer/exists?email="+email_peng,
            "method": "GET"
        }
        $.ajax(cekemail).done(function (response) {
            if (response.status === "1") {
                $("#emailnotverif").hide();
            } else {
                $("#emailnotverif").show();
            }
        })
    });
</script>
@endpush
