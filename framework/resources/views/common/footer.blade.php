<footer class="footer bg-gray">
    <div class="container">
        <div class="row">
            <div class=" col-sm-3 col-md col-sm-4  col-12 col">
                <h3 class="c-darkorange pt-5">About Us</h3>
                <p class="mb10" style="color: black;">With more than 15 years of experience we can proudly say that we are one of the best in business, a trusted supplier for more than 1000 companies.</p>
            </div>
            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="c-darkorange pt-5">Site Map</h3>
                <a href="#" style="color: black;">About</a><br/>
                <a href="#" style="color: black;">Bantuan</a><br/>
                <a href="#" style="color: black;">Daftar</a><br/>
                <a href="#" style="color: black;">Masuk</a><br/>
            </div>
            <div class=" col-sm-3 col-md  col-6 col">
                <h3 class="c-darkorange pt-5">Get In Touch</h3>
                <a href="#"><i class="fab fa-facebook-f" style="margin-right: 15px;"></i></a>
                <a href="#"><i class="fab fa-twitter" style="margin-right: 15px;"></i></a>
                <a href="#"><i class="fab fa-linkedin" style="margin-right: 15px;"></i></a>
                <a href="#"><i class="fab fa-instagram" style="margin-right: 15px;"></i></a>
            </div>
            <div class=" col-sm-3 col-md  col-12 col">
                <h3 class="c-darkorange pt-5">Newsletter</h3>
                <p style="color: black;">Subscribe us for new update information of our event</p>
                <form>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subscribe" placeholder="Enter your email here">
                    </div>
                    <button type="submit" class="btn btn-defaullt btn-sm" style="background-color: #f47b50; color: white;">SUBSCRIBE</button>
                </form>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-sm-12">
                <p class="text-center" style="color: black;">Copyright @2017 | Designed With by <a href="#">tukmu.com</a></p>
            </div>
        </div>
    </div>
</footer>
