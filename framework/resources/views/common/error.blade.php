@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div class="container content m-t-15 m-b-15">
        <div class="row">
            <div class="col-sm-12">
                <div class="jumbotron jumbotron-fluid">
					<div class="container">
                        <h1>Error: </h1>
					    {{$error}}
                        <br/>
                        <br/>
                        <p>Please contact administrator</p>
					</div>
				</div>
            </div>
        </div>
    </div>
@endsection

