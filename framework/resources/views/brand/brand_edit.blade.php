@extends('layouts.master')

@section('title', 'Register Profil Pengguna')

@section('rp_css')
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.2/semantic.min.css">
  <style type="text/css">
    .in, .out {
    -webkit-animation-timing-function: ease-in-out;
    -webkit-animation-duration: 500ms !important;
    }

    label{
      float: left;
    }

    p{
      text-align: left;
      font-style: italic;
    }

    a{
      float: left;
    }

    .font{
        font-weight:300;
        font-size:40px;
    }
    #social{
        display: none;
        padding:20px;
    }

    #personal{
        display:none;
        padding:20px;
    }

    #account{
        display: block;
        padding:20px;
    }

    .text.container{

        width:50%;

    }


    #logo{
        border:2px solid white;
        border-radius: 10px;
        color:white;
        padding:15px;
    }
    #logo:hover{
        box-shadow: 0px 0px 5px 5px white;
    }
  </style>
@endsection

@section('content')

  @include('common.header')

<body style="background-color: #e9e9e9;">
  <div class="ui centered  grid container">

    <div class="row"></div>
    <div class="row"></div>
    <div class="row"></div>


    <div class="ui text container">


      <div class="three ui buttons center aligned grid container" style="margin:20px;">
        <div href="{{ url('#') }}" class="ui big green button" id="details">
          <div class="content" style="font-size:12px;color:white;">FORM EDIT BRAND</div>
        </div>
      </div>

      <div class="row"></div>
      <div class="row"></div>

      <div id="">

        <div class="ui center aligned  segment container " id="signUpBox" style="background-color: #F1F0FF;border-radius:5px;">

          <div class="ui centered header">
            <h1 class="font" style="color:green; font-size: 35px;">Silahkan Edit Profil Usaha Anda!</h1>
          </div>

          <p>Pastikan Anda terhubung dengan pasar tenan dari pasar lokal dan internasional secara cepat!</p>

          <br>

          <a href="#" style="color: green;"><u><i>Terms & Condition</i></u></a>
          <p style="margin-top: 30px; margin-bottom: 1px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat.</p><a href="#" style="color: green;"><b>Read More</b></a>

          <br>
          <br>
          @foreach ($viewbrand->brand_data as $key => $value)
          <form class="ui form" action="editBrandProses" method="POST">
            @csrf
            <div class="field">
                <input type="text" placeholder="-" name="brandid" id="brandid" style="display: none;" value="{{ $value->brand_id }}">
            </div>
            <div class="field">
              <label>Nama Brand &nbsp; &nbsp; &nbsp;</label><p>Anda bisa memasukkan sampai 5 nama brand</p>
                <input type="text" placeholder="-" name="brandname" id="brandname" value="{{ $value->brand_name }}">
            </div>
          @endforeach

            <div class="field">
              <label>Brand Kategori</label>
              <select name="brandcat" id="brandcat" required="required">
                  <option value="">Pilih Kategori</option>
                  @foreach ($categorylist->category_list as $key => $value)
                  <option value="{{$value->custbrandcatid}}">{{$value->custbrandcatname}}</option>
                  @endforeach
              </select>
            </div>
            @foreach ($viewbrand->brand_data as $key => $value)
            <div class="field">
              <label>No Telepon</label>
                <input type="text" placeholder="-" name="brandhp" id="brandhp" value="{{ $value->hp }}">
            </div>

            <div class="field">
              <label>Deskripsi Brand</label>
                <textarea placeholder="Contoh: Peralatan masak / Aksesoris handphone / Makanan siap saji" name="branddesc" id="branddesc" cols="40" rows="4" style="resize: none;">{{ $value->brand_desc }}</textarea>
            </div>

            <div class="field">
              <label>NPWP</label>
                <input type="text" placeholder="-" name="npwp" id="npwp" value="{{ $value->npwp }}">
            </div>

            <div class="field">
              <label>Nama Registrasi &nbsp; &nbsp; &nbsp;</label><p>Samakan dengan Nama Brand jika tidak ada</p>
                <input type="text" placeholder="-" name="regname" id="regname">
            </div>

            <div class="field">
              <label>Website</label>
                <input type="text" placeholder="-" name="brandweb" id="brandweb" value="{{ $value->website }}">
            </div>

            <div class="field">
              <label>Twitter</label>
                <input type="text" placeholder="-" name="tw" id="tw" value="{{ $value->twitter }}">
            </div>

            <div class="field">
              <label>Facebook</label>
                <input type="text" placeholder="-" name="fb" id="fb" value="{{ $value->fb }}">
            </div>

            <div class="field">
              <label>Instagram</label>
                <input type="text" placeholder="-" name="ig" id="ig" value="{{ $value->instagram }}">
            </div>

            <div class="two ui buttons">
              <button type="submit" class="ui  inverted green  medium button submit">Ubah</button>
              <a href="{{ url('brand-detail/'.$value->brand_id) }}" class="ui  inverted red  medium button submit">Batal</a>
            </div>

          </form>
          @endforeach


        </div>

      </div>


    </div>
  </div>
</body>

<script type="text/javascript">
  function readURL(input) {
      if (input.files && input.files[0]) {
          var reader = new FileReader();

          reader.onload = function (e) {
              $('#uploadeImg')
                  .attr('src', e.target.result)
                  .width(90)
                  .height(90);
          };

          reader.readAsDataURL(input.files[0]);
      }
  }
</script>
@endsection
