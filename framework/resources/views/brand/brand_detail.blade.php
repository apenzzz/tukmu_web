@extends('layouts.master')

@section('title', 'Brand Detail')

@section('content')
  
  @include('common.header')

  <div class="container">
  	<div class="row">
  		<div class="col-md-2"></div>
  		<div class="col-md-8" style="margin-top: 30px;">
  			 @if(session()->has('message-success'))
		          <div class="alert alert-success">
		              {{ session()->get('message-success') }}
		          </div>
		      @elseif (session()->has('message-failed'))
		          <div class="alert alert-danger">
		              {{ session()->get('message-failed') }}
		          </div>
		      @endif
  			<div class="jumbotron" style="border: solid; border-width: 9px; border-color: rgb(244,123,80); margin-top: 50px;">
		  		<div class="row">
		  			<div class="col-md-4" style="text-align: center; margin-top: 30px;">
		  				@foreach ($viewbrand->list_display as $key => $value)
				      <div class="field">
				        <img src="https://tukbase3.tukmu.com/picGet/{{ $value->mediapath }}" width="150px" height="150px">
				        <br>
				        <br>
				        <p style="text-align: justify;">{{ $value->mediadesc }}</p>
				      </div>
				      @endforeach
				    </div>
				    <div class="col-md-8" style="margin-top: 20px;">
				      <h1 style="color: rgb(250,167,25);">DATA BRAND</h1>
				      @foreach ($viewbrand->brand_data as $key => $value)
				      <table class="table">
				        <tbody>
				          <tr>
				            <td>Nama Brand</td>
				            <td>:</td>
				            <td>{{ $value->brand_name }}</td>
				          </tr>
				          <tr>
				            <td>Deskripsi Brand</td>
				            <td>:</td>
				            <td>{{ $value->brand_desc }}</td>
				          </tr>
				          <tr>
				            <td>Perusahaan</td>
				            <td>:</td>
				            <td>{{ $value->PT }}</td>
				          </tr>
				          <tr>
				            <td>Telepon</td>
				            <td>:</td>
				            <td>{{ $value->hp }}</td>
				          </tr>
				          <tr>
				            <td>NPWP</td>
				            <td>:</td>
				            <td>{{ $value->npwp }}</td>
				          </tr>
				          <tr>
				            <td>Website</td>
				            <td>:</td>
				            <td>{{ $value->website }}</td>
				          </tr>
				          <tr>
				            <td>Alamat</td>
				            <td>:</td>
				            <td></td>
				          </tr>
				        </tbody>
				      </table>
				      <br>
				      <a href="{{ url('brand-edit/'.$value->brand_id) }}" class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH</b></span> <img src="{!! asset('assets/master/img/user/btn.png') !!}" width="20" height="20"></a>
				      <a href="{{ url('profil-pengguna') }}" class="btn btn-primary"><span style="color: white"><b>KEMBALI</b></span></a>
				    </div>
				     @endforeach
		  		</div>
		  	</div>
  		</div>
  		<div class="col-md-2"></div>
  	</div>
  </div>
@stop
@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection
