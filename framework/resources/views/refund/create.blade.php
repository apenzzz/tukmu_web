@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divRefundCreate" data-username="{{ Session::get('name__')}}" data-accesstoken="{{session('my_token___')}}" data-cpyid="{{$cpyid}}"></div>
@stop
@section('script')
    <script src="{{ url('framework/public/js/refund/refundcreate.js') }}"></script>
@stop
