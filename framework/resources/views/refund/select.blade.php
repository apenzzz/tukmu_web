@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divRefundSelect" data-username="{{ Session::get('name__')}}" data-accesstoken="{{session('my_token___')}}"></div>
@stop
@section('script')
    <script src="{{ url('framework/public/js/refund/refundselect.js') }}"></script>
@stop
