@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divRefundList" data-accesstoken="{{session('my_token___')}}"></div>
@stop
@section('script')
    <script src="{{ url('framework/public/js/refund/refundlist.js') }}"></script>
@stop
