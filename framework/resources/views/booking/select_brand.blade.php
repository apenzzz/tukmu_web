@inject('pointing', 'App\Helpers\Datastatik')
@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div class="container my-sm-5 bg-white">
        <div class="row">
            <div class="col-sm-12 p-5 border shadow-sm">
                <h2>PILIH BRAND</h2>
                <div class="row">
				    <div class="col-sm-9">
    				  	<select class="form-control brand_id" required="required">
				  		    <option value="">Pilih Brand</option>
				  		    @foreach ($displaybrand->brand_list as $key => $value)
				  		    <option value="{{ $value->brand_id }}">{{ $value->brand_name }}</option>
				  		    @endforeach
				  	    </select>
				    </div>
				    <div class="col-sm-3">
    				  	@if(count($displaybrand->brand_list) == 5)
				  	    BRAND TELAH MENCAPAI BATAS MAKSIMAL
				  	    @else
				  	    <a href="{{ url('daftar-brand-pengguna?redirecturl=' . Request::url()) }}" style="color: black;"><img src="{!! asset('assets/master/img/tambah.png') !!}" width="12" height="12"> BUAT / TAMBAH BRAND BARU</a>
				  	    @endif
				    </div>
                </div>
                <hr style="background-color: orange;">
                <div class="row">
				  	<div class="col-sm-9">
				  		<h2>{{ $event_detail->eventname }}</h2>
				  		<div class="d-flex">
		                    <div class="p-2 p-r-l">
		                        <i class="fa fa-calendar-alt c-orange" style="color: orange;"></i>
		                    </div>
		                    <div class="p-2 p-r-l">
								<span><b>{{ date("d-M-Y", strtotime($event_detail->startdate)) }} - {{ date("d-M-Y", strtotime($event_detail->finishdate)) }}</b></span>
		                    </div>
		                </div>
		                <div class="d-flex">
		                    <div class="p-2 p-r-l">
		                        <i class="fa fa-map-marker-alt c-orange" style="color: orange;"></i>
		                    </div>
		                    <div class="p-2 p-r-l">
		                        <span><b>{{ $event_detail->spacename }}</b></span>
		                    </div>
		                </div>
				  	</div>
				  	<div class="col-sm-3">
				  		<img class="img-responsive img-thumbnail w-100" src="{{$pointing->pointing()}}picGet/{{(count($event_detail->space_provide_media) > 0 ? $event_detail->space_provide_media[0]->spacemediapath : null)}}">
				  	</div>
				</div>
				<hr style="background-color: orange;">
				<div class="text-center"><a href="javascript:;" class="btn bg-orange c-white" id="btn-booking">LANJUTKAN</a></div>
            </div>
        </div>
    </div>
@stop
@section('script')
	<script>
		$('document').ready(function(){
			$('#btn-booking').click(function(){
				if($(".brand_id option:selected").val() !== ""){
					window.location = "{{ url('booking') }}/{{$id}}"+"/"+$(".brand_id option:selected").val();
				} else {
					alert("Mohon untuk memilih brand anda terlebih dahulu.");
				}
			});
		});
	</script>
@stop
