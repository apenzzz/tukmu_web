@inject('pointing', 'App\Helpers\Datastatik')
@extends('booking.layout')
@section('content')
    @include('common.header')
    <div id="chat_content"></div>
    @include('booking.blocks.modal-confirm-booking')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script>
    <script id="chat_room" type="text/babel" src="{{url('/')}}/framework/public/js/chat.js?endpoint={{ $pointing->pointing() }}&cpyid={{$id}}&brand_id={{$brand_id}}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script>
@endsection
