@inject('pointing', 'App\Helpers\Datastatik')
@extends('booking.layout')

@section('content')

    @include('common.header')

    <div class="container content m-t-15 m-b-15">
        <div class="row">
            <div class="col-md-12">
                <h2 class="f-w-300">TAMBAHAN</h2>
                <h5 class="f-w-200">
                    <i>
                        Lengkapi fasilitas pameran sesuai<br/>
                        dengan kebutuhan Anda!
                    </i>
                </h5>
                <div class="row content-tambahan">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table">
                                <tr class="bg-orange c-white t-a-c">
                                    <td>JENIS</td>
                                    <td>HARGA</td>
                                    <td>JUMLAH</td>
                                    <td>SISA</td>
                                    <td>MINIMAL WAKTU PEMESANAN</td>
                                </tr>
                                <tbody id="tambahan_content">

                                </tbody>
                            </table>
                        </div>
                        <br/>
                        <h6 class="m-r-b">KETERANGAN</h6>
                        <h6 class="f-w-200 f-s-12">
                            <i>
                                Harga diatas berlaku dari tanggal 08 Desember - 10 Desember 2017
                            </i>
                        </h6>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>


    {{-- <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script> --}}

    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>

    
    <script id="tambahan_room" type="text/babel" src="{{url('/')}}/assets/master/jsx/tambahan.jsx?endpoint={{ $pointing->pointing() }}&acces_token={{session('my_token___')}}&url={{url('/')}}&cpyid={{$cpyid}}"></script>
    

@endsection

