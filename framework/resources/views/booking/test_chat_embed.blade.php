@inject('pointing', 'App\Helpers\Datastatik')
@extends('booking.layout')

@section('content')

    @include('common.header')
    <iframe src="https://testnotificationfirebase-284de.firebaseapp.com/" width="100%" style="height: 100vh">
        <p>Your browser does not support iframes.</p>
    </iframe>
    @include('booking.blocks.modal-confirm-booking')

@endsection
