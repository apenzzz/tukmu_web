@extends('layouts.newmaster')
@section('style')
    <link rel="stylesheet" href="{{asset('assets/master/css/main.css')}}" />
    <style type="text/css">
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin-top: -4%;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            margin-right: 5px;
        }
	</style>
@stop

@section('content')
    @include('common.header')
    <div class="container content m-t-15 m-b-15">
        <div class="row">
            <div class="col-md-12">
                <h2 class="f-w-300">TERIMA KASIH!</h2>
                <h5 class="f-w-200 m-b-20">
                    <i>
                        Pesanan Anda telah kami terima<br/>
                    </i>
                </h5>
                <div class="row content-complite m-r">
                    <div class="col-md-12">
                        <div class="row bg-white p-15">
                            <div class="col-10 content">
                                <h4 class="f-w-300">INDEPENDENCE DAY</h4>
                                <div class="row">
                                    <div class="col-md-6 p-r-r">
                                        <div class="d-flex justify-content-start">
                                            <div class="p-2 p-r-l">
                                                <i class="fa fa-calendar-alt c-orange"></i>
                                            </div>
                                            <div class="p-2 p-r-l">
                                                <span class="t-upercase">
                                                    {{ date("d-M-Y", strtotime($bookingcomplete->event_data->startdate)) }} - {{ date("d-M-Y", strtotime($bookingcomplete->event_data->finishdate)) }}
                                                </span>
                                            </div>
                                            <div class="p-2 p-r-l">
                                                <i class="fa fa-map-marker-alt c-orange"></i>
                                            </div>
                                            <div class="p-2 p-r-l">
                                                <span class="t-upercase">
                                                    {{ $bookingcomplete->event_data->spacename }}
                                                </span>
                                            </div>
                                        </div>
                                        <hr/>
                                        <h6 class="m-r-b f-s-14">STAN</h6>
                			    		    <ul style="display: flex; padding: 0px; margin-top: 4%;">
                			    			<?php
                				    			 $length = count($bookingcomplete->payment_data->payment_grid);
                								for ($i = 0; $i < $length; $i++) {
                									echo "<li style='list-style: none; text-decoration: none; margin-right: 5px;'>";
                                                    echo $bookingcomplete->payment_data->payment_grid[$i]->code;
                								  	echo "</li>";
                								} 
                							?>
                			    		</ul>
                                        <hr/>
                                        <h6 class="m-r-b f-s-14">BRAND</h6>
                                        <span class="c-orange">{{ $bookingcomplete->payment_data->customer_brand->brandname }}</span>
                                        <hr/>
                                        <h6 class="m-r-b f-s-14">PENGELOLA</h6>
                                        <span class="c-orange t-td-u">{{ $bookingcomplete->event_data->space_provider->spaceprovname }}</span>
                                        <hr />
                                    </div>
                                    <div class="col-md-6 p-r-r">
                                        <div id="time-count-down" class="d-flex">
                                            <div class="p-2 hours-1" id="hours-1">
                                                0
                                            </div>
                                            <div class="p-2 hours-2" id="hours-2">
                                                0
                                            </div>
                                            <div class="p-2 titik-dua">
                                                :
                                            </div>
                                            <div class="p-2 minutes-1" id="minutes-1">
                                                0
                                            </div>
                                            <div class="p-2 minutes-2" id="minutes-2">
                                                0
                                            </div>
                                            <div class="p-2 titik-dua">
                                                :
                                            </div>
                                            <div class="p-2 seconds-1" id="seconds-1">
                                                0
                                            </div>
                                            <div class="p-2 seconds-2" id="seconds-2">
                                                0
                                            </div>
                                        </div>
                                        <div id="timeout" class="d-none">
                                            <h3 class="c-red">WAKTU ANDA TELAH HABIS!</h3>
                                        </div>
                                        <div class="t-a-j">
                                            <p>
                                                <span class="t-upercase f-s-20 f-w-500">Pembayaran Paling Lambat</span><br/>
                                                <span class="c-orange t-td-u t-upercase f-s-20 f-w-500">{{ $bookingcomplete->payment_data->limit_payment }}</span><br/><br/>
                                                Pesanan anda telah kami teruskan ke pihak penyelenggara. Pihak
                                                penyelenggara akan merespon dalam waktu 1x24 Jam. silahkan tutup halaman ini.<br/><br>
                                                Kami akan memberikan notifikasi jika pihak penyelenggara sudah memberikan jawaban
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <h5 class="t-upercase">Pesanan</h5>
                                <hr class="m-r"/>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr class="bg-orange c-white t-a-c">
                                            <td>JENIS</td>
                                            <td>HARGA</td>
                                            <td>JUMLAH</td>
                                            <td>TOTAL</td>
                                        </tr>
                                        @foreach($bookingcomplete->addon_payment as $key => $value)
                                        <tr class="t-a-c">
                                            <td class="p-r bg-light-gray">
                                                <div class="d-flex justify-content-center">
                                                    <div class="p-2">   
                                                        {{ $value->spaceaddsrvdesc }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-r bg-gray">
                                                <div class="d-flex justify-content-center">
                                                    <div class="p-2">
                                                        {{ $value->addon_price }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-r bg-light-gray">
                                                <div class="d-flex justify-content-center">
                                                    <div class="p-2">
                                                        {{ $value->addqty }}
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="p-r bg-gray">
                                                <div class="d-flex justify-content-center">
                                                    <div class="p-2">
                                                        {{ $value->total_price }}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <br/>
                                <h6>JUMLAH PEMBAYARAN</h6>
                                <div class="d-flex bd-highlight mb-2">
                                    <div class="mr-auto p-2 bd-highlight bg-orange c-white">
                                        {{ $bookingcomplete->grand_total_payment }}
                                    </div>
                                    <div class="p-2 bd-highlight c-orange">
                                        <i class="fa fa-print"></i>&nbsp;&nbsp;
                                        CETAK TAGIHAN
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
            // Set the date we're counting down to
            var countDownDate = new Date('{{ $bookingcomplete->payment_data->limit_payment }}').getTime();
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
                // Get todays date and time
                var now = new Date().getTime();
                
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                
                // Time calculations for days, hours, minutes and seconds
                // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
                var hours1 = hours.toString().slice(0,1);
                var hours2 = hours.toString().slice(1,2);

                var minutes1 = minutes.toString().slice(0,1);
                var minutes2 = minutes.toString().slice(1,2);
                
                var seconds1 = seconds.toString().slice(0,1);
                var seconds2 = seconds.toString().slice(1,2);
                
                if(hours < 10) {
                    document.getElementById("hours-1").innerHTML = 0;
                    document.getElementById("hours-2").innerHTML = hours1;
                } else {
                    document.getElementById("hours-1").innerHTML = hours1;
                    document.getElementById("hours-2").innerHTML = hours2;
                }

                if(minutes < 10) {
                    document.getElementById("minutes-1").innerHTML = 0;
                    document.getElementById("minutes-2").innerHTML = minutes1;
                } else {
                    document.getElementById("minutes-1").innerHTML = minutes1;
                    document.getElementById("minutes-2").innerHTML = minutes2;
                }

                if(seconds < 10) {
                    document.getElementById("seconds-1").innerHTML = 0;
                    document.getElementById("seconds-2").innerHTML = seconds1; 
                } else {
                    document.getElementById("seconds-1").innerHTML = seconds1;
                    document.getElementById("seconds-2").innerHTML = seconds2; 
                }

                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("timeout").classList.remove("d-none")
                    document.getElementById("time-count-down").classList.remove("d-flex")
                    document.getElementById("time-count-down").classList.add("d-none")
                }
            }, 1000);
    </script>

@endsection

