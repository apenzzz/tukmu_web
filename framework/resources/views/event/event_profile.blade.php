@inject('pointing', 'App\Helpers\Datastatik')
@extends('layouts.master')

@section('ep_css')
	<style type="text/css">
		.nav-tabs .nav-link.active,
		.nav-tabs .nav-item.show .nav-link {
		  color: white;
		  background-color: #ff9649;
		}

		.nav-tabs .nav-link {
		  border: 0;
		  color: black;
		}

		h3{
			color: #ff9649;
		}

		#user{
			margin-left: 30px;
		}

		#admin{
			margin-left: 80px;
			color: orange;
		} 

		div.ex1 {
		  	background-color: whitesmoke;
		  	height: 300px;
		}

		/* Set the size of the div element that contains the map */
    	#map {
        	height: 220px;  /* The height is 400 pixels */
        	width: 320px;  /* The width is the width of the web page */
       	}

       	.checkedBox {
       		background: #ff9649;
		}

		.display-data {
		  	height: 300px;
		}   

		.display-data ul {
			margin-left: 0px;
			padding-left: 0px;
		}
		.display-data ul li {
			list-style: none;
			background: #ff9649;
			padding: 5px 10px;
			margin-bottom: 2px
		}

		.display-data ul li i {
			margin-left: 15px;
			cursor: pointer;
		}
	</style>
@stop

@section('content')

	@include('common.header')



	<div class="container" style="margin-top: 40px;">
		{{-- <button class="btn btn-defaullt" style="background-color: #ff9649"><span style="color: white"><b>UBAH PENCARIAN</b></span></button>
		<br>
		<br> --}}
		<div class="row blog">
        <div class="col-md-12">
            <div id="blogCarousel" class="carousel slide" data-ride="carousel">

            	<!-- COUNT ROW IMAGE IN EVENT DETAIL -->
                <ol class="carousel-indicators">
                    <?php 
                    	$countRaw = count($detail->event_data[0]->space_provide_media);
                        if ($countRaw == 1) {
                            $event = 1;
                        } elseif ($countRaw > 1) {
                        	$event = 2;
                        } elseif ($countRaw > 2) {
                        	$event = 3;
                        } elseif ($countRaw > 3) {
                        	$event = 4;
                        } elseif ($countRaw > 4) {
                        	$event = 5;
                        }
                        else {
                            $event = 0;
                        }
                    ?>
                    @for($i = 0; $i < $event; $i++)
                    <li data-target="#blogCarousel" data-slide-to="{{$i}}" @if($i == 0)class="active"@endif></li>
                    @endfor
                </ol>

                <!-- Carousel items -->
                <div class="carousel-inner">
                    @for($i = 0; $i < $event; $i++)
                        <div @if($i == 0)class="carousel-item active" @else class="carousel-item" @endif>          
                            <div class="row">
                                @foreach($detail->event_data[0]->space_provide_media as $key => $value)
                                @if($i == 0 && $key <= 0)
                                <div class="col-md-12">
                                    <img src="{{$pointing->pointing()}}picGet/{{ $value->spacemediapath}}" style="width: 100%; height: 400px; margin-bottom: 15px"/>
                                </div>
                                @elseif($i == 1 && $key > 0)
                                <div class="col-md-12">
                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->spacemediapath}}" style="width: 100%; height: 400px; margin-bottom: 15px"/>
                                </div>
                                @elseif($i == 2 && $key > 1)
                                <div class="col-md-12">
                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->spacemediapath}}" style="width: 100%; height: 400px; margin-bottom: 15px"/>
                                </div>
                                @elseif($i == 3 && $key > 2)
                                <div class="col-md-12">
                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->spacemediapath}}" style="width: 100%; height: 400px; margin-bottom: 15px"/>
                                </div>
                                @elseif($i == 4 && $key > 3)
                                <div class="col-md-12">
                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->spacemediapath}}" style="width: 100%; height: 400px; margin-bottom: 15px"/>
                                </div>
                                @endif
                                @endforeach
                            </div>
                            <!--.row-->
                        </div>
                    @endfor
                    <!--.item-->
                </div>
                <!--.carousel-inner-->
            </div>
            <!--.Carousel-->
        </div>
    </div>

		<br>
		<br>

		<div class="jumbotron" style="padding: 10px;">
			<div class="container">
				<p style="color: orange; margin-left: 40px; margin-bottom: -1px;"><i>Highlight</i></p>
				<div class="row">
					<div class="col-md-8">
						<h2 style="margin-left: 40px;">{{$detail->event_data[0]->eventname }}</h2>
					</div>
					<div class="col-md-4">
						@foreach($detail->event_data[0]->space_category as $key => $value)	
							<p class="btn" style="background-color: #ff9649; color: white;">{{ $value->eventcatname }}</p>
						@endforeach
					</div>
				</div>
				<hr style="background-color: orange; height: 1px;">
				<div class="row">
					<div class="col-md-3">
						<p><img src="{!! asset('assets/master/img/search-engine/date.png') !!}" width="20" height="20">&nbsp; 
						@php $startdate = strtotime($detail->event_data[0]->startdate); @endphp
						@php $finishdate = strtotime($detail->event_data[0]->finishdate); @endphp
						{{ date('d-m-Y', $startdate) }} - {{ date('d-m-Y', $finishdate) }}</p>
						<input type="text" hidden id="finishdateValue" name="finishdateValue" value="{{ $detail->event_data[0]->finishdate }}">
						<p style="font-size: 15px; line-height: 130%;"><img src="{!! asset('assets/master/img/search-engine/location.png') !!}" width="20" height="20">&nbsp; {{$detail->event_data[0]->full_address}}</p>
						<p><img src="{!! asset('assets/master/img/search-engine/rp.png') !!}" width="20" height="20">&nbsp; RP {{$detail->event_data[0]->minprice }} - RP {{$detail->event_data[0]->maxprice }}</p>
						<p><img src="{!! asset('assets/master/img/jam.png') !!}" width="20" height="20">&nbsp; {{$detail->event_data[0]->starttime }} - {{$detail->event_data[0]->finishtime }}</p>
						<br>
						<p>PENGELOLA / EVENT ORGANIZER</p>
						<p style="margin-bottom: -3px; margin-top: -15px; color: orange;">{{$detail->event_data[0]->space_provider->spaceprovname }}</p>
					</div>
					@if(isset($detail->event_data[0]->latitude))
						<input type="text" hidden name="latitude" id="latitude" value="{{ $detail->event_data[0]->latitude }}">
						<input type="text" hidden name="longitude" id="longitude" value="{{ $detail->event_data[0]->longitude }}">
						<div class="col-md-4">
							<div id="map">{!! Mapper::render() !!}</div>
						</div>
					@else
						<div class="col-md-4">
							<h6>Lokasi tidak dapat ditampilkan di map</h6>
						</div>
					@endif
					<div class="col-md-5">
						<p><b>DESKRIPSI ACARA</b></p>
						<p>{!! $detail->event_data[0]->desc !!}</p>
						<p style="margin-bottom: -3px;"><b>KONDISI DAN SYARAT</b></p>
						<a href="{{$pointing->pointing()}}fileGet/{{ $detail->event_data[0]->space_provide_term[0]->spacetermpath }}" style="color: orange;" target="_blank"><i>lihat</i></a>
					</div>
				</div>
			</div>
		</div>

		<div class="jumbotron" style="padding: 10px;">
			<form action="{{ env('APP_URL') . 'detail-event/orderBookingProses' }}" method="POST">
				@csrf
				<center>
					<h6>DENAH PAMERAN</h6>
					<br>
					@if(intval(strtotime($detail->event_data[0]->finishdate)) > intval(strtotime(date("Y-m-d"))))
						<div class="row">
							<div class="col-md-4"></div>
							<div class="col-md-2">
								<span><b @if($detail->event_data[0]->opendateflag == 0) style="display: none;" @endif>Start Booking Date</b></span>
								<input style="width: 175px;" type="date" name="startbookingdate" id="startbookingdate" class="form-control" value="{{$detail->event_data[0]->startdate }}" required @if($detail->event_data[0]->opendateflag == 0) hidden @endif>
							</div>
							<div class="col-md-2">
								<span><b @if($detail->event_data[0]->opendateflag == 0) style="display: none;" @endif>Last Booking Date</b></span>
								<input style="width: 175px;" type="date" name="lastbookingdate" id="lastbookingdate" class="form-control" value="{{$detail->event_data[0]->finishdate }}" required @if($detail->event_data[0]->opendateflag == 0) hidden @endif>
							</div>
							<div class="col-md-4"></div>
						</div>
					@else
						<p style="color:red">EVENT SUDAH BERAKHIR MOHON UNTUK MENCOBA EVENT LAIN</p>
					@endif
					<br>
					@if(isset($detail->event_data[0]->space_provide_layout[0]->layoutpath))
					<div id="blogCarousel2" class="carousel slide" data-ride="carousel" data-interval="false">

		            	<!-- COUNT ROW IMAGE IN EVENT DETAIL -->
		                <ol class="carousel-indicators">
		                    <?php 
		                    	$countRaw = count($detail->event_data[0]->space_provide_layout);
		                        if ($countRaw == 1) {
		                            $event = 1;
		                        } elseif ($countRaw > 1) {
		                        	$event = 2;
		                        } elseif ($countRaw > 2) {
		                        	$event = 3;
		                        } elseif ($countRaw > 3) {
		                        	$event = 4;
		                        } elseif ($countRaw > 4) {
		                        	$event = 5;
		                        }
		                    ?>
		                    @for($i = 0; $i < $event; $i++)
		                    <li data-target="#blogCarousel2" data-slide-to="{{$i}}" @if($i == 0)class="active"@endif hidden></li>
		                    @endfor
		                </ol>

		                <!-- Carousel items -->
		                <div class="carousel-inner">
		                    @for($i = 0; $i < $event; $i++)
		                        <div @if($i == 0)class="carousel-item active" @else class="carousel-item" @endif>          
		                            <div class="row">
		                                @foreach($detail->event_data[0]->space_provide_layout as $key => $value)
		                                @if($i == 0 && $key <= 0)
		                                <div class="col-md-12">
		                                    <img src="{{$pointing->pointing()}}picGet/{{ $value->layoutpath}}" height="400" width="400"/>
		                                </div>
		                                @elseif($i == 1 && $key > 0)
		                                <div class="col-md-12">
		                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->layoutpath}}" height="400" width="400"/>
		                                </div>
		                                @elseif($i == 2 && $key > 1)
		                                <div class="col-md-12">
		                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->layoutpath}}" height="400" width="400"/>
		                                </div>
		                                @elseif($i == 3 && $key > 2)
		                                <div class="col-md-12">
		                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->layoutpath}}" height="400" width="400"/>
		                                </div>
		                                @elseif($i == 4 && $key > 3)
		                                <div class="col-md-12">
		                                	<img src="{{$pointing->pointing()}}picGet/{{ $value->layoutpath}}" height="400" width="400"/>
		                                </div>
		                                @endif
		                                @endforeach
		                            </div>
		                            <!--.row-->
		                        </div>
		                    @endfor
		                    <!--.item-->
		                </div>
		                <!--.carousel-inner-->
		                <a class="carousel-control-prev" href="#blogCarousel2" role="button" data-slide="prev">
						    <span class="carousel-control-prev-icon" aria-hidden="true" style="margin-right: -450px; background-color: #ff9649;"></span>
						    <span class="sr-only">Previous</span>
					  	</a>
					  	<a class="carousel-control-next" href="#blogCarousel2" role="button" data-slide="next">
						    <span class="carousel-control-next-icon" aria-hidden="true" style="margin-left: -450px; background-color: #ff9649;"></span>
						    <span class="sr-only">Next</span>
					  	</a>
		            </div>
					@endif
					<br>
					<br>
					<input type="text" name="spaceid" id="spaceid" value="{{$detail->event_data[0]->spaceid}}" style="display: none;">

					<!-- CEK USER LOGIN VIA TOKEN -->
					<input type="text" name="cektoken" id="cektoken" value="{{ Session::get('my_token___') }}" style="display: none;">

					@if(intval(strtotime($detail->event_data[0]->finishdate)) > intval(strtotime(date("Y-m-d"))))

					<div id="data-event" data-startdate="" data-lastdate="">
					</div>
					{{-- @foreach ($detail->event_data[0]->space_provide_layout as $key => $value)
						<input class="{{substr($value->space_provide_layout_grid[0]->code,0,1)}} btn classevent" data-id="{{$key}}" value="{{substr($value->space_provide_layout_grid[0]->code,0,1)}}" type="button">
					@endforeach
					<div class="row">
						<div class="col-md-3"></div>
							<div class="col-md-4">
								<div class="ex1" id="data-event-append">	
										@if(isset($detail->event_data[0]->space_provide_layout[0]->space_provide_layout_grid))
											@foreach($detail->event_data[0]->space_provide_layout as $i => $v)
												@foreach($detail->event_data[0]->space_provide_layout[$i]->space_provide_layout_grid as $key => $value)
													@if($value->grid_status == 1 || $value->brandname != null)
														<div @if($i != 0)class="d-none"@else class="d-flex bd-highlight" @endif style="background: black; color: white; border-bottom: 1px solid #fff">
															<div class="p-2 bd-highlight check">
																<input data-key="{{$key}}" @if($value->grid_status == 1 || $value->brandname != null) hidden @endif type="checkbox" name="gridselected" value="{{ $value->layoutgridid }}">
															</div>
															<div class="p-2 bd-highlight">&nbsp;&nbsp;&nbsp;{{ $value->code }}</div>
														</div>
													@else
														<div @if($i != 0)class="d-none"@else class="d-flex bd-highlight testbg" @endif style="color: black; border-bottom: 1px solid #fff">
															<div class="p-2 bd-highlight check">
															<input class="check-event" data-key="{{$key}}" @if($value->grid_status == 1 || $value->brandname != null) disabled @endif type="checkbox" data-id="{{ $value->code }}" name="gridselected[]" value="{{ $value->layoutgridid }}">
															</div>
															<div class="p-2 bd-highlight">{{ $value->code }}</div>
														</div>
													@endif
												@endforeach
											@endforeach
										@endif
								</div>
							</div>
							</div>
							<div class="col-md-2">
								<div class="display-data" id="display-data">
									<ul>
										<li>A1 <i class="fas fa-times"></i></li>
									<ul>
								</div>
							</div>
						<div class="col-md-3"></div>
					</div> --}}
					@endif
					{{-- <br>
					<br> --}}
					@php
						$today = date("Y-m-d H:i:s");
						$finishdateevent = $detail->event_data[0]->finishtime;
					@endphp
					@if(Session::get('my_token___') == null)
						<button type="button" class="btn btn-default" id="btnLogin" style="background-color: #ff9649; color: white;" data-toggle="modal" data-target="#modallogin">HARAP LOGIN UNTUK MEMULAI NEGOISASI</button>
					@else
						@if(intval(strtotime($detail->event_data[0]->finishdate)) > intval(strtotime(date("Y-m-d"))))
							<button type="submit" class="btn btn-default btnSubmitNego" id="btnSubmit" style="background-color: #ff9649; color: white;">NEGOSIASI HARGA</button>
						@endif
					@endif
				</center>
			</form>
		</div>

		<div class="jumbotron" style="padding: 10px;">
			<ul class="nav nav-tabs" id="myTab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="aboutevent-tab" data-toggle="tab" href="#aboutevent" role="tab" aria-controls="aboutevent" aria-selected="true">TENTANG EVENT</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="facility-tab" data-toggle="tab" href="#facility" role="tab" aria-controls="facility" aria-selected="false">FASILITAS</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="playrole-tab" data-toggle="tab" href="#playrole" role="tab" aria-controls="playrole" aria-selected="false">ATURAN MAIN</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="cancelrole-tab" data-toggle="tab" href="#cancelrole" role="tab" aria-controls="cancelrole" aria-selected="true">ATURAN PEMBATALAN</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false">REVIEW PENYELENGGARA</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="question-tab" data-toggle="tab" href="#question" role="tab" aria-controls="question" aria-selected="false">PERTANYAAN</a>
			  </li>
              <li class="nav-item">
			    <a class="nav-link" id="privatequestion-tab" data-toggle="tab" href="#privatequestion" role="tab" aria-controls="privatequestion" aria-selected="false">PERTANYAAN PRIBADI</a>
			  </li>
			</ul>
			<hr style="margin-top: -2px; background-color: #ff9649; height: 3px;">
			<br>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane fade show active" id="aboutevent" role="tabpanel" aria-labelledby="aboutevent-tab">
			  	<h3>DESKRIPSI EVENT <b>{!! $detail->event_data[0]->eventname !!}</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	{!! $detail->event_data[0]->desc !!}
			  </div>
			  <div class="tab-pane fade" id="facility" role="tabpanel" aria-labelledby="facility-tab">
			  	<h3>FASILITAS <b>TERSEDIA</b></h3>
			  	<hr style="background-color: #ff9649;">
                {!! $detail->event_data[0]->facility !!}
			  </div>
			  <div class="tab-pane fade" id="playrole" role="tabpanel" aria-labelledby="playrole-tab">
			  	<h3>ATURAN <b>MAIN</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	{!! $detail->event_data[0]->rule !!}
			  </div>
			  <div class="tab-pane fade" id="cancelrole" role="tabpanel" aria-labelledby="cancelrole-tab">
			  	<h3>ATURAN <b>PEMBATALAN</b></h3>
			  	<hr style="background-color: #ff9649;">
			  	{!! $detail->event_data[0]->cancelrule !!}
			  </div>
			  <div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
			  	<h6 style="color: #ff9649">DISELENGGARAI OLEH</h6>
			  	<h3>{!! $detail->event_data[0]->space_provider->spaceprovname !!}</h3>
			  	<p>{!! $detail->event_data[0]->space_provider->spdesc !!}</p>
			  </div>
			  <div class="tab-pane fade" id="question" role="tabpanel" aria-labelledby="question-tab">
			  	<h3>PERTANYAAN MENGENAI EVENT <b>{!! $detail->event_data[0]->eventname !!}</b></h3>
                @foreach($detail->event_data[0]->space_provide_question as $question)
                    <div id="user">
			  		    <p style="color: grey;"><img src="/assets/master/img/event/default-img.png" width="30" height="30"> {!! $question->creator !!}</p>
				  	    <hr style="background-color: #ff9649;">
				  	    <p>{!! $question->questionval !!}</p>
			  	    </div>
                @endforeach
			  </div>
              <div class="tab-pane fade" id="privatequestion" role="tabpanel" aria-labelledby="privatequestion-tab">
			  	<h3>PERTANYAAN PRIBADI</h3>
			  	<hr style="background-color: #ff9649;">
                @foreach($detail->event_data[0]->space_provide_private_question as $question)
                    <div id="user">
			  		    <p style="color: grey;"><img src="/assets/master/img/event/default-img.png" width="30" height="30"> {!! $question->creator !!}</p>
				  	    <hr style="background-color: #ff9649;">
				  	    <p>{!! $question->questionval !!}</p>
			  	    </div>
                @endforeach
			  </div>
			</div>
		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script>
		function initMap() {
	    	var getlatitude = document.getElementById("latitude").value;
	    	var getlongitude = document.getElementById("longitude").value;
		  	var pos = {lat: parseFloat(getlatitude), lng: parseFloat(getlongitude)}; 
		  	var map = new google.maps.Map(
		      document.getElementById('map'), {zoom: 10, center: pos});
		  	var marker = new google.maps.Marker({position: pos, map: map});
		}
	</script>
    <script async defer
    	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCwy2ovumz1j66BUMzrtdHDk9gCufHFkDw&callback=initMap">
	</script>


    <script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>


    {{-- <script crossorigin src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script> --}}

    {{-- <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.5.9/firebase-messaging.js"></script> --}}


    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
	

<script id="event_detail" type="text/babel" src="{{url('/')}}/assets/master/jsx/event_detail.jsx?endpoint={{ $pointing->pointing() }}&event_id={{Request::segment(2)}}"></script>

	<script>
	</script>

	<!-- ALERT WHEN USER NOT LOGIN -->
	@if(session('error_code') == 5)
	<script type="text/javascript">
	</script>
	@endif

	<!-- ALERT WHEN USER NOT CHOOSE ONE OF CHECKBOX -->
	<script type="text/javascript">
        var global = {
            changeStartDate: function(date) { },
            changeLastDate: function(date) { }
        };
    
		$(document).ready(function () {
            // default value dari attribute data-event start-last date
            $("#data-event").attr("data-startdate", "{{ $detail->event_data[0]->startdate }}");
            $("#data-event").attr("data-lastdate", "{{ $detail->event_data[0]->finishdate }}");

            // setiap ada perubahan startdate, ganti attr nya data-event (ini react uncleannya zona selector),
            // dan trigger global event yang ditangkep react
            $("#startbookingdate").change(function() {
                $("#data-event").attr("data-startdate", $(this).val());
                global.changeStartDate($(this).val());
            });
            $("#lastbookingdate").change(function() {
                $("#data-event").attr("data-lastdate", $(this).val());
                global.changeLastDate($(this).val());
            });

		    $('.btnSubmitNego').click(function() {
		      checked = $("input[type=checkbox]:checked").length;

		       // TODAY
		    var today = new Date();
			var month = today.getMonth()+1;
			var day = today.getDate();
			var todayValue = today.getFullYear() + '-' +
			    ((''+month).length<2 ? '0' : '') + month + '-' +
			    ((''+day).length<2 ? '0' : '') + day;

			// START BOOKING DATE
			var startbookingdate = new Date($('#startbookingdate').val());
      		var month = startbookingdate.getMonth()+1;
			var day = startbookingdate.getDate();
		    var startbookingdateValue = startbookingdate.getFullYear() + '-' +
		    ((''+month).length<2 ? '0' : '') + month + '-' +
		    ((''+day).length<2 ? '0' : '') + day;

		    // LAST BOOKING DATE
		    var lastbookingdate = new Date($('#lastbookingdate').val());
      		var month = lastbookingdate.getMonth()+1;
			var day = lastbookingdate.getDate();
		    var lastbookingdateValue = lastbookingdate.getFullYear() + '-' +
		    ((''+month).length<2 ? '0' : '') + month + '-' +
		    ((''+day).length<2 ? '0' : '') + day;

		    // GET FINISH DATE EVENT VALUE
		    var finishdateValue = new Date($('#finishdateValue').val());
		    var month = finishdateValue.getMonth()+1;
			var day = finishdateValue.getDate();
		    var finishdateGetValue = finishdateValue.getFullYear() + '-' +
		    ((''+month).length<2 ? '0' : '') + month + '-' +
		    ((''+day).length<2 ? '0' : '') + day;

		      if(!checked) {
		        alert("Setidaknya pilih salah satu stan untuk memulai negoisasi harga.");
		        return false;
		      }

		      if(startbookingdateValue < todayValue) {
		      	alert("Waktu awal pemesanan tidak boleh kurang dari hari ini.");
		      	return false;
		      } 

		      if(lastbookingdateValue < startbookingdateValue) {
		      	alert("Waktu akhir pemesanan tidak boleh kurang dari waktu awal pemesanan");
		      	return false;
			  }

			  if(lastbookingdateValue > finishdateGetValue) {
		      	alert("Waktu akhir pemesanan tidak boleh lebih dari waktu event berakhir");
		      	return false;
			  }
		    });
		});
	</script>

	<!-- CHANGED COLOR BG CHECKBOX WHEN CHECKED -->
	<script>
		$('document').ready(function(){
			$('input[type="checkbox"]').click(function(){
				if($(this).prop("checked") == true){
					//alert("Checkbox is checked.");
					$(this).parent().parent().addClass("checkedBox");
				} else if($(this).prop("checked") == false) {
					//alert("Checkbox is unchecked.");
					$(this).parent().parent().removeClass("checkedBox");
				}
			});
		});
	</script>
@stop
