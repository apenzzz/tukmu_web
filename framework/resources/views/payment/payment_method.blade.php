@extends('layouts.newmaster')
@section('style')
	<style type="text/css">
		.jumbotron{
			padding-top: 20px;
			padding-left: 25px;		
		}

		.hide{
			display: none;
		}

		.imageContainer {
	       background-image: url('{!! asset('assets/master/img/bayar.png') !!}');
	       background-repeat: no-repeat;
	       background-size: 150px;
 		}
 		
 		#time-count-down {
          margin-bottom: 15px;
        }
        #time-count-down .p-2 {
          background: orange;
          margin-right: 1px;
          color: #fff;
          font-size: 30px;
          padding: 0px 7px !important;
          font-weight: 700;
        }
        #time-count-down .p-2.titik-dua {
          background: transparent;
          color: orange;
        }
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin: 0px;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            color: darkorange;
            margin-right: 5px;
        }
	</style>
@stop
@section('content')
    @include('common.header')
	<div class="container" style="margin-top: 70px; margin-bottom: 20px;">
		<div>
			<h3>TERIMA KASIH!</h3>
			<p>Pesanan Anda Telah Kami Terima</p>
		</div>
		<form action="{{ env('APP_URL') . 'submitpaymentmethod' }}" method="POST">
			@csrf
			<input type="text" id="cpyid" name="cpyid" value="{{ $paymentsummary->customer_payment_deposit->custpaymentid }}" hidden>
			<div class="row">
				<div class="col-md-8">
                	<div class="jumbotron jumbotron-fluid">
					    <div class="container">
                            <input type="hidden" name="bankcode" id="bankcode" />
                            <input type="hidden" name="productcode" id="productcode" />
                            <input type="hidden" name="espayredirect" id="espayredirect" />
                            @foreach($paymentmethod as $eachpaymentmethod)
                                <div class="radio">
                                    <label><input type="radio" name="paymentmethod" value="{{ $eachpaymentmethod->methodid }}" data-bankcode="{{ $eachpaymentmethod->bankCode }}" data-productcode="{{ $eachpaymentmethod->productCode }}" data-espayredirect="{{ $eachpaymentmethod->espayRedirect }}" required>{{ $eachpaymentmethod->methodname }}</label>
                                </div>
                                <!-- bankCode, productCode -->
                            @endforeach
                        </div>
                    </div>
				</div>
				<div class="col-md-4">
					<div class="jumbotron jumbotron-fluid">
					  <div class="container" style="font-weight: bold;">
					    <h5>INDEPENDENCE DAY</h5>
					    <p class="f-p-s"><img src="{!! asset('assets/master/img/search-engine/date.png') !!}" width="15" height="17">&nbsp; {{ date("d-M-Y", strtotime($paymentsummary->event_data->startdate)) }} - {{ date("d-M-Y", strtotime($paymentsummary->event_data->finishdate)) }}</p>
			    		<p class="f-p-s"><img src="{!! asset('assets/master/img/search-engine/location.png') !!}" width="15" height="17">&nbsp; {{ $paymentsummary->event_data->spacename }}</p>
			    		<p>PENGELOLA / EVENT ORGINIZER <a href="#" style="color: #ff9649;"><br>{{ $paymentsummary->event_data->space_provider->spaceprovname }}</a></p>
			    		<p>LOKASI</p>
			    		    <ul class="lokasi-list">
			    			<?php
				    			$length = count($paymentsummary->payment_data->payment_grid);
								for ($i = 0; $i < $length; $i++) {
									echo "<li>";
									if($length == $i){
								  	    echo $paymentsummary->payment_data->payment_grid[$i]->code;
									} else {
								  	    echo $paymentsummary->payment_data->payment_grid[$i]->code.",";
									}
								  	echo "</li>";
								} 
							?>
			    		</ul>
					  </div>
					  <div class="container">
					  	<hr style="background-color: #ff9649;">
					  	<h5 style="color: #ff9649; margin-top: -14px; margin-bottom: -10px;"><img src="{!! asset('assets/master/img/search-engine/rp.png') !!}" width="25" height="25">&nbsp; <b>{{ $paymentsummary->grand_total_payment }}</b></h5>
					  	<hr style="background-color: #ff9649;">
					  	<!-- <h2>{{ $paymentsummary->payment_data->limit_count_down }}</h2> -->

	                                        <div id="time-count-down" class="d-flex">
	                                            <div class="p-2 hours-1" id="hours-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 hours-2" id="hours-2">
	                                                0
	                                            </div>
	                                            <div class="p-2 titik-dua">
	                                                :
	                                            </div>
	                                            <div class="p-2 minutes-1" id="minutes-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 minutes-2" id="minutes-2">
	                                                0
	                                            </div>
	                                            <div class="p-2 titik-dua">
	                                                :
	                                            </div>
	                                            <div class="p-2 seconds-1" id="seconds-1">
	                                                0
	                                            </div>
	                                            <div class="p-2 seconds-2" id="seconds-2">
	                                                0
	                                            </div>
	                                        </div>
	                                        <div id="timeout" class="d-none">
	                                            <h3 class="c-red">WAKTU ANDA TELAH HABIS!</h3>
	                                        </div>
					  	<p style="font-size: 12px; display: block; background-color: #ff9649; color: white; text-align: center;">HARGA BERLAKU SAMPAI {{ $paymentsummary->payment_data->limit_payment }}</p>
					  	<p style="margin: auto;"><b>DESKRIPSI ACARA</b></p>
                        <?php echo $paymentsummary->event_data->desc ?>
					  	<button type="submit" id="payButton" class="imageContainer" style="width: 100px; height: 40px; text-align: right; color: white; cursor: pointer;">BAYAR</button>					  	
					  </div>
					</div>
				</div>
			</div>
		</form>
	</div>
@stop
@section('script')
    <script>
        $(function() {
            $("input[name = 'paymentmethod']").change(function() {
                $("#bankcode").val($(this).data("bankcode"));
                $("#productcode").val($(this).data("productcode"));
                $("#espayredirect").val($(this).data("espayredirect"));
            });
        });
    </script>

	<script type="text/javascript">
		function tab1(){
		  document.getElementById('div1').style.display ='block';
		}
		function tab2(){
		  document.getElementById('div1').style.display = 'none';
		  document.getElementById('div2').style.display = 'none';
		}
		function tab3(){
		  document.getElementById('div2').style.display = 'block';
		}
	</script>
    <script>
            // Set the date we're counting down to
            var countDownDate = new Date('{{ $paymentsummary->payment_data->limit_payment }}').getTime();
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
                // Get todays date and time
                var now = new Date().getTime();
                
                // Find the distance between now and the count down date
                var distance = countDownDate - now;
                
                // Time calculations for days, hours, minutes and seconds
                // var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
                var hours1 = hours.toString().slice(0,1);
                var hours2 = hours.toString().slice(1,2);

                var minutes1 = minutes.toString().slice(0,1);
                var minutes2 = minutes.toString().slice(1,2);
                
                var seconds1 = seconds.toString().slice(0,1);
                var seconds2 = seconds.toString().slice(1,2);
                
                if(hours < 10) {
                    document.getElementById("hours-1").innerHTML = 0;
                    document.getElementById("hours-2").innerHTML = hours1;
                } else {
                    document.getElementById("hours-1").innerHTML = hours1;
                    document.getElementById("hours-2").innerHTML = hours2;
                }

                if(minutes < 10) {
                    document.getElementById("minutes-1").innerHTML = 0;
                    document.getElementById("minutes-2").innerHTML = minutes1;
                } else {
                    document.getElementById("minutes-1").innerHTML = minutes1;
                    document.getElementById("minutes-2").innerHTML = minutes2;
                }

                if(seconds < 10) {
                    document.getElementById("seconds-1").innerHTML = 0;
                    document.getElementById("seconds-2").innerHTML = seconds1; 
                } else {
                    document.getElementById("seconds-1").innerHTML = seconds1;
                    document.getElementById("seconds-2").innerHTML = seconds2; 
                }

                // If the count down is over, write some text 
                if (distance < 0) {
                    clearInterval(x);
                    document.getElementById("timeout").classList.remove("d-none")
                    document.getElementById("time-count-down").classList.remove("d-flex")
                    document.getElementById("time-count-down").classList.add("d-none")
                }
            }, 1000);
    </script>
@stop
