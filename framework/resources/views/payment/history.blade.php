@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divPaymentHistory" data-accesstoken="{{session('my_token___')}}" data-cpyid="{{$cpyid}}" ></div>
@stop
@section('script')
    <script src="{{ url('framework/public/js/payment/history.js') }}"></script>
@stop
