@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div class="container content m-t-15 m-b-15">
        <div class="row">
            <div class="col-sm-12">
                <div class="jumbotron jumbotron-fluid">
					<div class="container">
					    @if($paymentsummary->payment_data->paymentmethod == 10 || $paymentsummary->payment_data->paymentmethod == 11)
                            <!-- transfer bca / mandiri (manual) -->
                            <form action="{{ env('APP_URL') . 'payment/confirmation/submituploadreceipt' }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <input type="text" id="cpyid" name="cpyid" value="{{ $paymentsummary->payment_data->custpaymentid }}" hidden>
                                <div class="form-group">
                                    <label>Nama Bank</label>
                                    <input type="text" className="form-control" placeholder="Nama Bank" name="namabank"  required/>
                                </div>
                                <div class="form-group">
                                    <label>Nama Akun</label>
                                    <input type="text" className="form-control" placeholder="Nama Akun" name="namaakun" required/>
                                </div>
                                <div class="form-group">
                                    <label>Bukti Bayar</label>
                                    <input type="file" className="form-control-file" id="filebuktibayar" name="filebuktibayar" required />
                                </div>
                                <button type="submit" class="btn bg-orange">Submit</button>
                            </form>
                        @elseif($paymentsummary->payment_data->paymentmethod == 308)
                            <!-- credit card (espay), redirected, handled by controller -->
                        @elseif($paymentsummary->payment_data->paymentmethod == 301)
                            <!-- permata va (espay) -->
                            <p>Permata VA Number:<p>
                            <p>
                                <?php
                                    if(!is_null($paymentsummary->payment_data->customer_payment_va)) {
                                        echo $paymentsummary->payment_data->customer_payment_va->paymentva;
                                    }
                                ?>
                            </p>
                        @endif
					</div>
				</div>
            </div>
        </div>
    </div>
@endsection

