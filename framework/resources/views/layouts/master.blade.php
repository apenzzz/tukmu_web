<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta content="Web Service" name="description" />
	<meta content="Tukmu Web Service" name="author" />
	<link rel="shortcut icon" href="{{asset('img/favicon.ico')}}">
	<title>@yield('title', 'Home - Tukmu.com')</title>
    <link rel="stylesheet" type="text/css" href="{!! asset('assets/master/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="{{asset('assets/master/css/main.css')}}" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    @yield('rp_css')
    @yield('up_css')
    @yield('ep_css')
    @yield('st_css')
    @yield('dt_css')
    @yield('pm_css')
    <style>
    .row.list-content {
        border-bottom: 10px solid #fff;
        padding-bottom: 10px
    }
    .result_search .row:last-child {
        border-bottom: 0px !important;
    }
    </style>
</head>
<body>
    @yield('content')
    <br>
    <br>
    @yield('footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/babel-standalone@6.15.0/babel.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase.js"></script>
    @if(Request::segment(1) != 'detail-event')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    {{-- <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-app.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.7.1/firebase-messaging.js"></script> --}}
    {{-- <script src="{{url('/')}}/firebase_messaging_sw.js"></script> --}}
    @yield('rp_js')
    @yield('pm_js')
    <script>
         $('#email_peng').change(function () {
            var email_peng = $('#email_peng').val();
            var cekemail = {
                "async": true,
                "crossDomain": true,
                "url": "https://tukbase3.tukmu.com/api/customer/exists?email="+email_peng,
                "method": "GET"
            }
            $.ajax(cekemail).done(function (response) {
                if (response.status === "1") {
                    $("#emailnotverif").hide();
                } else {
                    $("#emailnotverif").show();
                }
            })
        });
    </script>
    <script>
        $('document').ready(function(){
            var base_url = '{{URL::to('/')}}';
            var resultlimit = 0
            var page = 1;
            var search_params = $('#search_params').val();
            var end_date = $('#end_date').val();
            var start_date = $('#start_date').val();
            var category = $('#category__').val();
            $('#daftar').click(function(){
            });
            pageNextOrPrev = (pagenumb) => {
                $('html, body').animate({
                    scrollTop: $("#result_search").offset().top
                }, 2000);
                $('#result_search').empty();
                $('#pagination  .pagination').empty();
                resultlimit = 0;
                loadData(pagenumb);
            }
            function changeResultLimit(data) {
                resultlimit = data;
            }
            loadData = (pagenumb) => {
                search_params = $('#search_params').val();
                end_date = $('#end_date').val();
                start_date = $('#start_date').val();
                category = $('#category__').val();
                $(".result_search .row:last-child").css({'border':'0px'})
                page = pagenumb;
                console.log(page)
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": false,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                if(resultlimit === 0){
                    $.ajax(settings).done((response) => {
                        if(response.status === "1"){
                            changeResultLimit(1)
                            var content = "";
                            var pagination = "";
                            if(response.total_page > 1){
                                $("#pagination").removeClass("d-none")
                                for(var i = 1; i <= response.total_page; i++){
                                    var pageactive = '';
                                    if(page == i){
                                        pageactive = 'active';
                                    } else {
                                        pageactive = '';
                                    }
                                    pagination +=
                                        '<li class="page-item '+pageactive+'" onclick="pageNextOrPrev('+i+')">'+
                                            '<a class="page-link" href="javascript:;">'+
                                                ''+i+''+
                                            '</a>'+
                                        '</li>';
                                }
                                $('#pagination .pagination').empty();
                                $('#pagination .pagination').append(pagination);
                            }
                            response.search_value.forEach((value, index) => {
                                var img_logo = '';
                                var pemisah = '';
                                var arrstartdate = value.startdate.split('-');
                                var arrfinishdate = value.finishdate.split('-');
                                var newstartdate = arrstartdate[1] + '-' + arrstartdate[2] + '-' + arrstartdate[0];
                                var newfinishdate = arrfinishdate[1] + '-' + arrfinishdate[2] + '-' + arrfinishdate[0];
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>'+value.eventname+'</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+newstartdate+' - '+newfinishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                            });

                            $('#result_search').append(content);
                        } else {
                            console.log("Gagal")
                            $("#loading-cari").addClass("d-none")
                            $("#fixtime").addClass("d-none")
                            $("#flexibletime").addClass("d-none")
                            $("#hasil_pencarian").addClass("d-none")
                            $("#result_div").addClass("d-none")
                            changeResultLimit(1)
                        }
                    });
                }
                if(resultlimit === 1) {
                    resultlimit = 0;
                    $("#loading-cari").addClass("d-none")
                    $("#fixtime").removeClass("d-none")
                    $("#flexibletime").removeClass("d-none")
                    $("#hasil_pencarian").removeClass("d-none")
                    $("#result_div").removeClass("d-none")
                }
            }
            $('#cari_event').click(function(e){
                $("#loading-cari").removeClass("d-none")
                $("#fixtime").addClass("d-none")
                $("#flexibletime").addClass("d-none")
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                resultlimit = 0;
                loadData(page)
            });
            $('#fixtime').on('click', function(){
                $('#result_search').empty()
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        var content = "";
                        var data_content = [];
                        var i = 1;
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';
                            if(value.opendateflag === 0){
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                                i ++;
                                data_content.push(value.opendateflag)
                            }
                        });
                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }
                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                });
            });
            $('#flexibletime').on('click', function(){
                $('#result_search').empty()
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        var content = "";
                        var data_content = [];
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';
                            if(value.opendateflag === 1){
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                                data_content.push(value.opendateflag)
                            }
                        });
                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }
                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                });
            });
        });
    </script>
    @endif
</body>
</html>
