@extends('layouts.newmaster')
@section('content')
    @if (session('error_code') == 5)
        <script>
            alert("Mohon untuk melakukan login terlebih dahulu!");
        </script>
    @endif
    @if (session('error_code') == 1)
        <script>
            alert("Email atau Password salah! pastikan Email atau Password anda benar dan coba lagi.");
        </script>
    @endif
    <!-- Header -->
    @include('common.header')
    <!-- Carousel -->
    <div id="carousel" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carousel" data-slide-to="0" class="active"></li>
        <li data-target="#carousel" data-slide-to="1"></li>
        <li data-target="#carousel" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="assets/master/img/home/slider/001.jpg" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="assets/master/img/home/slider/002.jpg" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="assets/master/img/home/slider/003.jpg" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <!-- Search Box -->
    <div class="container mt-5">
	    <div class="row">
		    <div class="col-md-1"></div>
		    <div class="col-md-10">
			    <div class="jumbotron">
				    <div class="row">
				  	    <div class="col-md-5">
				  		    <h6>CARI</h6>
				  		    <input type="text" class="form-control" name="search_params" id="search_params">
				  	    </div>
				  	    <div class="col-md-2">
				  		    <h6 style="font-size: 14px;">TANGGAL MULAI</h6>
				  		    <input class="form-control" type="date" name="start_date" id="start_date">
				  	    </div>
                        <div class="col-md-2">
                            <h6 style="font-size: 14px;">TANGGAL SELESAI</h6>
                            <input class="form-control" type="date" name="end_date" id="end_date">
                        </div>
				  	    <div class="col-md-3">
				  		    <h6 style="font-size: 14px;">KATEGORI EVENT</h6>
                            <select class="form-control" name="category__" id="category__">
                                <option value="0">Pilih Kategori</option>
                                @foreach ($category->category_list as $key => $value)
                                <option value="{{$value->eventcatid}}">{{$value->eventcatname}}</option>
                                @endforeach
                            </select>
				  	    </div>
				    </div>
				    <br><br>
				    <div class="row">
				  	    <div class="col-md-5"></div><!--  -->
				  	    <div class="col-md-2">
				  		    <button type="button" id="cari_event" class="btn btn-defaullt" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white"><b>CARI
                            &nbsp;<i id="loading-cari" class="fas fa-spinner fa-spin d-none"></i></b></span></button>
				  	    </div>
				  	    <div class="col-md-5"></div>
				    </div>
			    </div>
		    </div>
		    <div class="col-md-1"></div>
        </div>
            <div class="row">
			    <div class="col-md-2"></div>
			    <div class="col-md-8">
				    <div class="row">
					    <button id="fixtime" class="btn btn-defaullt d-none" style="margin-right: 20px; margin-left: 15px; color: white; background-color: rgb(143,143,143);">FIX TIME</button>
					    <button id="flexibletime" class="btn btn-defaullt d-none" style="background-color: #ff9649; color: white;">FLEXIBLE TIME</button>
				    </div>
				    <br>
				    <h4 id="hasil_pencarian" class="d-none">HASIL PENCARIAN</h4>
				    <br>
                    <div class="jumbotron d-none" id="result_div" style="padding: 10px;">
                        <div id="result_search">
                        </div>
				    </div>
                    <div id="pagination" class="d-none" style="margin: auto;display:block;width: 300px;max-width:350px; overflow-x:auto">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                {{-- <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li> --}}
                            </ul>
                        </nav>
                    </div>
			    </div>
			    <div class="col-md-2"></div>
		    </div>
    </div>
    <!-- Upcoming Event -->
    <div class="container mb-5">
	    <center>
		    <h1>UPCOMING EVENT</h1>
	    </center>
        <hr style="background-color: orange;">
	    <br>
        <div class="row blog">
            <div class="col-md-12">
                <div id="blogCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <?php 
                            if($upcomingevent->promoted_event > 4) {
                                $event = 2;
                            } else {
                                $event = 1;
                            }
                             ?>
                        @for($i = 0; $i < $event; $i++)
                        <li data-target="#blogCarousel" data-slide-to="{{$i}}" @if($i == 0)class="active"@endif></li>
                        @endfor
                    </ol>
                    <div class="carousel-inner">
                        @for($i = 0; $i < $event; $i++)
                            <div @if($i == 0)class="carousel-item active" @else class="carousel-item" @endif>          
                                <div class="row">
                                    @foreach($upcomingevent->promoted_event as $key => $value)
                                    @if($i == 0 && $key <= 3)
                                    <div class="col-md-3">
                                        <div class="jumbotron" style="padding: 15px; height: 100%;">
                                            <center>
                                                @if(isset($value->first_media->spacemediapath))
                                                <img src="https://tukbase3.tukmu.com/picGet/{{ $value->first_media->spacemediapath }}" width="200" height="150">
                                                @endif
                                                <br>
                                                <br>
                                                @if(isset($value->eventname))
                                                <h5>{{ $value->eventname }}</h5>
                                                @endif
                                                @if(isset($value->full_address))
                                                <p class="c-gray" style="font-size: 14px; line-height: 130%;">{{ $value->full_address }}</p>
                                                @endif
                                                <br>
                                                <a href="{{ url('detail-event/'.$value->spaceid) }}" class="btn btn-default btn-sm" style="background-color: #f47b50; color: white; margin-top: -30%;">Selengkapnya</a>
                                            </center>
                                        </div>
                                    </div>
                                    @elseif($i == 1 && $key > 3)
                                    <div class="col-md-3">
                                        <div class="jumbotron" style="padding: 15px; height: 100%;">
                                            <center>
                                                @if(isset($value->first_media->spacemediapath))
                                                <img src="https://tukbase3.tukmu.com/picGet/{{ $value->first_media->spacemediapath }}" width="200" height="150">
                                                @endif
                                                <br>
                                                <br>
                                                @if(isset($value->eventname))
                                                <h5>{{ $value->eventname }}</h5>
                                                @endif
                                                @if(isset($value->full_address))
                                                <p class="c-gray" style="font-size: 14px; line-height: 130%;">{{ $value->full_address }}</p>
                                                @endif
                                                <br>
                                                <a href="{{ url('detail-event/'.$value->spaceid) }}" class="btn btn-default btn-sm" style="background-color: #f47b50; color: white; margin-top: -30%;">Selengkapnya</a>
                                            </center>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                            </div>
                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer -->
    @include('common.footer')
@endsection
@push('script')
    <script>
        $('document').ready(function(){
            var base_url = '{{URL::to('/')}}';
            var resultlimit = 0
            var page = 1;
            var search_params = $('#search_params').val();
            var end_date = $('#end_date').val();
            var start_date = $('#start_date').val();
            var category = $('#category__').val();
            $('#daftar').click(function(){
            });
            pageNextOrPrev = (pagenumb) => {
                $('html, body').animate({
                    scrollTop: $("#result_search").offset().top
                }, 2000);
                $('#result_search').empty();
                $('#pagination  .pagination').empty();
                resultlimit = 0;
                loadData(pagenumb);
            }
            function changeResultLimit(data) {
                resultlimit = data;
            }
            loadData = (pagenumb) => {
                search_params = $('#search_params').val();
                end_date = $('#end_date').val();
                start_date = $('#start_date').val();
                category = $('#category__').val();
                $(".result_search .row:last-child").css({'border':'0px'})
                page = pagenumb;
                console.log(page)
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": false,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                if(resultlimit === 0){
                    $.ajax(settings).done((response) => {
                        if(response.status === "1"){
                            changeResultLimit(1)
                            var content = "";
                            var pagination = "";
                            if(response.total_page > 1){
                                $("#pagination").removeClass("d-none")
                                for(var i = 1; i <= response.total_page; i++){
                                    var pageactive = '';
                                    if(page == i){
                                        pageactive = 'active';
                                    } else {
                                        pageactive = '';
                                    }
                                    pagination +=
                                        '<li class="page-item '+pageactive+'" onclick="pageNextOrPrev('+i+')">'+
                                            '<a class="page-link" href="javascript:;">'+
                                                ''+i+''+
                                            '</a>'+
                                        '</li>';
                                }
                                $('#pagination .pagination').empty();
                                $('#pagination .pagination').append(pagination);
                            }
                            response.search_value.forEach((value, index) => {
                                var img_logo = '';
                                var pemisah = '';
                                var arrstartdate = value.startdate.split('-');
                                var arrfinishdate = value.finishdate.split('-');
                                var newstartdate = arrstartdate[1] + '-' + arrstartdate[2] + '-' + arrstartdate[0];
                                var newfinishdate = arrfinishdate[1] + '-' + arrfinishdate[2] + '-' + arrfinishdate[0];
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>'+value.eventname+'</h5>'+
                                            '<small>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="16" height="16">&nbsp; '+newstartdate+' - '+newfinishdate+
                                            '</small><br/>'+
                                            '<small>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="16" height="16">&nbsp; '+value.spacename+
                                            '</small><br/>'+
                                            '<small>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="16" height="16">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</small><br/>'+
                                            '<small>PENGELOLA / EVENT ORGANIZER</small>'+
                                            '<br>'+
                                            '<small><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></small>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                            });

                            $('#result_search').append(content);
                        } else {
                            console.log("Gagal")
                            $("#loading-cari").addClass("d-none")
                            $("#fixtime").addClass("d-none")
                            $("#flexibletime").addClass("d-none")
                            $("#hasil_pencarian").addClass("d-none")
                            $("#result_div").addClass("d-none")
                            changeResultLimit(1)
                        }
                    });
                }
                if(resultlimit === 1) {
                    resultlimit = 0;
                    $("#loading-cari").addClass("d-none")
                    $("#fixtime").removeClass("d-none")
                    $("#flexibletime").removeClass("d-none")
                    $("#hasil_pencarian").removeClass("d-none")
                    $("#result_div").removeClass("d-none")
                }
            }
            $('#cari_event').click(function(e){
                $("#loading-cari").removeClass("d-none")
                $("#fixtime").addClass("d-none")
                $("#flexibletime").addClass("d-none")
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                resultlimit = 0;
                loadData(page)
            });
            $('#fixtime').on('click', function(){
                $('#result_search').empty()
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        var content = "";
                        var data_content = [];
                        var i = 1;
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';
                            if(value.opendateflag === 0){
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                                i ++;
                                data_content.push(value.opendateflag)
                            }
                        });
                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }
                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                });
            });
            $('#flexibletime').on('click', function(){
                $('#result_search').empty()
                $("#hasil_pencarian").addClass("d-none")
                $("#result_div").addClass("d-none")
                $('#result_search').empty()
                var url = "";
                if(category != "0"){
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&category[0]="+category+"&search_date="+start_date+"&finish_date="+end_date;
                } else {
                    url = "https://tukbase3.tukmu.com/api/event/search?page="+page+"&search_param="+search_params+"&search_date="+start_date+"&finish_date="+end_date;
                }
                var settings = {
                    "async": true,
                    "crossDomain": true,
                    "url": url,
                    "method": "GET"
                }
                $.ajax(settings).done(function (response) {
                    if(response.status === "1"){
                        $("#hasil_pencarian").removeClass("d-none")
                        $("#result_div").removeClass("d-none")
                        var content = "";
                        var data_content = [];
                        response.search_value.forEach((value, index) => {
                            var img_logo = '';
                            var pemisah = '';
                            if(value.opendateflag === 1){
                                if(value.first_media != null && value.first_media !== null){
                                    img_logo += '<img src="https://tukbase3.tukmu.com/picGet/'+value.first_media.spacemediapath+'" width="170" height="170" style="margin-top: 30px;">';
                                } else {
                                    img_logo += '<img src="assets/master/img/search-engine/sample.png" width="170" height="170" style="margin-top: 30px;">';
                                }
                                content +=
                                    '<div class="row list-content">'+
                                        '<div class="col-md-3">'+
                                            img_logo+
                                        '</div>'+
                                        '<div class="col-md-5">'+
                                            '<p style="color: red;"><i>Highlight</i></p>'+
                                            '<h5>INDEPENDENCE DAY</h5>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; '+value.startdate+' - '+value.finishdate+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; '+value.spacename+
                                            '</p>'+
                                            '<p>'+
                                                '<img src="assets/master/img/search-engine/rp.png" width="20" height="20">&nbsp; Rp. '+value.minprice+' - Rp. '+value.maxprice+
                                            '</p>'+
                                            '<span>PENGELOLA / EVENT ORGANIZER</span>'+
                                            '<br>'+
                                            '<span><a href="#" style="color: #ff9649">'+value.space_provider.spaceprovname+'</a></span>'+
                                        '</div>'+
                                        '<div class="col-md-4">'+
                                            '<a href="'+base_url+'/detail-event/'+value.spaceid+'" class="btn btn-defaullt btn-sm" style="background-color: #ff9649"><img src="assets/master/img/home/cari.png" width="20" height="20"> <span style="color: white">DETAIL EVENT</span></a>'+
                                        '</div>'+
                                    '</div>'+
                                    '<br/>';
                                data_content.push(value.opendateflag)
                            }
                        });
                        if(data_content.length !== 0){
                            $('#result_search').append(content)
                        } else {
                            $('#result_search').append('<p style="text-align: center; color: red">Event tidak ditemukan!<p>')
                        }
                    } else {
                        console.log("Gagal")
                        $("#loading-cari").addClass("d-none")
                        $("#hasil_pencarian").addClass("d-none")
                        $("#result_div").addClass("d-none")
                    }
                });
            });
        });
    </script>
@endpush
