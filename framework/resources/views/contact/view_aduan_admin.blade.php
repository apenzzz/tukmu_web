@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divViewAduanAdmin" data-username="{{ Session::get('name__')}}" data-accesstoken="{{session('my_token___')}}" data-caseid="{{$caseid}}"></div>
@stop
@section('script')
    <script src="{{url('/framework/public/js')}}/contact/view-aduan-admin.js"></script>
@stop
