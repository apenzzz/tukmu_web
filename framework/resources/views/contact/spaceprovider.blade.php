@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    <div id="divContactSpaceProvider" data-username="{{ Session::get('name__')}}" data-accesstoken="{{session('my_token___')}}" data-param="{{ $param }}"></div>
@stop
@section('script')
    <script src="{{url('/framework/public/js')}}/contact/spaceprovider.js"></script>
@stop
