@extends('layouts.newmaster')
@section('content')
    @include('common.header')
    @if(Session::has('my_token___'))
    <div id="divHelpLogin" data-username="{{ Session::get('name__')}}" data-accesstoken="{{session('my_token___')}}" data-param="{{ $param }}"></div>
    @else
    <div id="divHelpNoLogin" data-param="{{ $param }}"></div>
    @endif
@stop
@section('script')
    @if(Session::has('my_token___'))
    <script src="{{url('/framework/public/js')}}/contact/help-login.js"></script>
    @else
    <script src="{{url('/framework/public/js')}}/contact/help-nologin.js"></script>
    @endif
@stop
