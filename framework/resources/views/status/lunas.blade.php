@inject('pointing', 'App\Helpers\Datastatik')
@extends('layouts.master')
@section('st_css')

	@include('common.header')

	<style type="text/css">
		.p-r-li {
			padding-left: 63px;
			padding-right: 63px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.statushead {
			background-color: #bababa;
			padding: 8px 80px 8px 50px;
			border-radius: 3px;
		}
		
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin-top: -4%;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            margin-right: 5px;
        }
	</style>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 mt-5">
				<div class="row">
                    <div class="col-sm-4">
                        <a href="{{ url('pending-order') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">NEGO</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('tagihan') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">TAGIHAN</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('lunas') }}" style="text-decoration: none;">
                            <div class="bg-orange p-3 text-center c-white">LUNAS</div>
                        </a>
                    </div>
                </div>
				<div class="tab-content" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
				  	<dir class="jumbotron jumbotron-fluid" style="background-color: green; color: white;">
				  		<div class="container" style="text-align: center;">
					  			<h5>PEMBAYARAN TELAH KAMI TERIMA</h5>
					  			<p style="font-size: 13px;"><b>Silahkan download surat loading anda.</b></p>
				  		</div>
				  	</dir>
						@if(isset($lunas->pending_booking_list))
				  	@foreach($lunas->pending_booking_list as $key => $value)
				  	<div class="jumbotron jumbotron-fluid">
						  <div class="container">
						    <div class="row">
						    	<div class="col-md-4">
                                    @if(isset($value->space_detail[0]->first_media))
						    		    <img src="https://tukbase3.tukmu.com/picGet/{{ $value->space_detail[0]->first_media->spacemediapath }}" width="230px" height="230px" style="margin-top: 15px;">
                                    @endif
						    	</div>
							    <div class="col-md-5">
							    	<h5>{{ $value->space_detail[0]->eventname }}</h5>
							    	<div class="row">
							    		<p class="f-p-s" style="margin-left: 15px;"><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; {{ date("d-m-Y", strtotime($value->startbookdate)) }} - {{ date("d-m-Y", strtotime($value->finishbookdate)) }}</p>
							    		<p class="f-p-s" style="margin-left: 20px;"><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->spacename }}</p>
							    	</div>
							    	<p class="f-s-13">STAN</p>
        			    		    <ul class="lokasi-list">
            			    			<?php
            				    			$length = count($value->payment_grid_with_code);
            								for ($i = 0; $i < $length; $i++) {
            									echo "<li>";
            									if($length == $i){
            								  	    echo $value->payment_grid_with_code[$i]->code;
            									} else {
            								  	    echo $value->payment_grid_with_code[$i]->code.",";
            									}
            								  	echo "</li>";
            								} 
            							?>
            			    		</ul>
							    	<hr style="margin-top: -1em;">

							    	<span class="f-s-13">BRAND</span>
							    	<p>{{ $value->customer_brand->brandname }}</p>
							    	<hr style="margin-top: -1em;">

							    	<span class="f-s-13">PENGELOLA / EVENT ORGANIZER</span>
							    	<p>{{ $value->space_detail[0]->space_provider->spaceprovname }}</p>
							    	<hr style="margin-top: -1em;">
							    </div>
							    <div class="col-md-3">
							    	<br>
							    	<br>
							    	<br>
							    	<br>
							    	<br>
							    	<br>
							    	<br>
							    	<a href="/tambahan/1/null/{{ $value->custpaymentid }}" style="font-size: 14px; color: black;" target="_blank">
                                        <img src="assets/master/img/tambah.png" width="15px" height="15px">&nbsp; TAMBAHAN</a>
							    	<br>
							    	<a href="{{ url('transaksi/loading/create/' . $value->custpaymentid) }}" style="font-size: 13px; color: black;">
                                        <img src="assets/master/img/pensil.png" width="15px" height="15px">&nbsp; BUAT SURAT LOADING</a>
							    	<br>
                                    <?php
                                        if(isset($value->latest_customer_payment_loading_letter)) {
                                            if(isset($value->latest_customer_payment_loading_letter->loadingletterpath)) {
                                                echo '<a href="' . $pointing->pointing() . 'fileGet/' . $value->latest_customer_payment_loading_letter->loadingletterpath .
                                                '" target="_blank" style="font-size: 12px; color: orange;"><img src="assets/master/img/print.png" width="18px" height="18px">&nbsp; LIHAT SURAT LOADING</a>';
                                            }
                                        }
                                    ?>
							    </div>
						    </div>
						  </div>
						</div>
						@endforeach
						@endif
				  </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

	<script type="text/javascript">
		$('#dropdownMenu a').on('click', function(){
	    $('#pills-tab li a.active').removeClass('active');
	    $(this).addClass('active');
});
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@stop
