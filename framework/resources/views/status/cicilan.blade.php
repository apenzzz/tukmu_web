@extends('layouts.master')
@section('st_css')

	@include('common.header')

	<style type="text/css">
		.p-r-li {
			padding-left: 63px;
			padding-right: 63px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.statushead {
			background-color: #bababa;
			padding: 8px 80px 8px 50px;
			border-radius: 3px;
		}
	</style>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8" style="position: relative; margin: auto; display: block;">
				<ul class="nav nav-pills mb-3" style="margin-top: 80px;" id="pills-tab" role="tablist">
					<div class="row">
					  	<div class="col-md-3">
					  		<a href="{{ url('pending-order') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">NEGO</p>
						  		</div>
					  		</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="#" style="text-decoration: none;">
						  		<div style="background-color: orange; padding: 8px 80px 8px 50px; border-radius: 3px">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">CICILAN</p>
						  		</div>
						  	</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="{{ url('tagihan') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">TAGIHAN</p>
						  		</div>
						  	</a>
					  	</div>
					  	<div class="col-md-3">
					  		<a href="{{ url('lunas') }}" style="text-decoration: none;">
						  		<div class="statushead">
						  			<p style="color: white;text-align: center;display: table;margin: auto;">LUNAS</p>
						  		</div>
						  	</a>
					  	</div>
					</div>
				</ul>
				<div class="tab-content" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-cicilan" role="tabpanel" aria-labelledby="pills-cicilan-tab">
				  	<div class="jumbotron jumbotron-fluid">
					  <div class="container">
					  	<div class="row">
					  		<div class="col-md-8">
					  			<h2>CICILAN <b>1</b></h2>
					  		</div>
					  		<div class="col-md-4" style="margin-top: 10px; color: orange;">
					  			<p>Maret 15, 2018</p>
					  		</div>
					  	</div>
					  	<hr style="background-color: orange; margin-top: 0em;">
					    <table>
					    	<tbody>
					    		<tr>
					    			<td>Jumlah Cicilan</td>
					    			<td>: 25.000.000</td>
					    		</tr>
					    		<tr>
					    			<td>Status</td>
					    			<td>: Approved</td>
					    		</tr>
					    		<tr>
					    			<td colspan="2">Photo : image.img</td>
					    			<td>&nbsp; &nbsp;</td>
					    			<td>Catatan</td>
					    		</tr>
					    		<tr>
					    			<td colspan="2">
					    				<img src="{{ asset('assets/master/img/sample.png') }}" width="200px" height="200px">
					    				<a href="">Click to enlarge</a>
					    			</td>
					    			<td>&nbsp; &nbsp;</td>
					    			<td>
					    				<textarea rows="8" cols="67" style="resize: none; margin-top: -1em;" readonly></textarea>
					    			</td>
					    		</tr>
					    	</tbody>
					    </table>
					  </div>
					</div>
				  </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>

	<script type="text/javascript">
		$('#dropdownMenu a').on('click', function(){
	    $('#pills-tab li a.active').removeClass('active');
	    $(this).addClass('active');
});
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@stop
