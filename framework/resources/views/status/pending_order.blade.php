@inject('pointing', 'App\Helpers\Datastatik')
@extends('layouts.master')
@section('st_css')

	@include('common.header')

	<style type="text/css">
		.p-r-li {
			padding-left: 80px;
			padding-right: 80px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.statushead {
			background-color: #bababa;
			padding: 8px 80px 8px 50px;
			border-radius: 3px;
		}
	</style>
@stop
@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 mt-5">
				<div class="row">
                    <div class="col-sm-4">
                        <a href="{{ url('pending-order') }}" style="text-decoration: none;">
                            <div class="bg-orange p-3 text-center c-white">NEGO</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('tagihan') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">TAGIHAN</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('lunas') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">LUNAS</div>
                        </a>
                    </div>
                </div>
				<div class="mt-3 tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
				  	<div class="jumbotron-fluid" style="background-color: darkblue; color: white;">
				  		<div class="container" style="text-align: center;">
					  		<h5>
					  			<img src="assets/master/img/tandaseru2.png" width="20" height="20">&nbsp;&nbsp; PESANAN ANDA TELAH KAMI TERIMA &nbsp;&nbsp;<img src="assets/master/img/tandaseru2.png" width="20" height="20">
					  		</h5>
					  		<p style="font-size: 13px;"><b>Kami akan memberikan notifikasi jika pihak penyelenggara sudah memberikan jawaban.</b></p>
				  		</div>
					</div>
					<div id="pending"></div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
@stop


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script crossorigin src="https://unpkg.com/react@16/umd/react.production.min.js"></script>
<script crossorigin src="https://unpkg.com/react-dom@16/umd/react-dom.production.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.js"></script>

<script id="pending_room" type="text/babel" src="{{url('/')}}/framework/public/js/pending.js?endpoint={{ $pointing->pointing() }}&acces_token={{session('my_token___')}}&url={{url('/')}}&session_chat={{session('chat_session__')}}"></script>
