@extends('layouts.newmaster')

@section('style')
	<style type="text/css">
		.p-r-li {
			padding-left: 63px;
			padding-right: 63px;
			background-color: #bababa;
			color: white;
		}

		.jumbotron {
			padding: 0;
		}

		.f-p-s {
			font-size: 10px;
		}

		.f-s-13 {
			font-size: 13px;
		}

		.statushead {
			background-color: #bababa;
			padding: 8px 80px 8px 50px;
			border-radius: 3px;
		}
		
        .lokasi-list {
            display: flex;
            padding: 0px;
            margin-top: -4%;
        }
        .lokasi-list li {
            list-style: none;
            text-decoration: none;
            margin-right: 5px;
        }
	</style>
@stop

@section('content')
    @include('common.header')
	<div class="container">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8 mt-5">
                <div class="row">
                    <div class="col-sm-4">
                        <a href="{{ url('pending-order') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">NEGO</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('tagihan') }}" style="text-decoration: none;">
                            <div class="bg-orange p-3 text-center c-white">TAGIHAN</div>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a href="{{ url('lunas') }}" style="text-decoration: none;">
                            <div class="bg-gray p-3 text-center c-white">LUNAS</div>
                        </a>
                    </div>
                </div>
				<div class="tab-content mt-3" id="pills-tabContent">
				  <div class="tab-pane fade show active" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
				  	<div class="jumbotron jumbotron-fluid" style="
				  	background-color: red; color: white;">
				  		<div class="container" style="text-align: center;">
					  			<h5>
					  				<img src="assets/master/img/tandaseru2.png" width="20" height="20">&nbsp;&nbsp;
					  				LAKUKAN PEMBAYARAN 
					  				&nbsp;&nbsp;<img src="assets/master/img/tandaseru2.png" width="20" height="20">
					  			</h5>
					  			<p style="font-size: 13px;"><b>Segera lakukan pembayaran untuk melanjutkan ke tahap selanjutnya.</b></p>
				  		</div>
				  	</div>
                    @if(isset($tagihan->pending_booking_list))
				  	    @foreach($tagihan->pending_booking_list as $key => $value)
				  	        <div class="jumbotron jumbotron-fluid">
					          <div class="container">
					            <div class="row p-3">
					    	        <div class="col-md-4">
                                        @if(isset($value->space_detail[0]->first_media))
					    		            <img class="img-responsive w-100" src="https://tukbase3.tukmu.com/picGet/{{ (isset($value->space_detail[0]->first_media) ? $value->space_detail[0]->first_media->spacemediapath : null) }}">
                                        @endif
					    	        </div>
						            <div class="col-md-6">
						    	        <h5>{{ $value->space_detail[0]->eventname }}</h5>
						    	        <div class="row">
						    		        <p class="f-p-s" style="margin-left: 15px;"><img src="assets/master/img/search-engine/date.png" width="20" height="20">&nbsp; {{ date("d-m-Y", strtotime($value->startbookdate)) }} - {{ date("d-m-Y", strtotime($value->finishbookdate)) }}</p>
						    		        <p class="f-p-s" style="margin-left: 20px;"><img src="assets/master/img/search-engine/location.png" width="20" height="20">&nbsp; {{ $value->space_detail[0]->spacename }}</p>
						    	        </div>
						    	        <p class="f-s-13">STAN</p>
        			    		            <ul class="lokasi-list">
        			    			        <?php
        				    			        $length = count($value->payment_grid_with_code);
        								        for ($i = 0; $i < $length; $i++) {
        									        echo "<li>";
        									        if($length == $i){
        								  	            echo $value->payment_grid_with_code[$i]->code;
        									        } else {
        								  	            echo $value->payment_grid_with_code[$i]->code.",";
        									        }
        								  	        echo "</li>";
        								        } 
        							        ?>
        			    		        </ul>
						    	        <hr style="margin-top: -1em;">

						    	        <span class="f-s-13">BRAND</span>
						    	        <p>{{ $value->customer_brand->brandname }}</p>
						    	        <hr style="margin-top: -1em;">

						    	        <span class="f-s-13">PENGELOLA / EVENT ORGANIZER</span>
						    	        <p>{{ $value->space_detail[0]->space_provider->spaceprovname }}</p>
						    	        <hr style="margin-top: -1em;">
						            </div>
						            <div class="col-md-2">
                                        <div class="py-1 px-2 text-center border border-danger rounded-sm text-danger">
                                            <?php
                                                if($value->customer_term_payment == null && $value->customer_flexible_payment == null) {
                                                    echo "DI MUKA";
                                                }
                                                else if($value->customer_term_payment != null) {
                                                    echo "BERKALA";
                                                }
                                                else if($value->customer_flexible_payment != null) {
                                                    echo "FLEKSIBEL";
                                                }
                                            ?>
                                        </div>
                				        <br>
                				        <br>
                				        <br>
                                        @if($value->customer_term_payment != null || $value->customer_flexible_payment != null)
                                        <a href="{{ url('payment/history/'.$value->custpaymentid) }}" style="font-size: 12px; color: black;"><img src="{!! asset('assets/master/img/search-engine/date.png') !!}" width="18px" height="18px">&nbsp; HISTORY</a>
                                        @endif
                                        <a href="{{ url('payment/print/'.$value->custpaymentid) }}" style="font-size: 12px; color: black;"><img src="{!! asset('assets/master/img/print.png') !!}" width="18px" height="18px">&nbsp; TAGIHAN</a>
                				        <a href="{{ url('booking/'.$value->custpaymentid.'/'.$value->custbrandid) }}" style="font-size: 12px; color: black;"><img src="{!! asset('assets/master/img/chat.png') !!}" width="18px" height="18px">&nbsp; CHAT</a>
						    	        <a href="{{ url('metode-pembayaran/'.$value->custpaymentid) }}" class="btn btn-defaut" style="background-color: red; color: white; margin-top: 10px; margin-left: 10px;">BAYAR</a>
						            </div>
					            </div>
					          </div>
					        </div>
					    @endforeach
                    @endif
				  </div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
@stop

@section('script')
	<script type="text/javascript">
		$('#dropdownMenu a').on('click', function(){
	    $('#pills-tab li a.active').removeClass('active');
	    $(this).addClass('active');
});
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@stop
