import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class ContactSPList extends React.Component {
    constructor(props) {
        super(props);
        this.handleDetailButton = this.handleDetailButton.bind(this);
    }

    handleDetailButton(custcaseid) {
        window.location.href = "/contact/spaceprovider/view/" + custcaseid;
    }

    render() {
        const listItems = this.props.items.map((value, i) => {
            return (
                <div className="row mb-3" key={i}>
                    <div className="col-sm-12 bg-white p-3">
                        <div className="row">
                            <div className="col-sm-6">
                                <h3>{value.casesubject}</h3>
                            </div>
                            <div className="col-sm-6">
                                <h3 className="text-right">{value.casestartdatetime}</h3>
                            </div>
                        </div>
                        <hr className="bg-orange" />
                        <button className="btn bg-orange c-white" onClick={() => this.handleDetailButton(value.custcaseid)}>Detail</button>
                    </div>
                </div>
            );
        });

        return (
            <div className="container">
                <div className="row">
                    <div className="offset-sm-2 col-sm-8">
                        <div className="container my-5">
                            <div className="row">
                                <div className="col-sm-12 bg-orange p-3">
                                    <h2 className="text-center c-white">{this.props.title}</h2>
                                </div>
                            </div>
                            <div style={{ maxHeight: '600px', overflowX: 'none', overflowY: 'auto', paddingLeft: '15px', paddingRight: '15px', marginLeft: '-15px', marginRight: '-15px' }}>
                                {listItems}
                            </div>
                            <div className="row">
                                <div className="col-sm-12 bg-orange p-3">
                                    <h2 className="text-center c-white">&nbsp;</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('divContactSPList')) {
    const element = document.getElementById('divContactSPList');
    let items = [];
    const title = "LIST ADUAN";
    if (element.dataset.accesstoken !== undefined) {
        axios.get(Config.APIUrl + "/customer/view/case/SP/all", { 'headers': { 'Authorization': 'Bearer ' + element.dataset.accesstoken } })
            .then(result => {
                items = result.data.case_list;
                ReactDOM.render(<ContactSPList title={title} items={items} />, element);
            })
            .catch(e => {
                console.log(e);
            });
    }
}
