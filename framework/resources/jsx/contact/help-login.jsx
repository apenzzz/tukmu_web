import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class HelpLogin extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currenttopicid: null,
            topics: [],
            faq: null,
            faq_title: null,
            mode: 'card' /* card or faq or contact */,
            subject: '',
            description: '',
            submitresult: '',
            issubmitting: false
        };
        this.handleClick = this.handleCardClick.bind(this);
        this.handleFAQNoClick = this.handleFAQNoClick.bind(this);
        this.handleSubjectText = this.handleSubjectText.bind(this);
        this.handleDescriptionText = this.handleDescriptionText.bind(this);
        this.handleFormSendClick = this.handleFormSendClick.bind(this);
        this.fetchData = this.fetchData.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        var param = this.props.param;
        var splitted = param.split("/");

        var topicid = '';
        var mode = 'card';
        if (splitted[splitted.length - 1] === 'faq') {
            topicid = splitted[splitted.length - 3];
            mode = 'faq';
        }
        else if (splitted[splitted.length - 1] === 'form') {
            topicid = splitted[splitted.length - 3];
            mode = 'form';
        }
        else {
            topicid = splitted[splitted.length - 1];
        }

        let url = "/customer/view/admin/case/topic/login";

        if (topicid !== '') {
            url += "/" + topicid;
        }

        axios.get(Config.APIUrl + url, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (mode === 'faq') {
                    let filteredTopic = result.data.topic_list.filter((topic) => topic.casetopicid === parseInt(splitted[splitted.length - 2]));
                    filteredTopic.map((v, i) => {
                        this.setState({
                            currenttopicid: splitted[splitted.length - 2],
                            faq: v.case_faq.casefaqdesc,
                            faq_title: v.casetopicname,
                            mode: 'faq',
                            topics: []
                        });
                    });
                }
                else if (mode === 'form') {
                    this.setState({
                        mode: mode,
                        topics: result.data.topic_list,
                        currenttopicid: splitted[splitted.length - 2]
                    });
                }
                else {
                    this.setState({
                        mode: mode,
                        topics: result.data.topic_list
                    });
                }
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleCardClick(topicid, ischildless, isfaqexist) {
        if (ischildless === true && isfaqexist === true) {
            window.location.href = window.location.href + "/" + topicid + "/faq";
        }
        else if (ischildless === true && isfaqexist === false) {
            window.location.href = window.location.href + "/" + topicid + "/form";
        }
        else {
            this.setState({
                currenttopicid: topicid,
                topics: []
            });

            window.location.href = window.location.href + "/" + topicid;
        }
    }

    handleFAQYesClick() {
        window.history.back();
    }

    handleFAQNoClick() {
        window.location.href = window.location.href.substr(0, window.location.href.lastIndexOf("/")) + "/form";
    }

    handleFormCancelClick() {
        window.history.back();
    }

    handleSubjectText(e) {
        this.setState({ subject: e.target.value });
    }

    handleDescriptionText(e) {
        this.setState({ description: e.target.value });
    }

    handleFormSendClick(e) {
        if (this.state.subject !== '' && this.state.description !== '') {
            e.preventDefault();

            if (!this.state.issubmitting) {
                this.setState({
                    submitresult: 'Sending...',
                    issubmitting: true
                });

                let url = '/customer/create/admin/case/' + this.state.currenttopicid;
                axios.post(Config.APIUrl + url, { case_subject: this.state.subject, case_message: this.state.description },
                    { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                    .then(result => {
                        if (result.data.status === "0") {
                            this.setState({
                                subject: '',
                                description: '',
                                submitresult: 'Error : ' + result.data.error_codes[0],
                                issubmitting: false
                            });
                        }
                        else {
                            this.setState({
                                subject: '',
                                description: '',
                                submitresult: 'Success',
                                issubmitting: false
                            });
                        }
                    })
                    .catch(e => {
                        console.log(e);
                    });
            }
        }        
    }

    handleListAduanClick() {
        window.location.href = "/contact/admin/list";
    }

    render() {
        const Topics = this.state.topics.map((value, i) => {
            const ischildless = value.child_topic.length === 0 ? true : false;
            const isfaqexist = value.case_faq !== null ? true : false;
            let btnlabel = "Detail";
            if (ischildless && isfaqexist) {
                btnlabel = "FAQ";
            }
            else if (ischildless && !isfaqexist) {
                btnlabel = "Contact";
            }

            return (
                <div className="col-sm-4 mb-3" key={i}>
                    <div className="p-3 bg-white">
                        <h3 className="text-center">{value.casetopicname}</h3>
                        <hr className="bg-orange" />
                        <p className="text-center">{value.casetopicdesc}</p>
                        <div className="text-center">
                            <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleCardClick(value.casetopicid, ischildless, isfaqexist)}>{btnlabel}</button>
                        </div>
                    </div>
                </div>
            );
        });

        const Cards = (
            <section className="my-5">
                <div className="container">
                    <div className="row justify-content-md-center align-self-stretch">
                        {Topics}
                    </div>
                </div>
            </section>
        );

        const Faq = (
            <section className="my-5">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <div className="p-3 bg-white">
                                <h3>{this.state.faq_title}</h3>
                                <p>{this.state.faq}</p>
                                <hr className="bg-orange" />
                                <p>Apakah jawaban ini membantu?</p>
                                <div>
                                    <button className="bg-orange c-white px-3 py-2" onClick={this.handleFAQYesClick}>YA</button>
                                    &nbsp;&nbsp;
                                    <button className="bg-orange c-white px-3 py-2" onClick={this.handleFAQNoClick}>TIDAK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );

        const Form = (
            <section className="my-5">
                <div className="container">
                    <div className="row">
                        <div className="col-sm-12">
                            <h2>HUBUNGI KAMI</h2>
                            <h4>Beritahu kami tentang kendala anda terhadap servis kami</h4>
                            <div className="mt-5 bg-orange p-3 c-white text-center">
                                <h4>Form pengaduan</h4>
                            </div>
                            <div className="bg-white p-3" style={{ minHeight: '200px' }}>
                                <p className="text-center">AGUSTUS 30, 2017</p>
                            </div>
                            <div className="bg-gray p-3">
                                &nbsp;
                            </div>
                            <form>
                                <div className="bg-white p-3">
                                    <input className="form-control form-control-lg border-0" placeholder="SUBJECT" value={this.state.subject} onChange={this.handleSubjectText} required />
                                    <hr className="bg-orange" />
                                    <textarea className="form-control border-0" placeholder="Silahkan isi kendala anda disini" style={{ minHeight: '200px' }}value={this.state.description} onChange={this.handleDescriptionText} required />
                                </div>
                                <div className="bg-gray p-3">
                                    <button type="submit" className="bg-orange c-white px-3 py-2" onClick={this.handleFormSendClick}>KIRIM</button>
                                    &nbsp;&nbsp;
                                    <button className="bg-orange c-white px-3 py-2" onClick={this.handleFormCancelClick}>BATAL</button>
                                    &nbsp;&nbsp;
                                    <label>{this.state.submitresult}</label>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </section>
        );

        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="offset-sm-2 col-sm-8">
                            <section className="mt-5">
                                <div className="container">
                                    <h2 className="text-center">Halo, {this.props.username}</h2>
                                    <p className="text-center">Silahkan pilih informasi yang sesuai dengan kebutuhan anda</p>
                                </div>
                            </section>
                            <section className="my-5">
                                <div className="container">
                                    <div className="row justify-content-md-center align-self-stretch">
                                        <div className="col-sm-4">
                                            <div className="p-3 bg-white">
                                                <h3 className="text-center">List Aduan</h3>
                                                <hr className="bg-orange" />
                                                <p className="text-center">Anda dapat melihat semua komplain yang anda ajukan.</p>
                                                <div className="text-center">
                                                    <button className="bg-orange c-white px-3 py-2"
                                                        onClick={() => this.handleListAduanClick()}>LIST</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <div className="container">
                                <hr className="bg-orange" />
                            </div>
                            {this.state.mode === 'card' ? Cards : null}
                            {this.state.mode === 'faq' ? Faq : null}
                            {this.state.mode === 'form' ? Form : null}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

if (document.getElementById('divHelpLogin')) {
    const element = document.getElementById('divHelpLogin');
    if (element.dataset.username !== undefined && element.dataset.accesstoken !== undefined && element.dataset.param !== undefined) {
        ReactDOM.render(<HelpLogin username={element.dataset.username} accesstoken={element.dataset.accesstoken} param={element.dataset.param} />, document.getElementById('divHelpLogin'));   
    }
}
