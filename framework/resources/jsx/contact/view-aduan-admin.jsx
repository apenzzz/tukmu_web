import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class ViewAduanSP extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            case_message: [],
            description: '',
            isloading: false
        };
        this.loadCaseMessage = this.loadCaseMessage.bind(this);
        this.handleDescriptionText = this.handleDescriptionText.bind(this);
        this.handleFormSendClick = this.handleFormSendClick.bind(this);
    }

    componentDidMount() {
        this.loadCaseMessage();
    }

    loadCaseMessage() {
        this.setState({
            isloading: true
        });

        const url = '/customer/view/admin/case/spec?caseid=' + this.props.caseid;
        axios.get(Config.APIUrl + url, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                this.setState({
                    case_message: result.data.case_message
                });
            })
            .catch(e => {
                console.log(e);
            })
            .finally(() => {
                this.setState({
                    isloading: false
                });
            });
    }

    handleDescriptionText(e) {
        this.setState({ description: e.target.value });
    }

    handleFormSendClick(e) {
        if (this.state.description !== '') {
            e.preventDefault();

            let url = "/customer/add/admin/case/message";
            if (!this.state.issubmitting) {
                this.setState({
                    isloading: true
                });
                axios.post(Config.APIUrl + url, { caseid: this.props.caseid, case_message: this.state.description },
                    { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                    .then(result => {
                        this.setState({
                            description: ''
                        });

                        this.loadCaseMessage();
                    })
                    .catch(e => {
                        console.log(e);
                    })
                    .finally(() => {
                        this.setState({
                            isloading: false
                        });
                    });
            }
        }
    }

    render() {
        console.log(this.state);

        // yang dirender adalah state dari hasil data load
        const messageList = this.state.case_message.map((v, i) => {
            return (
                <React.Fragment key={i}>
                    <p className="text-center">{v.messagedate}</p>
                    <p><img src="/assets/master/img/user/user-img.png" width="20" />&nbsp;&nbsp;{v.SP_name !== null ? v.SP_name : v.tenant_name}</p>
                    <p>{v.messageval}</p>
                    <hr className="bg-orange" />
                </React.Fragment>
            );
        });


        // plus handle kalau dia mau send message

        // terus juga handle kalau dia setelah send message perlu refresh state data load

        return (
            <div className="container">
                <div className="row">
                    <div className="offset-sm-2 col-sm-8">
                        <section className="my-5">
                            <p className="position-fixed" hidden={this.state.isloading ? "" : "hidden"} style={{ top: '50%', left: '50%', marginLeft: '-25px', marginTop: '-25px', zIndex: '999', fontSize: '50px' }}><i className="fa fa-spinner fa-spin" /></p>
                            <div className="container">
                                <form>
                                    <div className="row">
                                        <div className="col-sm-12">
                                            <h2>Halo, {this.props.username}</h2>
                                            <h4>Silahkan masukkan kebutuhan anda kepada penyelenggara</h4>
                                            <div className="mt-5 bg-orange p-3 c-white text-center">
                                                <h4></h4>
                                            </div>
                                            <div className="bg-white p-3 overflow-auto" style={{ minHeight: '200px', maxHeight: '400px' }}>
                                                {messageList}
                                            </div>
                                            <div className="bg-gray p-3">
                                                &nbsp;
                                            </div>
                                            <div className="bg-white p-3">
                                                <textarea className="form-control border-0" placeholder="Silahkan isi kendala anda disini" style={{ minHeight: '100px' }} value={this.state.description} onChange={this.handleDescriptionText} required />
                                            </div>
                                            <div className="bg-gray p-3">
                                                <button type="submit" className="bg-orange c-white px-3 py-2" onClick={this.handleFormSendClick}>KIRIM</button>
                                                &nbsp;&nbsp;
                                            <button type="button" className="bg-orange c-white px-3 py-2">BATAL</button>
                                                &nbsp;&nbsp;
                                            <label></label>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        );
    }
}

const elementId = 'divViewAduanAdmin';
if (document.getElementById(elementId)) {
    const element = document.getElementById(elementId);
    if (element.dataset.username !== undefined && element.dataset.accesstoken !== undefined && element.dataset.caseid !== undefined) {
        ReactDOM.render(<ViewAduanSP username={element.dataset.username} caseid={element.dataset.caseid} accesstoken={element.dataset.accesstoken} />, element);
    }
}
