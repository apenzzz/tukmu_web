import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class ContactSpaceProvider extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currenttopicid: null,
            topics: [],
            mode: 'card', /* card, event, form */
            subject: '',
            description: '',
            submitresult: '',
            issubmitting: false,
            events: [],
            currentcpyid: null,
            currenttopic: ''
        };
        this.handleClick = this.handleCardClick.bind(this);
        this.handleSubjectText = this.handleSubjectText.bind(this);
        this.handleDescriptionText = this.handleDescriptionText.bind(this);
        this.handleFormSendClick = this.handleFormSendClick.bind(this);
        this.fetchData = this.fetchData.bind(this);
        this.handleEventClick = this.handleEventClick.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    fetchData() {
        var param = this.props.param;
        var splitted = param.split("/");

        var topicid = '';
        var cpyid = '';
        var mode = 'card';
        if (splitted[splitted.length - 1] === 'form') {
            topicid = splitted[splitted.length - 3];
            cpyid = splitted[splitted.length - 2];
        }
        else {
            topicid = splitted[splitted.length - 1];
        }

        if (topicid === '' && cpyid === '') {
            let url = "/customer/view/event/case/topic";

            if (topicid !== '') {
                url += "/" + topicid;
            }

            axios.get(Config.APIUrl + url, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                .then(result => {
                    this.setState({
                        topics: result.data.topic_list
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else if (topicid !== '' && cpyid === '') {
            let url = "/customer/view/event/payed/case/SP/" + topicid;
            axios.get(Config.APIUrl + url, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                .then(result => {
                    this.setState({
                        currenttopicid: topicid,
                        mode: 'event',
                        events: result.data.event_list
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        }
        else if (topicid !== '' && cpyid !== '') {
            this.setState({
                currenttopicid: topicid,
                currentcpyid: cpyid,
                mode: 'form'
            });
        }
    }

    handleCardClick(topicid) {
        window.location.href = window.location.href + "/" + topicid;
    }

    handleFormCancelClick() {
        window.history.back();
    }

    handleSubjectText(e) {
        this.setState({ subject: e.target.value });
    }

    handleDescriptionText(e) {
        this.setState({ description: e.target.value });
    }

    handleFormSendClick(e) {
        if (this.state.subject !== '' && this.state.description !== '') {
            e.preventDefault();

            let url = "/customer/create/event/case/SP/" + this.state.currenttopicid;
            if (!this.state.issubmitting) {
                this.setState({
                    submitresult: 'Sending...',
                    issubmitting: true
                });
                axios.post(Config.APIUrl + url, { cpyid: this.state.currentcpyid, case_subject: this.state.subject, case_message: this.state.description },
                    { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                    .then(result => {
                        if (result.data.status === "0") {
                            this.setState({
                                subject: '',
                                description: '',
                                submitresult: 'Error : ' + result.data.error_codes[0],
                                issubmitting: false
                            });
                        }
                        else {
                            this.setState({
                                subject: '',
                                description: '',
                                submitresult: 'Success',
                                issubmitting: false
                            });
                        }
                        
                    })
                    .catch(e => {
                        console.log(e);
                    });
            }
        }
    }

    handleListAduanClick() {
        window.location.href = "/contact/spaceprovider/list";
    }

    handleRefund() {
        window.location.href = "/refund/select";
    }

    handleEventClick(cpyid) {
        window.location.href = window.location.href + "/" + cpyid + "/form";
    }

    render() {
        const Topics = this.state.topics.map((value, i) => {
            return (
                <div className="col-sm-4 mb-3" key={i}>
                    <div className="p-3 bg-white">
                        <h3 className="text-center">{value.casetopicname}</h3>
                        <hr className="bg-orange" />
                        <p className="text-center">{value.casetopicdesc}</p>
                        <div className="text-center">
                            <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleCardClick(value.casetopicid)}>Contact</button>
                        </div>
                    </div>
                </div>
            );
        });

        const Cards = (
            <React.Fragment>
                <section className="mt-5">
                    <div className="container">
                        <h2 className="text-center">Halo, {this.props.username}</h2>
                        <p className="text-center">Silahkan pilih informasi yang sesuai dengan kebutuhan anda</p>
                    </div>
                </section>
                <section className="my-5">
                    <div className="container">
                        <div className="row justify-content-md-center align-self-stretch">
                            <div className="col-sm-4">
                                <div className="p-3 bg-white">
                                    <h3 className="text-center">List Aduan</h3>
                                    <hr className="bg-orange" />
                                    <p className="text-center">Anda dapat melihat semua komplain yang anda ajukan.</p>
                                    <div className="text-center">
                                        <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleListAduanClick()}>LIST</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="p-3 bg-white">
                                    <h3 className="text-center">Ajukan Refund</h3>
                                    <hr className="bg-orange" />
                                    <p className="text-center">Anda dapat mengajukan refund.</p>
                                    <div className="text-center">
                                        <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleRefund()}>AJUKAN</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div className="container">
                    <hr className="bg-orange" />
                </div>
                <section className="my-5">
                    <div className="container">
                        <div className="row justify-content-md-center align-self-stretch">
                            {Topics}
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );

        const EventItems = this.state.events.map((value, i) => {
            const Stans = value.payment_grid.map((v, j) => {
                if (value.payment_grid.length - 1 === j) {
                    return v.code;
                }
                else {
                    return v.code + ',';
                }
            });

            return (
                <div className="row" key={i}>
                    <div className="col-sm-12 p-3 my-3 bg-white">
                        <div className="row">
                            <div className="col-sm-3">
                                <img className="img-responsive w-100" src="/assets/master/images/beatles.png" />
                            </div>
                            <div className="col-sm-9">
                                <h3>THE BEATLES</h3>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <div className="row">
                                            <div className="col-sm-6 mb-1">
                                                <img src="/assets/master/img/search-engine/date.png" style={{ width: '20px' }} />&nbsp;&nbsp;17-20 AGUSTUS 2017
                                            </div>
                                            <div className="col-sm-6 mb-1">
                                                <img src="/assets/master/img/search-engine/location.png" style={{ width: '20px' }} />&nbsp;&nbsp;{value.spacename}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <span className="c-gray">STAN</span><br />
                                <span><strong>{Stans}</strong></span><br />
                                <div className="col-sm-6 p-0">
                                    <hr className="bg-gray m-0" />
                                </div>
                                <br />
                                <span className="c-gray">BRAND</span><br />
                                <span><strong>{value.customer_brand != null ? value.customer_brand.brandname : ''}</strong></span><br />
                                <div className="col-sm-6 p-0">
                                    <hr className="bg-gray m-0" />
                                </div>
                                <br />
                                <span className="c-gray">PENGELOLA / EVENT ORGANIZER</span><br />
                                <span><strong>{value.SP_name}</strong></span><br />
                                <br />
                            </div>
                        </div>
                        <hr className="bg-gray" />
                        <div className="row">
                            <div className="col-sm-12 text-center">
                                <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleEventClick(value.custpaymentid)}>HUBUNGI</button>
                            </div>
                        </div>
                    </div>
                </div>
            )
        });

        const Events = (
            <React.Fragment>
                <section className="mt-5">
                    <div className="container">
                        <h2 className="text-center">Halo, {this.props.username}</h2>
                        <p className="text-center">Silahkan pilih penyelenggara yang perlu dihubungi</p>
                    </div>
                </section>
                <section className="my-5">
                    <div className="container">
                        {EventItems}
                    </div>
                </section>
            </React.Fragment>
        );

        const Form = (
            <section className="my-5">
                <div className="container">
                    <form>
                        <div className="row">
                            <div className="col-sm-12">
                                <h2>Halo, {this.props.username}</h2>
                                <h4>Silahkan masukkan kebutuhan anda kepada penyelenggara</h4>
                                <div className="mt-5 bg-orange p-3 c-white text-center">
                                    <h4>{this.state.currenttopic}</h4>
                                </div>
                                <div className="bg-white p-3" style={{ minHeight: '200px' }}>
                                    <p className="text-center">AGUSTUS 30, 2017</p>
                                </div>
                                <div className="bg-gray p-3">
                                    &nbsp;
                            </div>
                                <div className="bg-white p-3">
                                    <input className="form-control form-control-lg border-0" placeholder="SUBJECT" value={this.state.subject} onChange={this.handleSubjectText}
                                        required />
                                    <hr className="bg-orange" />
                                    <textarea className="form-control border-0" placeholder="Silahkan isi kendala anda disini" style={{ minHeight: '200px' }} value={this.state.description} onChange={this.handleDescriptionText} required />
                                </div>
                                <div className="bg-gray p-3">
                                    <button type="submit" className="bg-orange c-white px-3 py-2" onClick={this.handleFormSendClick}>KIRIM</button>
                                    &nbsp;&nbsp;
                                <button type="button" className="bg-orange c-white px-3 py-2" onClick={this.handleFormCancelClick}>BATAL</button>
                                    &nbsp;&nbsp;
                                <label>{this.state.submitresult}</label>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </section>
        );

        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="offset-sm-2 col-sm-8">
                            {this.state.mode === 'card' ? Cards : null}
                            {this.state.mode === 'event' ? Events : null}
                            {this.state.mode === 'form' ? Form : null}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

if (document.getElementById('divContactSpaceProvider')) {
    const element = document.getElementById('divContactSpaceProvider');
    if (element.dataset.username !== undefined && element.dataset.accesstoken !== undefined && element.dataset.param !== undefined) {
        ReactDOM.render(<ContactSpaceProvider username={element.dataset.username} accesstoken={element.dataset.accesstoken} param={element.dataset.param} />, document.getElementById('divContactSpaceProvider'));
    }
}
