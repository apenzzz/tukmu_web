var moment = require('moment');

var scripts = document.getElementById('pending_room').getAttribute("src");
var queryString = scripts.replace(/^[^\?]+\??/, '');
var res = queryString.split("=");
var res_param = queryString.split("&");
var dataurl = res[1].split("=")[0].split("&")[0];
var access_token = res_param[1].split("=")[1];
var url = res_param[2].split("=")[1];
var chat_session = res_param[3].split("=")[1];

const headers_ = {
    "Authorization": "Bearer " + access_token
};

class Pending extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pending: [],
            loading_pending: false
        };
    }

    componentDidMount() {
        this.getDataPending();
    }

    getDataPending() {
        this.setState({
            loading_pending: true
        });

        axios.get(dataurl + 'api/customer/view/order/booking/pending/all', { headers: headers_ })
            .then(data => {
                this.setState({
                    pending: data.data.pending_booking_list,
                    loading_pending: false
                });
            })
            .catch(e => {
                this.setState({
                    loading_pending: false
                });
                if (e === "Error: Request failed with status code 401") {
                    window.location.replace(url + "/logout");
                }
            });
    }

    render() {
        const dataPending = this.state.pending.map((value, i) => {
            let actionButton = null;
            if (value.customer_brand !== null && !(value.paymentstatus === 2 || value.paymentstatus === 10 || value.paymentstatus === 51)) {
                actionButton = <a href={url + '/booking/' + value.custpaymentid + '/' + value.customer_brand.custbrandid} className='btn btn-defaut' style={{ backgroundColor: 'orange', color: 'white', position: 'absolute', bottom: '20px', right: '20px' }}>NEGO</a>;
            }
            else if (value.customer_brand !== null && (value.paymentstatus === 2  || value.paymentstatus === 10 || value.paymentstatus === 51) && value.customer_payment_add_service.length === 0) {
                actionButton = <a href={url + '/tambahan/1/null/' + value.custpaymentid} className='btn btn-defaut' style={{ backgroundColor: 'orange', color: 'white', position: 'absolute', bottom: '20px', right: '20px' }}>PILIH ADDON</a>;
            }
            else if (value.customer_brand === null) {
                actionButton = <a href={url + '/select-brand/' + value.custpaymentid + '/' + value.space_detail[0].spaceid} className='btn btn-defaut' style={{ backgroundColor: 'orange', color: 'white', position: 'absolute', bottom: '20px', right: '20px' }}>PILIH BRAND</a>;
            }


            return (
                <div key={i} className="jumbotron jumbotron-fluid">
                    {
                        <div className="container">
                            <div className="row">
                                <div className="col-md-4">
                                    <img src={value.space_detail[0].first_media !== null ? "https://tukbase3.tukmu.com/picGet/" + value.space_detail[0].first_media.spacemediapath : null} width="230px" height="230px" style={{ marginTop: '15px' }} />
                                </div>
                                <div className="col-md-5">
                                    <br />
                                    <h5>{value.space_detail[0].eventname}</h5>
                                    <div className="row">
                                        <p className="f-p-s" style={{ marginLeft: '15px' }}>
                                            <img src="assets/master/img/search-engine/date.png" width="20" height="20" />&nbsp; {moment(value.startbookdate).format("DD-MM-YYYY")} - {moment(value.finishbookdate).format("DD-MM-YYYY")}
                                        </p>
                                        <p className="f-p-s" style={{ marginLeft: '20px' }}>
                                            <img src="assets/master/img/search-engine/location.png" width="20" height="20" />&nbsp; {value.space_detail[0].spacename}
                                        </p>
                                    </div>
                                    <span className="f-s-13">STAN</span>
                                    <p>
                                        {value.payment_grid_with_code.map((value, index) => {
                                            return <span> {value.code},</span>;
                                        })}
                                    </p>
                                    <hr style={{ marginTop: '-1em' }} />

                                    {
                                        value.customer_brand !== null ?
                                            <React.Fragment>
                                                <span className="f-s-13">BRAND</span>
                                                <p>{value.customer_brand.brandname}</p>
                                                <hr style={{ marginTop: '-1em' }} />
                                            </React.Fragment>
                                            : null
                                    }
                                    <span className="f-s-13">PENGELOLA / EVENT ORGANIZER</span>
                                    <p>{value.space_detail[0].space_provider.spaceprovname}</p>
                                    <hr style={{ marginTop: '-1em' }} />
                                </div>
                                <div className="offset-md-1 col-md-2" style={{ position: 'relative' }}>
                                    <div id="notif_chat" style={{ position: 'relative', bottom: '0px' }}>
                                        <i className="fas fa-comment-dots" style={{ fontSize: '30px', color: 'black', position: 'absolute', top: '150px', right: '30px' }} />
                                        <div className="count_info" style={{ border: '1px solid #000', padding: '5px 10px', borderRadius: '50%', display: 'inline', position: 'absolute', top: '120px', right: '15px' }}>
                                            {value.nego_chat_unread_count}
                                        </div>
                                    </div>
                                    {actionButton}
                                </div>
                            </div>
                            <hr style={{ backgroundColor: 'orange' }} />
                        </div>
                    }
                </div>
            );
        });

        return (
            <React.Fragment>
                {
                    this.state.loading_pending ?
                        <div style={{ width: "300px", margin: "30px auto" }}>
                            <p style={{ textAlign: 'center' }}>
                                <i className="fas fa-spinner fa-spin" style={{ fontSize: '40px', textAlign: 'center' }} />
                            </p>
                        </div>
                        :
                        dataPending
                }
            </React.Fragment>
        );
    }
}

ReactDOM.render(
    <Pending />,
    document.getElementById('pending')
);
