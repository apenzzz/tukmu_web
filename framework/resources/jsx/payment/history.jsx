import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';
import { ConvertToRupiah } from '../common/helper';
import EventCard from '../event/eventcard';

class PaymentHistory extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            termPaymentChoose: [],
            termPaymentCompleted: this.props.item.customer_term_payment !== null ? this.props.item.customer_term_payment.customer_term_payment_data.length > 0 ? this.props.item.customer_term_payment.customer_term_payment_data.reduce((accumulator, currentValue) => {
                if (currentValue.custtermpayeddataid !== null) return accumulator + 1;
                else return accumulator;
            }, 0) : 0 : 0,
            paymentMethod: 1,
            issubmitting: false,
            selectedTpydid: null,
            namabank: '',
            namaakun: '',
            filebuktibayar: null,
            jumlahbayarflexible: 0,
            flexiblepaymentmethod: 1,
            selectedFpydid: null,

            // payment method list untuk term payment
            paymentMethodListTerm: [],

            // payment method list untuk flexible payment
            paymentMethodListFlexible: [],

            // ini untuk bantu pembayaran CC
            totalAmountSelected: 0
        };
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.handleBayarButton = this.handleBayarButton.bind(this);
        this.handleHapusPembayaranButton = this.handleHapusPembayaranButton.bind(this);
        this.handleAttachBuktiButton = this.handleAttachBuktiButton.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFileInputChange = this.handleFileInputChange.bind(this);
        this.handleSendBuktiButton = this.handleSendBuktiButton.bind(this);
        this.handleFlexibleBayarButton = this.handleFlexibleBayarButton.bind(this);
        this.handleAttachBuktiFlexibleButton = this.handleAttachBuktiFlexibleButton.bind(this);
        this.handleHapusPembayaranFlexibleButton = this.handleHapusPembayaranFlexibleButton.bind(this);
        this.handleSendBuktiFlexibleButton = this.handleSendBuktiFlexibleButton.bind(this);
        this.handlePaymentMethodTermOption = this.handlePaymentMethodTermOption.bind(this);
        this.handlePaymentMethodFlexibleOption = this.handlePaymentMethodFlexibleOption.bind(this);
        this.redirectToEspay = this.redirectToEspay.bind(this);
    }

    componentDidMount() {
        // load state pilihan combobox term dan flexible
        axios.get(Config.APIUrl + "/customer/payment/method/term", { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                this.setState({
                    paymentMethodListTerm: result.data
                });
            })
            .catch(e => {
                console.log(e);
            });

        axios.get(Config.APIUrl + "/customer/payment/method/flexible", { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                this.setState({
                    paymentMethodListFlexible: result.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleCheckboxChange(termpaymentid, amount, event) {
        let totalAmountSel = 0;

        if (event.target.checked) {
            this.setState({
                termPaymentChoose: [...this.state.termPaymentChoose, termpaymentid]
            });

            let selectedIds = [...this.state.termPaymentChoose, termpaymentid];
            for (var i = 0; i < selectedIds.length; i++) {
                for (var j = 0; j < this.props.item.customer_term_payment.customer_term_payment_data.length; j++) {
                    if (selectedIds[i] === this.props.item.customer_term_payment.customer_term_payment_data[j].custtermpaymentdataid) {
                        totalAmountSel += parseInt(this.props.item.customer_term_payment.customer_term_payment_data[j].termpaymentval);
                    }
                }
            }
        }
        else {
            let tempTermPaymentChoose = [...this.state.termPaymentChoose];
            let indexDel = tempTermPaymentChoose.indexOf(termpaymentid);
            let arrLength = tempTermPaymentChoose.length;
            if (indexDel !== -1) {
                tempTermPaymentChoose.splice(indexDel, arrLength - indexDel);
                this.setState({
                    termPaymentChoose: tempTermPaymentChoose
                });
            }

            let selectedIds = tempTermPaymentChoose;
            for (var k = 0; k < selectedIds.length; k++) {
                for (var l = 0; l < this.props.item.customer_term_payment.customer_term_payment_data.length; l++) {
                    if (selectedIds[i] === this.props.item.customer_term_payment.customer_term_payment_data[j].custtermpaymentdataid) {
                        totalAmountSel += parseInt(this.props.item.customer_term_payment.customer_term_payment_data[j].termpaymentval);
                    }
                }
            }
        }

        // total amount selected buat bantu CC amount
        this.setState({
            totalAmountSelected: totalAmountSel
        });
    }

    handleBayarButton() {
        this.setState({
            issubmitting: true
        });

        axios.post(Config.APIUrl + "/customer/add/payed/term/data", { tid: this.props.item.customer_term_payment.custtermpaymentid, cpyid: this.props.cpyid, tdid_group: this.state.termPaymentChoose, term_payment_method: this.state.paymentMethod }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {

                    // kalau sukses ganti payment method, submit juga ke espay untuk method tertentu
                    if (this.state.paymentMethod === "301") {

                        // permata
                        let ctpydid = result.data.payed_term_data_id;

                        // post ke endpoint untuk espay
                        axios.post(Config.APIUrl + "/customer/payed/term/data/payment/method", { cpyid: this.props.cpyid, tpydid: ctpydid, method: this.state.paymentMethod }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                            .then(result => {

                                // kalau sukses keduanya baru reset params, dan refetch data
                                if (result.data.status === "1") {
                                    this.setState({
                                        submitresult: 'Success',
                                        termPaymentChoose: [],
                                        termPaymentCompleted: this.props.item.customer_term_payment !== null ? this.props.item.customer_term_payment.customer_term_payment_data.length > 0 ? this.props.item.customer_term_payment.customer_term_payment_data.reduce((accumulator, currentValue) => {
                                            if (currentValue.custtermpayeddataid !== null) return accumulator + 1;
                                            else return accumulator;
                                        }, 0) : 0 : 0
                                    });

                                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                                        .then(result => {
                                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                                        })
                                        .catch(e => {
                                            console.log(e);
                                        });
                                }
                                else {
                                    this.setState({
                                        submitresult: 'Error'
                                    });
                                }

                                console.log(result);
                            })
                            .catch(e => {
                                console.log(e);
                            });
                    }
                    else if (this.state.paymentMethod === "308") {
                        // cc
                        let ctpydid = result.data.payed_term_data_id;
                        this.redirectToEspay(this.state.totalAmountSelected, ctpydid);
                    }
                    else {
                        // kalau sukses keduanya baru reset params, dan refetch data -- atau kalau bukan espay
                        if (result.data.status === "1") {
                            this.setState({
                                submitresult: 'Success',
                                termPaymentChoose: [],
                                termPaymentCompleted: this.props.item.customer_term_payment !== null ? this.props.item.customer_term_payment.customer_term_payment_data.length > 0 ? this.props.item.customer_term_payment.customer_term_payment_data.reduce((accumulator, currentValue) => {
                                    if (currentValue.custtermpayeddataid !== null) return accumulator + 1;
                                    else return accumulator;
                                }, 0) : 0 : 0
                            });

                            axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                                .then(result => {
                                    ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                                })
                                .catch(e => {
                                    console.log(e);
                                });
                        }
                        else {
                            this.setState({
                                submitresult: 'Error'
                            });
                        }
                    }
                }
                else {
                    this.setState({
                        submitresult: 'Error'
                    });
                }

                this.setState({
                    issubmitting: false
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleHapusPembayaranButton(tpydid) {
        this.setState({
            issubmitting: true
        });

        let url = '/customer/delete/payed/term/data';
        let cpyid = this.props.item.custpaymentid;

        axios.post(Config.APIUrl + url, { cpyid: cpyid, tpydid: tpydid }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {
                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                        .then(result => {
                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                        })
                        .catch(e => {
                            console.log(e);
                        });
                }

                this.setState({
                    issubmitting: false
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleAttachBuktiButton(tpydid) {
        this.setState({
            selectedTpydid: tpydid
        });
    }

    handleSendBuktiButton(e) {
        this.setState({
            issubmitting: true
        });

        let url = '/customer/add/payment/term/transfer/attach';

        var formdata = new FormData();
        formdata.set('cpyid', this.props.cpyid);
        formdata.set('tpydid', this.state.selectedTpydid);
        formdata.set('bank', this.state.namabank);
        formdata.set('accname', this.state.namaakun);
        formdata.append('transfer_attach', this.state.filebuktibayar); 

        axios.post(Config.APIUrl + url, formdata, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {
                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                        .then(result => {
                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));

                            this.setState({
                                issubmitting: false
                            });
                        })
                        .catch(e => {
                            console.log(e);
                        });

                    this.setState({
                        namabank: '',
                        namaakun: '',
                        filebuktibayar: null
                    });

                    $('#modalAttachBukti').modal('hide');
                    $('#filebuktibayar').val(''); 
                }
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleInputChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    handleFileInputChange(e) {
        this.setState({
            filebuktibayar: e.target.files[0]
        });
    }

    handleFlexibleBayarButton() {
        this.setState({
            issubmitting: true
        });

        axios.post(Config.APIUrl + "/customer/add/payed/flexible/data", { fid: this.props.item.customer_flexible_payment.custflexpaymentid, cpyid: this.props.cpyid, payedval: this.state.jumlahbayarflexible, flexible_payment_method: this.state.flexiblepaymentmethod }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {

                    // kalau sukses ganti payment method, submit juga ke espay untuk method tertentu
                    if (this.state.flexiblepaymentmethod === "301") {

                        // permata
                        let fpydid = result.data.payed_flex_data_id;

                        // post ke endpoint untuk espay
                        axios.post(Config.APIUrl + "/customer/payed/flexible/data/payment/method", { cpyid: this.props.cpyid, fpydid: fpydid, method: this.state.flexiblepaymentmethod }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                            .then(result => {

                                // kalau sukses keduanya baru reset params, dan refetch data
                                axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                                    .then(result => {
                                        ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                                    })
                                    .catch(e => {
                                        console.log(e);
                                    });
                            })
                            .catch(e => {
                                console.log(e);
                            });
                    }
                    else if (this.state.flexiblepaymentmethod === "308") {
                        // cc
                        let fpydid = result.data.payed_flex_data_id;
                        this.redirectToEspay(this.state.jumlahbayarflexible, fpydid);
                    }
                    else {
                        // kalau sukses keduanya baru reset params, dan refetch data -- atau kalau bukan espay
                        if (result.data.status === "1") {
                            axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                                .then(result => {
                                    ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                                })
                                .catch(e => {
                                    console.log(e);
                                });
                        }
                        else {
                            this.setState({
                                submitresult: 'Error'
                            });
                        }
                    }

                    this.setState({
                        jumlahbayarflexible: 0
                    });

                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                        .then(result => {
                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                        })
                        .catch(e => {
                            console.log(e);
                        });
                }

                this.setState({
                    issubmitting: false
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleAttachBuktiFlexibleButton(fpydid) {
        this.setState({
            selectedFpydid: fpydid
        });
    }

    handleSendBuktiFlexibleButton() {
        this.setState({
            issubmitting: true
        });

        let url = '/customer/add/payment/flexible/transfer/attach';

        var formdata = new FormData();
        formdata.set('cpyid', this.props.cpyid);
        formdata.set('fpydid', this.state.selectedFpydid);
        formdata.set('bank', this.state.namabank);
        formdata.set('accname', this.state.namaakun);
        formdata.append('transfer_attach', this.state.filebuktibayar);

        axios.post(Config.APIUrl + url, formdata, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {
                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                        .then(result => {
                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));

                            this.setState({
                                issubmitting: false
                            });
                        })
                        .catch(e => {
                            console.log(e);
                        });

                    this.setState({
                        namabank: '',
                        namaakun: '',
                        filebuktibayar: null
                    });

                    $('#modalAttachBukti').modal('hide');
                    $('#filebuktibayar').val('');
                }
            })
            .catch(e => {
                console.log(e);
            });
    }

    handleHapusPembayaranFlexibleButton(fpydid) {
        this.setState({
            issubmitting: true
        });

        let url = '/customer/delete/payed/flexible/data';
        let cpyid = this.props.item.custpaymentid;

        axios.post(Config.APIUrl + url, { cpyid: cpyid, fpydid: fpydid }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
            .then(result => {
                if (result.data.status === "1") {
                    axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + this.props.cpyid, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                        .then(result => {
                            ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={this.props.cpyid} accesstoken={this.props.accesstoken} />, document.getElementById('divPaymentHistory'));
                        })
                        .catch(e => {
                            console.log(e);
                        });
                }

                this.setState({
                    issubmitting: false
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    handlePaymentMethodTermOption(e) {
        this.setState({
            paymentMethod: e.target.value
        });
    }

    handlePaymentMethodFlexibleOption(e) {
        this.setState({
            flexiblepaymentmethod: e.target.value
        });
    }

    redirectToEspay(amount, paymentId) {
        window.location = "https://sandbox-kit.espay.id/index/order/?url=" + window.location.origin + "/payment/history/" + this.props.cpyid + "&key=e08e895b1666f60eedc2a66a7ed849e5&paymentId=" + paymentId + "&paymentAmount=" + amount + "&commCode=SGWTUKMU&bankCode=008&productCode=CREDITCARD";
    }

    render() {
        const Stans = this.props.item.payment_grid_with_code.map((v, j) => {
            if (this.props.item.payment_grid_with_code.length - 1 === j) {
                return v.code;
            }
            else {
                return v.code + ',';
            }
        });

        let eventImage = null;
        if (this.props.item.space_detail[0].first_media !== null) {
            eventImage = <img className="img-responsive w-100" src={Config.BaseUrl + "/picGet/" + this.props.item.space_detail[0].first_media.spacemediapath} alt="event-image" />;
        }

        const event = (
            <div className="row">
                <div className="col-sm-3">
                    {eventImage}
                </div>
                <div className="col-sm-9">
                    <h3>{this.props.item.space_detail[0].eventname}</h3>
                    <div className="row">
                        <div className="col-sm-9">
                            <div className="row">
                                <div className="col-sm-6 mb-1">
                                    <img src="/assets/master/img/search-engine/date.png" style={{ width: '20px' }} />&nbsp;&nbsp;{this.props.item.startbookdate + " - " + this.props.item.finishbookdate}
                                </div>
                                <div className="col-sm-6 mb-1">
                                    <img src="/assets/master/img/search-engine/location.png" style={{ width: '20px' }} />&nbsp;&nbsp;{this.props.item.space_detail[0].spacename}
                                </div>
                            </div>
                        </div>
                    </div>
                    <br />
                    <span className="c-gray">STAN</span><br />
                    <span><strong>{Stans}</strong></span><br />
                    <div className="col-sm-9 p-0">
                        <hr className="bg-gray m-0" />
                    </div>
                    <br />
                    <span className="c-gray">BRAND</span><br />
                    <span><strong>{this.props.item.customer_brand !== null ? this.props.item.customer_brand.brandname : null}</strong></span><br />
                    <div className="col-sm-9 p-0">
                        <hr className="bg-gray m-0" />
                    </div>
                    <br />
                    <span className="c-gray">PENGELOLA / EVENT ORGANIZER</span><br />
                    <span><strong>{this.props.item.space_detail[0].space_provider.spaceprovname}</strong></span><br />
                    <br />
                </div>
            </div>
        );

        const listTerm = this.props.item.customer_term_payment !== null ? this.props.item.customer_term_payment.customer_term_payment_data.map((v, i) => {
            let termpaymentstatus = "";
            switch (v.termpaymentstatus) {
                case 0: {
                    termpaymentstatus = "-";
                } break;
                case 1: {
                    termpaymentstatus = "Lunas";
                } break;
            }

            return (
                <tr key={i}>
                    <td>
                        {i + 1}
                    </td>
                    <td>
                        {v.paymentdate}
                    </td>
                    <td>
                        {v.termpaymentval}
                    </td>
                    <td>
                        {termpaymentstatus}
                    </td>
                    <td>
                        {
                            v.custtermpayeddataid === null ? <input name={"payment-data-checkbox-" + v.custtermpaymentdataid} type="checkbox" disabled={!(this.state.termPaymentChoose.length >= i - this.state.termPaymentCompleted)} checked={this.state.termPaymentChoose.indexOf(v.custtermpaymentdataid) !== -1} onChange={this.handleCheckboxChange.bind(this, v.custtermpaymentdataid, v.termpaymentval)} /> : ""
                        }
                    </td>
                </tr>);
        }) : null;

        const paymentMethodTermOptions = this.state.paymentMethodListTerm.map((v, i) => {
            return <option key={i} value={v.methodid}>{v.methodname}</option>;
        });

        const listTagihan = (
            <React.Fragment>
                <h2>List Tagihan</h2>
                <table className="table">
                    <thead>
                        <tr>
                            <th>
                                No
                            </th>
                            <th>
                                Tanggal
                            </th>
                            <th>
                                Nilai Tagihan
                            </th>
                            <th>
                                Status
                            </th>
                            <th>
                                &nbsp;
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {listTerm}
                    </tbody>
                </table>
                <div className="row">
                    <div className="offset-sm-9 col-sm-3">
                        <div className="input-group mb-3">
                            <select className="float-right form-control" onChange={this.handlePaymentMethodTermOption}>
                                {paymentMethodTermOptions}
                            </select>
                            <div className="input-group-append">
                                <button className="btn bg-orange c-white" onClick={this.handleBayarButton} disabled={this.state.termPaymentChoose.length > 0 ? "" : "disabled"}>Bayar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );

        const listPayment = this.props.item.customer_term_payment !== null ? this.props.item.customer_term_payment.customer_term_payed_data_detail.map((v, i) => {

            let transferReceiptImage = null;
            if (v.customer_term_payed_data_transfer_detail !== null) {
                transferReceiptImage = (
                    <React.Fragment>
                        <a href={Config.BaseUrl + "/picGet/" + v.customer_term_payed_data_transfer_detail.transferevidencepath} target="_blank"><img className="img-responsive w-100" src={Config.BaseUrl + "/picGet/" + v.customer_term_payed_data_transfer_detail.transferevidencepath} alt="foto-cicilan" /></a><span>Click to enlarge</span>
                    </React.Fragment>
                );
            }

            let termpayedstatus = "";
            switch (v.termpayedstatus) {
                case 0: {
                    termpayedstatus = "-";
                } break;
                case 3: {
                    termpayedstatus = "Verified";
                } break;
            }

            let catatan = '';
            let actionButtons = null;
            if (v.custtermpaymentmethodid === 301) {
                actionButtons = null;
                if (v.customer_term_payment_va !== null) {
                    catatan = "Permata VA Number: " + v.customer_term_payment_va.paymentva;
                }
            }
            else if (v.custtermpaymentmethodid === 308 && v.termpayedstatus === 0) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-9 col-sm-3">
                            <button className="btn bg-orange c-white form-control" onClick={() => this.redirectToEspay(v.termpayedval, v.custtermpayeddataid)}>Bayar</button>
                        </div>
                    </div>
                );
            }
            else if (v.custtermpaymentmethodid === 308 && v.termpayedstatus === 3) {
                actionButtons = null;
            }
            else if (v.customer_term_payed_data_transfer_detail === null) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-8 col-sm-4">
                            <button className="btn bg-orange c-white form-control" data-toggle="modal" data-target="#modalAttachBukti" onClick={() => this.handleAttachBuktiButton(v.custtermpayeddataid)}>Attach Bukti Transfer</button>
                        </div>
                    </div>
                );
            }
            else if (v.customer_term_payed_data_transfer_detail !== null && v.termpayedstatus === 0) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-5 col-sm-4">
                            <button className="btn bg-orange c-white form-control" data-toggle="modal" data-target="#modalAttachBukti" onClick={() => this.handleAttachBuktiButton(v.custtermpayeddataid)}>Re-attach Bukti Transfer</button>
                        </div>
                        <div className="col-sm-3">
                            <button className="btn bg-orange c-white form-control" onClick={() => this.handleHapusPembayaranButton(v.custtermpayeddataid)}>Hapus Pembayaran</button>
                        </div>
                    </div>
                );
            }

            return (
                <div className="p-3 bg-white my-3" key={i}>
                    <div className="row">
                        <div className="col-sm-6">
                            <h3>CICILAN {i + 1}</h3>
                        </div>
                        <div className="col-sm-6">
                            <h4 className="c-orange text-right">{v.termpayeddate}</h4>
                        </div>
                    </div>
                    <hr className="bg-orange" />
                    <div className="row">
                        <div className="col-sm-3">
                            Jumlah Cicilan
                        </div>
                        <div className="col-sm-3">
                            {v.termpayedval !== null ? ConvertToRupiah(v.termpayedval) : ConvertToRupiah(0)}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3">
                            Status
                        </div>
                        <div className="col-sm-3">
                            {termpayedstatus}
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-sm-3">
                            Photo
                        </div>
                        <div className="col-sm-9">
                            Catatan
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3">
                            {transferReceiptImage}
                        </div>
                        <div className="col-sm-9">
                            <textarea className="form-control" rows="5" value={catatan} disabled />
                            <br/>
                            {actionButtons}
                        </div>
                    </div>                    
                </div>
            );
        }) : null;

        let totalTagihan = 0;
        this.props.item.payment_grid_with_code.map((v, i) => {
            totalTagihan += parseInt(v.dealvalue);
        });

        let totalDalamProses = 0;
        if (this.props.item.customer_flexible_payment !== null) {
            this.props.item.customer_flexible_payment.customer_flexible_payment_payed_data.map((v, i) => {
                totalDalamProses += parseInt(v.flexpayedval);
            });
        }

        let totalBelumProses = totalTagihan - totalDalamProses;

        const paymentMethodFlexibleOptions = this.state.paymentMethodListFlexible.map((v, i) => {
            return <option key={i} value={v.methodid}>{v.methodname}</option>;
        });

        let listFlexible = null;
        if (this.props.item.customer_flexible_payment !== null) {
            listFlexible = (
                <React.Fragment>
                    <p>Total Tagihan</p>
                    <p><strong>{ConvertToRupiah(totalTagihan)}</strong></p>
                    <p>Total Tagihan Dalam Proses</p>
                    <p><strong>{ConvertToRupiah(totalDalamProses)}</strong></p>
                    <p>Total Tagihan Belum Proses</p>
                    <p><strong>{ConvertToRupiah(totalBelumProses)}</strong></p>
                    <div className="row">
                        <div className="col-sm-2">
                            <select className="float-right form-control" onChange={this.handlePaymentMethodFlexibleOption}>
                                {paymentMethodFlexibleOptions}
                            </select>
                        </div>
                        <div className="col-sm-2">
                            Sejumlah Rp. 
                        </div>
                        <div className="col-sm-2">
                            <input className="form-control" value={this.state.jumlahbayarflexible} name="jumlahbayarflexible" onChange={this.handleInputChange} />
                        </div>
                        <div className="col-sm-2">
                            <button className="btn bg-orange c-white" onClick={this.handleFlexibleBayarButton}>Bayar</button>
                        </div>
                    </div>
                </React.Fragment>
            );
        };

        const listPaymentFlexible = this.props.item.customer_flexible_payment !== null ? this.props.item.customer_flexible_payment.customer_flexible_payment_payed_data.map((v, i) => {
            let transferReceiptImage = null;
            if (v.customer_flexible_payment_payed_transfer_detail !== null) {
                transferReceiptImage = (
                    <React.Fragment>
                        <a href={Config.BaseUrl + "/picGet/" + v.customer_flexible_payment_payed_transfer_detail.transferevidencepath} target="_blank"><img className="img-responsive w-100" src={Config.BaseUrl + "/picGet/" + v.customer_flexible_payment_payed_transfer_detail.transferevidencepath} alt="foto-cicilan" /></a><span>Click to enlarge</span>
                    </React.Fragment>
                );
            }

            let flexpayedstatus = "";
            switch (v.flexpayedstatus) {
                case 0: {
                    flexpayedstatus = "-";
                } break;
                case 3: {
                    flexpayedstatus = "Verified";
                } break;
            }

            let catatan = '';
            let actionButtons = null;
            if (v.custflexpaymentmethodid === 301) {
                actionButtons = null;
                if (v.customer_flexible_payment_va !== null) {
                    catatan = "Permata VA Number: " + v.customer_flexible_payment_va.paymentva;
                }
            }
            else if (v.custflexpaymentmethodid === 308 && v.flexpayedstatus === 0) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-9 col-sm-3">
                            <button className="btn bg-orange c-white form-control" onClick={() => this.redirectToEspay(v.flexpayedval, v.custflexpayeddataid)}>Bayar</button>
                        </div>
                    </div>
                );
            }
            else if (v.custflexpaymentmethodid === 308 && v.flexpayedstatus === 3) {
                actionButtons = null;
            }
            else if (v.customer_flexible_payment_payed_transfer_detail === null) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-8 col-sm-4">
                            <button className="btn bg-orange c-white form-control" data-toggle="modal" data-target="#modalAttachBukti" onClick={() => this.handleAttachBuktiButton(v.custflexpayeddataid)}>Attach Bukti Transfer</button>
                        </div>
                    </div>
                );
            }
            else if (v.customer_flexible_payment_payed_transfer_detail !== null && v.flexpayedstatus === 0) {
                actionButtons = (
                    <div className="row">
                        <div className="offset-sm-5 col-sm-4">
                            <button className="btn bg-orange c-white form-control" data-toggle="modal" data-target="#modalAttachBukti" onClick={() => this.handleAttachBuktiButton(v.custflexpayeddataid)}>Re-attach Bukti Transfer</button>
                        </div>
                        <div className="col-sm-3">
                            <button className="btn bg-orange c-white form-control" onClick={() => this.handleHapusPembayaranButton(v.custtermpayeddataid)}>Hapus Pembayaran</button>
                        </div>
                    </div>
                );
            }

            return (
                <div className="p-3 bg-white my-3" key={i}>
                    <div className="row">
                        <div className="col-sm-6">
                            <h3>CICILAN {i + 1}</h3>
                        </div>
                        <div className="col-sm-6">
                            <h4 className="c-orange text-right">{v.flexpayeddate}</h4>
                        </div>
                    </div>
                    <hr className="bg-orange" />
                    <div className="row">
                        <div className="col-sm-3">
                            Jumlah Cicilan
                        </div>
                        <div className="col-sm-3">
                            {v.termpayedval !== null ? ConvertToRupiah(v.flexpayedval) : ConvertToRupiah(0)}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3">
                            Status
                        </div>
                        <div className="col-sm-3">
                            {flexpayedstatus}
                        </div>
                    </div>
                    <br />
                    <div className="row">
                        <div className="col-sm-3">
                            Photo
                        </div>
                        <div className="col-sm-9">
                            Catatan
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-3">
                            {transferReceiptImage}
                        </div>
                        <div className="col-sm-9">
                            <textarea className="form-control" rows="5" disabled value={catatan} />
                            <br />
                            {actionButtons}
                        </div>
                    </div>
                </div>
            );
        }) : null;

        return (
            <div className="container my-5">
                <p className="position-fixed" hidden={this.state.issubmitting ? "" : "hidden"} style={{ top: '50%', left: '50%', marginLeft: '-25px', marginTop: '-25px', zIndex: '999', fontSize: '50px'}}><i className="fa fa-spinner fa-spin" /></p>

                {event}
                <hr />
                {this.props.item.customer_term_payment !== null ? listTagihan : null}
                {this.props.item.customer_flexible_payment !== null ? listFlexible : null}
                <hr />
                <h2>List Pembayaran</h2>
                {this.props.item.customer_term_payment !== null ? listPayment : null}
                {this.props.item.customer_flexible_payment !== null ? listPaymentFlexible : null}

                <div className="modal fade" id="modalAttachBukti" tabIndex="-1" role="dialog">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title">Attach Bukti Pembayaran</h5>
                                <button type="button" className="close" data-dismiss="modal">
                                    <span>&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="form-group">
                                    <label>Nama Bank</label>
                                    <input type="text" className="form-control" placeholder="Nama Bank" name="namabank" value={this.state.namabank} onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group">
                                    <label>Nama Akun</label>
                                    <input type="text" className="form-control" placeholder="Nama Akun" name="namaakun" value={this.state.namaakun} onChange={this.handleInputChange} />
                                </div>
                                <div className="form-group">
                                    <label>Bukti Bayar</label>
                                    <input type="file" className="form-control-file" id="filebuktibayar" onChange={this.handleFileInputChange} />
                                </div>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn bg-orange c-white" onClick={this.props.item.customer_term_payment ? this.handleSendBuktiButton : this.handleSendBuktiFlexibleButton}>Kirim</button>
                                <button type="button" className="btn bg-orange c-white" data-dismiss="modal">Batal</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('divPaymentHistory')) {
    const element = document.getElementById('divPaymentHistory');
    if (element.dataset.accesstoken !== undefined && element.dataset.cpyid !== undefined) {
        axios.get(Config.APIUrl + "/customer/view/payment/term?cpyid=" + element.dataset.cpyid, { 'headers': { 'Authorization': 'Bearer ' + element.dataset.accesstoken } })
            .then(result => {
                ReactDOM.render(<PaymentHistory item={result.data.payment_term} cpyid={element.dataset.cpyid} accesstoken={element.dataset.accesstoken} />, element);
            })
            .catch(e => {
                console.log(e);
            });
    }
}
