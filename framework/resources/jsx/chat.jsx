var scripts = document.getElementById('chat_room').getAttribute("src");
var endpoint_url = "https://tukbase3.tukmu.com/";
var queryString = scripts.replace(/^[^\?]+\??/, '');
var res = queryString.split("=");
var res_param = queryString.split("&");
var dataurl = res[1].split("=")[0].split("&")[0];
var cpyid = res_param[1].split("=")[1];
var brand_id = res_param[2].split("=")[1];
var access_token = res_param[3].split("=")[1];
var url = res_param[4].split("=")[1];
var chat_session = res_param[5].split("=")[1];

const headers_ = {
    "Authorization": "Bearer " + access_token
};

var config = {
    apiKey: "AIzaSyDdtiewGNa6PYaGwmQ9pxuyO-AiD2Aoevc",
    authDomain: "bukapameran-45e78.firebaseapp.com",
    databaseURL: "https://bukapameran-45e78.firebaseio.com",
    projectId: "bukapameran-45e78",
    storageBucket: "bukapameran-45e78.appspot.com",
    messagingSenderId: "929258258899"
};

firebase.initializeApp(config);
const messaging = firebase.messaging();

messaging.requestPermission()
    .then(function () {
        console.log("have Permission");
        return messaging.getToken();
    })
    .catch(function (e) {
        console.log("error ", e);
    });

messaging.getToken().then(function (token) {
    const fd = new FormData();
    fd.append("register_id", token);
    fd.append("authenticator", "browser");
    fd.append("cpyid", cpyid);

    axios.post("https://tukbase3.tukmu.com/api/customer/add/nego/chat/init", fd, { headers: headers_ })
        .then(data => {
            localStorage.setItem("_chat_token", data.data.token.chat_session_token);
        })
        .catch(e => {
            console.log(e);
            if (e == "Error: Request failed with status code 401") {
                window.location.replace(url + "/logout");
            }
        });
});

const headers_chat = {
    "Authorization": "Bearer " + access_token,
    "chat-session": "Bearer " + localStorage.getItem("_chat_token")
};

class Chat extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // chat_session: '',
            chat_text: '',
            chat_list: [],
            loading: false,
            loading_data: false,
            loading_chat: false,
            stan_list: [],
            posisi_list: [],
            total_stan: 0,
            total_amount: 0,
            branddetail: {},
            addon: []
        };

        this.handleChangeChat = this.handleChangeChat.bind(this);
    }

    componentDidMount() {
        this.getDataChat();
        this.getDataStan();
        this.getDataStan1();
        this.getDataBrandDetail();
        this.getTambahan();

        navigator.serviceWorker.addEventListener('message', event => {
            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ })
                .then(data => {
                    this.setState({
                        chat_list: data.data.nego_chat
                    });
                })
                .catch(e => {
                });
        });

        messaging.onMessage(payload => {
            new Notification('New Chat Message', {
                icon: 'https://cdn.sstatic.net/stackexchange/img/logos/so/so-icon.png',
                body: payload.data.body,
            });
            var audioElement = document.createElement('audio');
            audioElement.setAttribute('src', url + '/assets/master/audio/sound.mp3');
            audioElement.play();

            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ })
                .then(data => {
                    this.setState({
                        chat_list: data.data.nego_chat
                    });
                })
                .catch(e => {
                });


            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat })
                .then(data => {
                    this.setState({
                        stan_list: data.data.payment_grid_with_code
                    });
                })
                .catch(e => {
                    console.log(e);
                });

            axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat })
                .then(data => {
                    this.setState({
                        posisi_list: data.data.payment_grid_with_code,
                        total_stan: data.data.payment_grid_with_code.length
                    });
                    var total = [];
                    data.data.payment_grid_with_code.map((value, i) => {
                        total.push(parseInt(value.dealvalue));
                    });
                    var sum = total.reduce((a, b) => a + b, 0);
                    this.setState({
                        total_amount: this.convertToRupiah(sum)
                    });
                })
                .catch(e => {
                    console.log(e);
                });
        });
    }

    componentDidUpdate() {
        // get the messagelist container and set the scrollTop to the height of the container
        if (!this.state.loading_data) {
            const objDiv = document.getElementById('chat');
            objDiv.scrollTop = objDiv.scrollHeight;
        }
    }

    getDataBrandDetail() {
        this.setState({
            loading_data: true
        });
        axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ })
            .then(data => {
                try {
                    var otherfee = 0;
                    for (var i = 0; i < data.data.customer_payment_other_fee.length; i++) {
                        otherfee = otherfee + parseInt(data.data.customer_payment_other_fee[i].otherpayvalue);
                    }
                    this.setState({
                        branddetail: {
                            startdate: data.data.startdate,
                            finishdate: data.data.finishdate,
                            spacename: data.data.spacename,
                            minprice: data.data.minprice,
                            maxprice: data.data.maxprice,
                            brandname: data.data.customer_brand.brandname,
                            payment_grid_with_code: data.data.payment_grid_with_code,
                            code: data.data.payment_grid_with_code[0].code,
                            customer_brand: data.data.customer_brand,
                            desc: data.data.desc,
                            namaspaceprov: data.data.namaspaceprov,
                            termpath: data.data.term_condition.length > 0 ? data.data.term_condition[0].spacetermpath : '',
                            eventname: data.data.eventname,
                            eventpic: data.data.space_detail.length > 0 ? data.data.space_detail[0].first_media != null ? data.data.space_detail[0].first_media.spacemediapath : '' : '',
                            depositvalue: data.data.customer_payment_deposit.depositvalue,
                            otherfeevalue: otherfee,
                            customer_term_payment: data.data.customer_term_payment,
                            customer_flexible_payment: data.data.customer_flexible_payment
                        },
                        loading_data: false
                    });
                }
                catch (e)
                {
                    console.log(e);
                }
            })
            .catch(e => {
                // console.log(e)
                this.setState({
                    loading_data: false
                });
            });
    }

    getDataChat() {
        this.setState({
            loading_chat: true
        });
        axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ })
            .then(data => {
                this.setState({
                    chat_list: data.data.nego_chat,
                    loading_chat: false
                });
            })
            .catch(e => {
                // console.log(e)
                this.setState({
                    loading_chat: false
                });
            });
    }

    getTambahan() {
        this.setState({
            loading_data: true
        });
        axios.get(dataurl + 'api/event/view/detail/addon?cpyid=' + cpyid, { headers: headers_ })
            .then(data => {
                this.setState({
                    addon: data.data.addon_data,
                    loading_data: false
                });
            })
            .catch(e => {
                // console.log(e)
                this.setState({
                    loading_data: false
                });
            });
    }

    handleChangeChat(event) {
        this.setState({ chat_text: event.target.value });
    }

    sendChat() {
        // this.setState({
        //     chat_list: this.state.chat_list.concat(this.state.chat_text)
        // })

        const fd = new FormData();
        fd.append("cust_chat", this.state.chat_text);
        fd.append("cpyid", cpyid);
        fd.append("chat_type", 0);

        this.setState({
            loading: true
        });

        axios.post(dataurl + "api/customer/add/nego/chat/send", fd, { headers: headers_chat })
            .then(data => {
                // console.log(data)
                if (data.data.status === "1") {
                    axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_ })
                        .then(data => {
                            // console.log(data)
                            this.setState({
                                chat_list: data.data.nego_chat,
                                chat_text: '',
                                loading: false
                            });
                        })
                        .catch(e => {
                            console.log(e);
                            this.setState({
                                loading: false
                            });
                        });
                }
            })
            .catch(e => {
                // console.log(e)
            });
        // console.log(fd)
    }

    rejectSPOffer() {
        const fd = new FormData();
        fd.append("cpyid", cpyid);

        this.setState({
            loading: true
        });

        axios.post(dataurl + "api/customer/add/nego/deal/button/2", fd, { headers: headers_chat })
            .then(data => {
                console.log(data);
            });
    }

    getDataStan() {
        axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat })
            .then(data => {
                // console.log(data)
                this.setState({
                    stan_list: data.data.payment_grid_with_code
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    getDataStan1() {

        axios.get(dataurl + 'api/customer/view/order/booking/pending/spec?cpyid=' + cpyid, { headers: headers_chat })
            .then(data => {
                // console.log(data)
                this.setState({
                    posisi_list: data.data.payment_grid_with_code,
                    total_stan: data.data.payment_grid_with_code.length
                });

                // var t = data.data.payment_grid_with_code.filter( obj => obj.name === 'dealvalue')[0];
                // console.log(t)
                var total = [];
                data.data.payment_grid_with_code.map((value, i) => {
                    // console.log(value.dealvalue)
                    total.push(parseInt(value.dealvalue));
                });

                // console.log(total)
                var sum = total.reduce((a, b) => a + b, 0);
                // console.log(sum);
                this.setState({
                    total_amount: this.convertToRupiah(sum)
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    convertToRupiah(angka) {
        var rupiah = '';
        var angkarev = angka.toString().split('').reverse().join('');
        for (var i = 0; i < angkarev.length; i++) if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
        return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
    }

    formatDate(date) {

        var monthNames = [
            "Januari", "Februari", "Maret",
            "April", "Mei", "Juni", "Juli",
            "Augustus", "September", "Oktober",
            "November", "Desember"
        ];

        var hari = [
            "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
        ];

        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12
        minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = this.addZero(hours) + ':' + this.addZero(minutes) + ' ' + ampm;

        var hariIndex = date.getDay();
        var tanggal = this.addZero(date.getDate());
        var monthIndex = date.getMonth();
        var year = date.getFullYear();

        return tanggal + ' ' + monthNames[monthIndex] + ' ' + year;
    }

    addZero(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }

    render() {
        const { branddetail, loading_data, loading_chat } = this.state;

        const ListChat = this.state.chat_list.map((value, i) => {
            if (value.sp_name === null && value.cust_name === null) {
                return (
                    <div key={i} className="row ml-5 mr-1 my-3">
                        <div className="col-11 p-3" style={{ borderRadius: '1rem', backgroundColor: '#b7a7f8' }}>
                            <div>SYSTEM</div>
                            <hr className="my-1" style={{ borderColor: 'white' }} />
                            <div>{value.chatvalue}</div>
                        </div>
                        <div className="col-1 d-flex p-1">
                            <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                        </div>
                    </div>
                );
            }
            else if (value.sp_name !== null) {
                if (value.chattype === 0) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>{value.chatvalue}</div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
                else if (value.chattype === 3) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>{value.chatvalue}</div>
                                <div>{"Rp. " + value.chatparams.total}</div>
                                <div>
                                    {
                                        value.chatparams.status === "0" ?
                                            <div>
                                                <a href={url + "/tambahan/1/" + chat_session + "/" + cpyid}>YES</a>&nbsp;&nbsp;
                                                        <a href="#" onClick={() => this.rejectSPOffer()}>NO</a>
                                            </div>
                                            : null
                                    }
                                    {
                                        value.chatparams.status === "1" ?
                                            <div>Approved</div> : null
                                    }
                                    {
                                        value.chatparams.status === "2" ?
                                            <div>Rejected</div> : null
                                    }
                                </div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
                else if (value.chattype === 4) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>Perubahan Harga</div>
                                <div>{value.chatparams.grid_code + " - " + this.convertToRupiah(parseInt(value.chatparams.price))}</div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
                else if (value.chattype === 5) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>Perubahan Tanggal</div>
                                <div>Dari: {value.chatparams.old_start_booking_date + " - " + value.chatparams.old_finish_booking_date}</div>
                                <div>Menjadi: {value.chatparams.start_booking_date + " - " + value.chatparams.finish_booking_date}</div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
                else if (value.chattype === 6) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>Biaya Lain-lain</div>
                                <div>{decodeURI(value.chatparams.other_fee_name) + " - " + this.convertToRupiah(parseInt(value.chatparams.other_fee_value))}</div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
                else if (value.chattype === 7) {
                    return (
                        <div key={i} className="row ml-5 mr-1 my-3">
                            <div className="bg-orange col-11 p-3" style={{ borderRadius: '1rem' }}>
                                <div>{value.sp_name}</div>
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                <div>Deposit</div>
                                <div>{this.convertToRupiah(parseInt(value.chatparams.deposit))}</div>
                            </div>
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-orange.png" />
                            </div>
                        </div>
                    );
                }
            }
            else if (value.cust_name !== null) {
                if (value.chattype === 0) {
                    return (
                        <div key={i} className="row ml-1 mr-5 my-3">
                            <div className="col-1 d-flex p-1">
                                <img className="img-responsive w-100 align-self-center" src="/assets/master/img/user/user-gray.png" />
                            </div>
                            <div className="bg-light-gray col-11 p-3" style={{ borderRadius: '1rem' }}>
                                {value.cust_name}
                                <hr className="my-1" style={{ borderColor: 'white' }} />
                                {value.chatvalue}
                            </div>
                        </div>
                    );
                }
            }
        });

        const ListFile = this.state.chat_list.map((value, i) => {
            if (value.sp_name !== null) {
                if (value.chattype === 1) {
                    return (
                        <div className="p-2 border-bottom">
                            <span><i className="far fa-file f-s-20 align-middle"></i></span>
                            <a className="p-2 align-middle" href={endpoint_url + "/fileGet/" + value.nego_attachment[0].attachmentpath} target="_blank">{value.chatvalue}</a>
                        </div>
                    );
                }
            }
            else if (value.cust_name !== null) {
                if (value.chattype == 1) {
                    return (
                        <div className="p-2 border-bottom">
                            <span><i className="far fa-file f-s-20 align-middle"></i></span>
                            <a className="p-2 align-middle" href={endpoint_url + "/fileGet/" + value.nego_attachment[0].attachmentpath} target="_blank">{value.chatvalue}</a>
                        </div>
                    );
                }
            }
        });

        const ListLink = this.state.chat_list.map((value, i) => {
            if (value.sp_name !== null) {
                if (value.chattype == 2) {
                    return (
                        <div className="p-2 border-bottom">
                            <span><i className="fa fa-link f-s-20 align-middle"></i></span>
                            <a className="p-2 align-middle" href={value.nego_link[0].linkpath} target="_blank">{value.nego_link[0].linkname}</a>
                            <br /><small>{value.nego_link[0].linkpath}</small>
                        </div>
                    );
                }
            }
            else if (value.cust_name !== null) {
                if (value.chattype == 2) {
                    return (
                        <div className="p-2 border-bottom">
                            <span><i className="fa fa-link f-s-20 align-middle"></i></span>
                            <a className="p-2 align-middle" href={value.nego_link[0].linkpath} target="_blank">{value.nego_link[0].linkname}</a>
                            <br /><small>{value.nego_link[0].linkpath}</small>
                        </div>
                    );
                }
            }
        });

        const ListAddon = this.state.addon.map((value, i) => {
            return (
                <tr className="t-a-c">
                    <td className="bg-light-orange">{value.spaceaddsrvdesc}</td>
                    <td className="bg-light-yellow">{this.convertToRupiah(parseInt(value.spaceaddsrvprice))}</td>
                    <td className="bg-light-orange">{value.remaining}</td>
                </tr>
            );
        });

        const stan = this.state.stan_list.map((value, i) =>
            <div key={i} style={{ marginRight: '2px' }}>
                {value.code},
            </div>
        )

        const posisi = this.state.posisi_list.map((value, i) =>
            <tr key={i}>
                <td className="bg-light-gray">{value.code}</td>
                <td className="bg-light-gray">{this.convertToRupiah(parseInt(value.dealvalue))}</td>
            </tr>
        )

        const DetailCicilan = () => {
            if (this.state.branddetail.payment_grid_with_code != undefined) {
                const header = this.state.branddetail.payment_grid_with_code.map((value, i) => {
                    if (this.state.branddetail.payment_grid_with_code.length == i + 1) {
                        return value.code;
                    }
                    else {
                        return value.code + ", ";
                    }
                });
                var total = [];
                this.state.branddetail.payment_grid_with_code.map((value, i) => {
                    total.push(parseInt(value.dealvalue))
                })
                var sum = total.reduce((a, b) => a + b, 0);
                var termdates = null;
                if (this.state.branddetail.customer_term_payment != null) {
                    termdates = this.state.branddetail.customer_term_payment.customer_term_payment_data.map((value, i) => {
                        if (i != 0) {
                            return (
                                <tr>
                                    <td>{("0" + new Date(value.paymentdate).getDate()).slice(-2)}</td>
                                    <td>{("0" + (new Date(value.paymentdate).getMonth() + 1)).slice(-2)}</td>
                                    <td>{new Date(value.paymentdate).getFullYear()}</td>
                                </tr>
                            );
                        }
                    });
                }
                if (this.state.branddetail.customer_term_payment == null && this.state.branddetail.customer_flexible_payment == null) {
                    return (
                        <div>
                            <div className="text-center c-white bg-orange p-2"><strong>{header}</strong></div>
                            <div className="bg-light-gray">
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Pembayaran di Muka
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.convertToRupiah(parseInt(sum))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
                else if (this.state.branddetail.customer_term_payment != null) {
                    return (
                        <div>
                            <div className="text-center c-white bg-orange p-2"><strong>{header}</strong></div>
                            <div className="bg-light-gray">
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Harga Total
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.convertToRupiah(parseInt(sum))}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Down Payment
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.state.branddetail.customer_term_payment.customer_term_payment_data != undefined ?
                                            this.convertToRupiah(parseInt(this.state.branddetail.customer_term_payment.customer_term_payment_data[0].termpaymentval)) :
                                            this.convertToRupiah(parseInt(sum))}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Batas Akhir Tenor
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.formatDate(new Date(this.state.branddetail.customer_term_payment.finishtermdate))}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Tenor
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.state.branddetail.customer_term_payment.customer_term_payment_data != undefined ?
                                            this.state.branddetail.customer_term_payment.customer_term_payment_data.length - 1 : 0}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Tanggal Ditagih
                                    </div>
                                    <div className="col-sm-8 p-2" style={{ maxHeight: '200px', overflow: 'auto' }}>
                                        <table className="table table-striped table-borderless text-center bg-white">
                                            <thead className="bg-orange">
                                                <tr>
                                                    <th>Tanggal</th>
                                                    <th>Bulan</th>
                                                    <th>Tahun</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {termdates}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2 border-top border-bottom border-warning">
                                        Tagihan Per Tenor
                                    </div>
                                    <div className="col-sm-8 p-2 border-top border-bottom border-warning">
                                        <strong>
                                            {
                                                this.convertToRupiah(
                                                    this.state.branddetail.customer_term_payment.customer_term_payment_data !== undefined ?
                                                        this.state.branddetail.customer_term_payment.customer_term_payment_data.length > 1 ?
                                                            this.state.branddetail.customer_term_payment.customer_term_payment_data[1].termpaymentval : 0 : 0)
                                            }
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
                else if (this.state.branddetail.customer_flexible_payment != null) {
                    return (
                        <div>
                            <div className="text-center c-white bg-orange p-2"><strong>{header}</strong></div>
                            <div className="bg-light-gray">
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Harga Total
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.convertToRupiah(parseInt(sum))}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Batas Awal Tenor
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.formatDate(new Date(this.state.branddetail.customer_flexible_payment.startflexdate))}
                                    </div>
                                </div>
                                <div className="row p-3">
                                    <div className="col-sm-4 p-2">
                                        Batas Akhir Tenor
                                    </div>
                                    <div className="col-sm-8 p-2">
                                        {this.formatDate(new Date(this.state.branddetail.customer_flexible_payment.finishflexdate))}
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }
            }
        }

        return (
            <React.Fragment>
                {
                    loading_data ?
                        <div style={{ width: "300px", margin: "100px auto" }}>
                            <p style={{ textAlign: 'center' }}>
                                <i className="fas fa-spinner fa-spin" style={{ fontSize: '90px', textAlign: 'center' }}></i>
                            </p>
                        </div>
                        :
                        <div className="container content m-t-15 m-b-15">
                            <div className="row">
                                <div className="col-md-12">
                                    <h2 className="f-w-300">PEMESANAN</h2>
                                    <div className="row content-booking">
                                        <div className="col-md-6">
                                            <div className="bg-white p-15 m-b-15">
                                                <div className="row">
                                                    <div className="col-md-6">
                                                        <h2 className="f-w-300">{branddetail.eventname}</h2>
                                                        <br />
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-calendar-alt c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>{this.formatDate(new Date(branddetail.startdate))} - {this.formatDate(new Date(branddetail.finishdate))}</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-map-marker-alt c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>{branddetail.spacename}</span>
                                                            </div>
                                                        </div>
                                                        <div className="d-flex">
                                                            <div className="p-2 p-r-l">
                                                                <i className="fa fa-dollar-sign c-orange"></i>
                                                            </div>
                                                            <div className="p-2 p-r-l">
                                                                <span>Rp. {branddetail.minprice} - Rp. {branddetail.maxprice}</span>
                                                            </div>
                                                        </div>
                                                        <br />
                                                        <h6 className="m-r-b f-s-14">STAN</h6>
                                                        <span className="c-orange t-td-u">
                                                            <div id="stan" style={{ display: 'flex' }}>
                                                                {stan}
                                                            </div>
                                                        </span>
                                                        <hr />
                                                        {branddetail.customer_brand !== null ?
                                                            <React.Fragment>
                                                                <h6 className="m-r-b f-s-14">BRAND</h6>
                                                                <span className="c-orange">{branddetail.brandname}</span>
                                                            </React.Fragment>
                                                            : null
                                                        }
                                                        <hr />
                                                        <h6 className="m-r-b f-s-14">PENGELOLA</h6>
                                                        <span className="c-orange t-td-u">{branddetail.namaspaceprov} </span>
                                                        <hr />
                                                    </div>
                                                    <div className="col-md-6">
                                                        <img className="img-fluid img-thumbnail" style={{ width: '100%' }} src={endpoint_url + "/picGet/" + branddetail.eventpic} />
                                                        <br /><br />
                                                        <h6 className="m-r-b">KONDISI & SYARAT</h6>
                                                        <a className="c-orange t-td-u" target="_blank" href={endpoint_url + "/fileGet/" + branddetail.termpath}><span>LIHAT</span></a>
                                                    </div>
                                                    <div className="col-md-12">
                                                        <h6>TENTANG ACARA</h6>
                                                        <p className="f-s-14" dangerouslySetInnerHTML={{ __html: branddetail.desc }} />
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="bg-white p-15">
                                                <h2 className="m-r-b">TAMBAHAN</h2>
                                                <span className="c-orange f-s-12"><i>Anda baru bisa memilih tambahan setelah menyelesaikan negosiasi</i></span>
                                                <div className="table-responsive-md m-t-15 m-b-15">
                                                    <table className="table">
                                                        <thead>
                                                            <tr className="bg-orange c-white t-a-c">
                                                                <td>JENIS</td>
                                                                <td>HARGA</td>
                                                                <td>SISA</td>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            {ListAddon}
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 p-r-l chat-content">
                                            <div className="bg-light-black p-15">
                                                <div className="b-radius-15 p-b-15 p-r-15 p-l-15 bg-dark-gray">
                                                    <h3 className="t-a-c f-w-300 c-white">PESAN</h3>
                                                    <div id="chat_content">
                                                        <ul className="nav nav-tabs tabs-pesan" id="myTab" role="tablist">
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link active c-black" id="home-tab" data-toggle="tab" href="#chat" role="tab" aria-controls="chat" aria-selected="true">CHAT</a>
                                                            </li>
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link c-black" id="profile-tab" data-toggle="tab" href="#dokumen" role="tab" aria-controls="dokumen" aria-selected="false">DOKUMEN</a>
                                                            </li>
                                                            <li className="nav-item bg-light-gray b-radius-t-10">
                                                                <a className="nav-link c-black" id="contact-tab" data-toggle="tab" href="#link" role="tab" aria-controls="link" aria-selected="false">LINK</a>
                                                            </li>
                                                        </ul>
                                                        <div className="tab-content content-pesan" id="myTabContent">
                                                            <div className="tab-pane fade show active p-5" id="chat" role="tabpanel" aria-labelledby="chat-tab" style={{ height: '400px', overflowY: 'scroll' }}>
                                                                {
                                                                    loading_chat ?
                                                                        <p style={{ textAlign: 'center' }}>
                                                                            <i className="fas fa-spinner fa-spin" style={{ fontSize: '40px', textAlign: 'center' }}></i>
                                                                        </p>
                                                                        :
                                                                        ListChat
                                                                }
                                                            </div>
                                                            <div className="tab-pane fade p-5" id="dokumen" role="tabpanel" aria-labelledby="dokumen-tab" style={{ height: '400px', overflowY: 'scroll' }}>
                                                                {ListFile}
                                                            </div>
                                                            <div className="tab-pane fade p-5" id="link" role="tabpanel" aria-labelledby="link-tab" style={{ height: '400px', overflowY: 'scroll' }}>
                                                                {ListLink}
                                                            </div>
                                                        </div>
                                                        <div className="p-5 bg-white m-t-5 b-radius-10 papan-ketik-pesan">
                                                            <div className="d-flex bd-highlight">
                                                                <div className="p-2 flex-grow-1 bd-highlight">
                                                                    <textarea style={{
                                                                        borderTop: '1px solid #bebebe', borderBottom: '1px solid #bebebe',
                                                                        borderRight: '1px solid #bebebe', borderLeft: '1px solid #bebebe', borderRadius: '5px'
                                                                    }} className="form-control text-area" rows="2" value={this.state.chat_text} onChange={this.handleChangeChat} />
                                                                </div>
                                                                <div className="p-2 bd-highlight p-r-r">
                                                                    <button className="link">
                                                                        <i className="fa fa-link c-orange"></i>
                                                                    </button>
                                                                </div>
                                                                <div className="p-2 bd-highlight p-r-r">
                                                                    <button className="link">
                                                                        <i className="fa fa-paperclip c-orange"></i>
                                                                    </button>
                                                                </div>
                                                                <div className="p-2 bd-highlight">
                                                                    {
                                                                        this.state.loading ?
                                                                            <i className="fas fa-spinner fa-spin" style={{ fontSize: '30px' }}></i>
                                                                            :
                                                                            <button className="link bg-orange" onClick={() => this.sendChat()}>
                                                                                <i className="fa fa-paper-plane c-white"></i>
                                                                            </button>
                                                                    }
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <React.Fragment>
                                                    <div className="table-responsive m-t-15">
                                                        <table className="table t-a-c m-r-b">
                                                            <thead>
                                                                <tr className="bg-orange c-white">
                                                                    <td>NO. POSISI</td>
                                                                    <td>HARGA</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                {posisi}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="table-responsive">
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-gray c-white">
                                                                    <td>TOTAL STAN</td>
                                                                    <td className="t-a-r">{this.state.total_stan}</td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div className="table-responsive">
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-light-black c-white">
                                                                    <td className="b-r-top">
                                                                        <i>
                                                                            LAIN LAIN<br />
                                                                            DEPOSIT
                                                                        </i>
                                                                    </td>
                                                                    <td className="t-a-r b-r-top">
                                                                        {this.convertToRupiah(parseInt(branddetail.otherfeevalue))}<br />
                                                                        {this.convertToRupiah(parseInt(branddetail.depositvalue))}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <hr className="b-orange m-r" />
                                                    <div className="table-responsive">
                                                        <table className="table m-r-b">
                                                            <tbody>
                                                                <tr className="bg-light-black c-white">
                                                                    <td className="b-r-top">
                                                                        <h6 className="m-r-b">TOTAL TAGIHAN</h6>
                                                                    </td>
                                                                    <td className="t-a-r b-r-top">
                                                                        {this.state.total_amount}
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </React.Fragment>
                                                <hr className="b-orange m-r" />
                                                <div className="table-responsive">
                                                    <table className="table m-r-b">
                                                        <tbody>
                                                            <tr className="bg-light-black c-white">
                                                                <td className="b-r-top">
                                                                    <span className="t-td-u f-s-12">
                                                                        <a className="c-orange" data-toggle="modal" href="#detail-cicilan"><i>detail cicilan</i></a>
                                                                    </span>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="modal" id="detail-cicilan">
                                                    <div className="modal-dialog">
                                                        <div className="modal-content">
                                                            <div className="modal-body p-0">
                                                                {DetailCicilan()}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                }
            </React.Fragment>
        );
    }
}

ReactDOM.render(
    <Chat />,
    document.getElementById('chat_content')
)
