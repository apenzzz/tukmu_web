import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class MessageList extends React.Component {
    render() {
        const listItems = this.props.items.map((value, i) => {
            let annouceparam = [];
            if (value.announceparam !== undefined) {
                annouceparam = JSON.parse(value.announceparam);
            }

            const highlightItems = Object.keys(annouceparam).map(function (key, i) {
                if (annouceparam[key] !== null) {
                    return (
                        <div className="row" key={i}>
                            <div className="col-sm-3">
                                {key}
                            </div>
                            <div className="col-sm-9">
                                {annouceparam[key]}
                            </div>
                        </div>
                    );
                }
            });

            return (
                <div className="row mb-3" key={i}>
                    <div className="col-sm-12 bg-white p-3">
                        <div className="row">
                            <div className="col-sm-6">
                                <h3>{value.announcevalue}</h3>
                            </div>
                            <div className="col-sm-6">
                                <h3 className="text-right">{value.announcedate} {value.announcetime}</h3>
                            </div>
                        </div>
                        <hr className="bg-orange" />
                        {highlightItems}
                    </div>
                </div>
            );
        });

        return (
            <div className="container my-5">
                <div className="row">
                    <div className="offset-sm-2 col-sm-8">
                        <div className="row">
                            <div className="col-sm-12 bg-orange p-3">
                                <h2 className="text-center c-white">{this.props.title}</h2>
                            </div>
                        </div>
                        <div style={{ maxHeight: '600px', overflowX: 'none', overflowY: 'auto', paddingLeft: '15px', paddingRight: '15px', marginLeft: '-15px', marginRight: '-15px' }}>
                            {listItems}
                        </div>
                        <div className="row">
                            <div className="col-sm-12 bg-orange p-3">
                                <h2 className="text-center c-white">&nbsp;</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('divMessage')) {
    const element = document.getElementById('divMessage');
    let items = [];
    const title = "INFO & PROMOSI";
    if (element.dataset.accesstoken !== undefined) {
        axios.get(Config.APIUrl + "/customer/get/notification", { 'headers': { 'Authorization': 'Bearer ' + element.dataset.accesstoken } })
            .then(result => {
                items = result.data.notification_list;
                ReactDOM.render(<MessageList title={title} items={items} />, element);
            })
            .catch(e => {
                console.log(e);
            });
    }
}
