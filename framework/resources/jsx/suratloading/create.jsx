import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import DatePicker from 'react-datepicker';
import Config from '../common/config';
import moment from 'moment';

class LoadingCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            surat: 1,
            penanggungjawab: '',
            telepon: '',
            tanggal: '',
            namabarang: '',
            qty: 0,
            listbarang: [],
            issubmitting: false,
            submitresult: ''
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handlePlusButton = this.handlePlusButton.bind(this);
        this.handleMinusButton = this.handleMinusButton.bind(this);
        this.handleAddButton = this.handleAddButton.bind(this);
        this.handleDeleteButton = this.handleDeleteButton.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
        this.handleSubmitButton = this.handleSubmitButton.bind(this);
    }

    handlePlusButton() {
        this.setState({
            qty: parseInt(this.state.qty) + 1
        });
    }

    handleMinusButton() {
        if (parseInt(this.state.qty) === 0)
            return;

        this.setState({
            qty: parseInt(this.state.qty) - 1
        });
    }

    handleAddButton() {
        if (this.state.namabarang === '' || this.state.qty === 0) {
            alert('Harap masukkan Nama Barang dan Qty');
            return;
        }

        let newBarang = {
            item_name: this.state.namabarang,
            item_qty: parseInt(this.state.qty)
        };

        const list = this.state.listbarang;
        list.push(newBarang);

        this.setState({
            listbarang: list,
            namabarang: '',
            qty: 0
        });
    }

    handleDeleteButton(e, i) {
        const list = this.state.listbarang;
        list.splice(i, 1);

        this.setState({
            listbarang: list
        });
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        this.setState({
            [name]: value
        });
    }

    handleDateChange(date) {
        this.setState({
            tanggal: date
        });
    }

    handleSubmitButton() {
        if (this.state.penanggungjawab === '' || this.state.telepon === '' || this.state.tanggal === '' || this.state.listbarang.length === 0) {
            alert('Please Fill The Form');
            return;
        }

        let url = '/customer/add/event/loading';

        if (this.state.issubmitting === false) {
            if (confirm("Are You Sure?")) {
                this.setState({
                    issubmitting: true
                });

                axios.post(Config.APIUrl + url, {
                    cpyid: this.props.cpyid,
                    pic_name: this.state.penanggungjawab,
                    pic_telp: this.state.telepon,
                    loading_purpose: this.state.surat,
                    loading_date: moment(this.state.tanggal).format("YYYY-MM-DD"),
                    loading_item: this.state.listbarang
                },
                    { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                    .then(result => {
                        this.setState({
                            penanggungjawab: '',
                            telepon: '',
                            surat: '',
                            tanggal: '',
                            listbarang: [],
                            submitresult: 'Success',
                            issubmitting: false
                        });
                    })
                    .catch(e => {
                        console.log(e);
                    });
            }
        }
    }

    render() {
        const listBarang = this.state.listbarang.map((value, index) => {
            return (
                <tr key={index}>
                    <td>{value.item_name}</td>
                    <td>{value.item_qty}</td>
                    <td><button className="btn bg-orange c-white" type="button" onClick={(e, i) => this.handleDeleteButton(e, index)}>x</button></td>
                </tr>
            );
        });

        return (
            <div className="container">
                <div className="p-3 bg-white">
                    <div className="row form-group">
                        <div className="col-sm-4">
                            <label>Surat</label>
                            <select className="form-control" name="surat" onChange={this.handleInputChange} value={this.state.surat}>
                                <option value="1">Masuk</option>
                                <option value="2">Keluar</option>
                            </select>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-4">
                            <label>Penanggung Jawab</label>
                            <input className="form-control" name="penanggungjawab" onChange={this.handleInputChange} placeholder="Penanggung Jawab" value={this.state.penanggungjawab} />
                        </div>
                        <div className="col-sm-4">
                            <label>Telepon</label>
                            <input className="form-control" name="telepon" onChange={this.handleInputChange} placeholder="Telepon" value={this.state.telepon} />
                        </div>
                        <div className="col-sm-4">
                            <label>Tanggal</label>
                            <div>
                                <DatePicker dateFormat="yyyy-MM-dd" className="form-control" placeholderText="Tanggal" selected={this.state.tanggal} onChange={this.handleDateChange} />
                            </div>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-4">
                            Nama Barang
                            <input className="form-control" name="namabarang" onChange={this.handleInputChange} placeholder="Nama Barang" value={this.state.namabarang} />
                        </div>
                        <div className="col-sm-4">
                            <br />
                            <div className="input-group mb-2">
                                <div className="input-group-prepend">
                                    <button className="form-control btn bg-orange c-white" type="button" onClick={this.handleMinusButton}>-</button>
                                </div>
                                <input type="text" className="mx-1 form-control" name="qty" onChange={this.handleInputChange} placeholder="0" value={this.state.qty} />
                                <div className="input-group-postpend">
                                    <button className="form-control btn bg-orange c-white" type="button" onClick={this.handlePlusButton}>+</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4">
                            <br/>
                            <button className="form-control btn bg-orange c-white" type="button" onClick={this.handleAddButton}>Tambahkan</button>
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-12" style={{ overflowY: 'auto', maxHeight: '200px' }}>
                            <label>Daftar barang yang sudah dimasukkan</label>
                            <table className="table table-sm">
                                <thead>
                                    <tr className="bg-orange c-white">
                                        <th>JENIS</th>
                                        <th>JUMLAH</th>
                                        <th />
                                    </tr>
                                </thead>
                                <tbody>
                                    {listBarang}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-4">
                            <button className="btn bg-orange form-control c-white" type="button" onClick={this.handleSubmitButton}>SELESAI</button>
                            <p>{this.state.submitresult}</p>
                        </div>
                        <div className="col-sm-8">
                            <div className="border p-2">
                                <div className="row">
                                    <div className="col-sm-1">
                                        <img className="img-responsive w-100" src="/assets/master/img/tandaseru.png" style={{ maxWidth: '25px' }} />
                                    </div>
                                    <div className="col-sm-11">
                                        PASTIKAN DAFTAR BARANG YANG ANDA AJUKAN SESUAI DENGAN&nbsp;
                                        <a href="#" target="_blank" className="c-orange" style={{ textDecoration: 'none' }}>SYARAT DAN KETENTUAN</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}


if (document.getElementById('divLoadingCreate')) {
    const element = document.getElementById('divLoadingCreate');
    if (element.dataset.accesstoken !== undefined || element.dataset.cpyid !== undefined) {
        ReactDOM.render(<LoadingCreate accesstoken={element.dataset.accesstoken} cpyid={element.dataset.cpyid} />, document.getElementById('divLoadingCreate'));
    }
}
