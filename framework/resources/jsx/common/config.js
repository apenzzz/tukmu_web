const Config = {
    APIUrl: 'https://tukbase3.tukmu.com/api',
    BaseUrl: 'https://tukbase3.tukmu.com/'
};

export default Config;
