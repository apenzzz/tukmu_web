import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';

class RefundSelect extends React.Component {
    constructor(props) {
        super(props);
        this.handleEventClick = this.handleEventClick.bind(this);
    }

    handleEventClick(cpyid) {
        window.location.href = "/refund/create/" + cpyid;
    }

    render() {
        const EventItems = this.props.items.map((value, i) => {
            const Stans = value.payment_grid_with_code.map((v, j) => {
                if (value.payment_grid_with_code.length - 1 === j) {
                    return v.code;
                }
                else {
                    return v.code + ',';
                }
            });

            return (
                <div className="row" key={i}>
                    <div className="col-sm-12 p-3 my-3 bg-white">
                        <div className="row">
                            <div className="col-sm-3">
                                <img className="img-responsive w-100" src={value.space_detail[0].first_media !== null ? Config.BaseUrl + '/picGet/' + value.space_detail[0].first_media.spacemediapath : null} />
                            </div>
                            <div className="col-sm-9">
                                <h3>{value.space_detail[0].eventname}</h3>
                                <div className="row">
                                    <div className="col-sm-9">
                                        <div className="row">
                                            <div className="col-sm-6 mb-1">
                                                <img src="/assets/master/img/search-engine/date.png"
                                                    style={{ width: '20px' }} />&nbsp;&nbsp;{value.startbookdate + " - " + value.finishbookdate}
                                            </div>
                                            <div className="col-sm-6 mb-1">
                                                <img src="/assets/master/img/search-engine/location.png" style={{ width: '20px' }} />&nbsp;&nbsp;{value.space_detail[0].spacename}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <span className="c-gray">STAN</span><br />
                                <span><strong>{Stans}</strong></span><br />
                                <div className="col-sm-9 p-0">
                                    <hr className="bg-gray m-0" />
                                </div>
                                <br />
                                <span className="c-gray">BRAND</span><br />
                                <span><strong>{value.customer_brand !== null ? value.customer_brand.brandname : null}</strong></span><br />
                                <div className="col-sm-9 p-0">
                                    <hr className="bg-gray m-0" />
                                </div>
                                <br />
                                <span className="c-gray">PENGELOLA / EVENT ORGANIZER</span><br />
                                <span><strong>{value.space_detail[0].space_provider.spaceprovname}</strong></span><br />
                                <br />
                            </div>
                        </div>
                        <hr className="bg-gray" />
                        <div className="row">
                            <div className="col-sm-12 text-center">
                                <button className="bg-orange c-white px-3 py-2" onClick={() => this.handleEventClick(value.custpaymentid)}>REFUND</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        });

        return (
            <React.Fragment>
                <div className="container">
                    <div className="row">
                        <div className="offset-sm-2 col-sm-8">
                            <section className="mt-5">
                                <div className="container">
                                    <h2 className="text-center">Halo, {this.props.username}</h2>
                                    <p className="text-center">Silahkan pilih event yang ingin direfund</p>
                                </div>
                            </section>
                            <section className="my-5">
                                <div className="container">
                                    {EventItems}
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

if (document.getElementById('divRefundSelect')) {
    const element = document.getElementById('divRefundSelect');
    let items = [];
    if (element.dataset.accesstoken !== undefined && element.dataset.username !== undefined) {
        axios.get(Config.APIUrl + "/customer/view/event/refund/trigger/list", { 'headers': { 'Authorization': 'Bearer ' + element.dataset.accesstoken } })
            .then(result => {
                items = result.data.payment_list;
                ReactDOM.render(<RefundSelect username={element.dataset.username} items={items} />, element);
            })
            .catch(e => {
                console.log(e);
            });
    }
}
