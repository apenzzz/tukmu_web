import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Config from '../common/config';
import { ConvertToRupiah } from '../common/helper';

class RefundCreate extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            catatan: '',
            issubmitting: false,
            submitresult: ''
        };
        this.handleCatatan = this.handleCatatan.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleCatatan(e) {
        this.setState({
            catatan: e.target.value
        });
    }

    handleSubmit(e) {
        if (this.state.catatan !== '') {
            e.preventDefault();
            let url = "/customer/create/event/refund";
            if (!this.state.issubmitting) {
                this.setState({
                    submitresult: 'Sending...',
                    issubmitting: true
                });
                axios.post(Config.APIUrl + url, { cpyid: this.props.cpyid, refund_note: this.state.catatan }, { 'headers': { 'Authorization': 'Bearer ' + this.props.accesstoken } })
                    .then(result => {
                        console.log(result);
                        if (result.data.status === "1") {
                            this.setState({
                                catatan: '',
                                submitresult: 'Success',
                                issubmitting: false
                            });
                        }
                        else {
                            this.setState({
                                submitresult: 'Error',
                                issubmitting: false
                            });
                        }
                    })
                    .catch(e => {
                        console.log(e);
                    });
            }
        }
    }

    render() {
        console.log(this.props);
        return (
            <div className="container">
                <div className="row">
                    <div className="offset-sm-2 col-sm-8">
                        <div className="container my-5">
                            <h2>Halo, {this.props.username}</h2>
                            <p>Silahkan isi formulir pengajuan refund</p>
                            <form>
                                <div className="bg-white p-3">
                                    <h3>{this.props.paymentdata.space_detail[0].eventname}</h3>
                                    <div className="row">
                                        <div className="col-sm-6">
                                            <p><img src="/assets/master/img/search-engine/date.png" style={{ width: '20px' }} />&nbsp;&nbsp;{this.props.paymentdata.space_detail[0].startdate + " - " + this.props.paymentdata.space_detail[0].finishdate}&nbsp;&nbsp;<img src="/assets/master/img/search-engine/location.png" style={{ width: '20px' }} />&nbsp;&nbsp;{this.props.paymentdata.space_detail[0].spacename}</p>
                                            <hr />
                                            <span>BRAND</span><br />
                                            <span className="c-orange"><strong><u>{this.props.paymentdata.customer_brand.brandname}</u></strong></span><br />
                                            <hr />
                                            <span>PENGELOLA</span><br />
                                            <span className="c-orange"><strong><u>{this.props.paymentdata.space_detail[0].space_provider.spaceprovname}</u></strong></span><br />
                                            <hr />
                                            <span>DEAL HARGA</span><br />
                                            <span className="c-orange"><strong><u>{ConvertToRupiah(this.props.paymentdata.total_payment_plus_addon_deposit)}</u></strong></span><br />
                                        </div>
                                        <div className="col-sm-6">
                                            <p>CATATAN</p>
                                            <textarea rows="5" className="form-control" onChange={this.handleCatatan} required />
                                        </div>
                                    </div>
                                    <hr />
                                    <button type="submit" className="btn btn-default bg-orange c-white" onClick={this.handleSubmit}>AJUKAN</button>
                                    <p>{this.state.submitresult}</p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

if (document.getElementById('divRefundCreate')) {
    const element = document.getElementById('divRefundCreate');
    if (element.dataset.accesstoken !== undefined && element.dataset.cpyid !== undefined && element.dataset.username !== undefined) {
        axios.get(Config.APIUrl + "/customer/get/event/refund/payment/data?cpyid=" + element.dataset.cpyid, { 'headers': { 'Authorization': 'Bearer ' + element.dataset.accesstoken } })
            .then(result => {
                ReactDOM.render(<RefundCreate accesstoken={element.dataset.accesstoken} cpyid={element.dataset.cpyid} paymentdata={result.data.payment_data} username={element.dataset.username} />, element);
            })
            .catch(e => {
                console.log(e);
            });
    }
}
